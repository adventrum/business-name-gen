<?php

namespace BNWM\Base;

use BNWM\Base\Config;
use BNWM\Utility\ArrayUtility;
use BNWM\WordManager\WordManager;

/**
 * BusinessNameWordManager Class.
 *
 * @class BusinessNameWordManager
 */
final class BusinessNameWordManager {

    use ArrayUtility;


    /**
	 * The single instance of the class.
	 *
	 * @var BusinessNameWordManager
	 */
	protected static $_instance = null;

    public Config $config;

    public WordManager $wordmanager;

    /**
	 * Main BusinessNameWordManager Instance.
	 *
	 * Ensures only one instance of BusinessNameWordManager is loaded or can be loaded.
	 *
	 * @static
	 * @return BusinessNameWordManager - Main instance.
	 */
	public static function instance(): BusinessNameWordManager {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

    /**
	 * Cloning is forbidden.
	 *
	 */
	public function __clone() {
		echo __FUNCTION__ . 'Cloning is forbidden.';
		die;
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	public function __wakeup() {
		echo __FUNCTION__ . 'Unserializing instances of this class is forbidden.';
		die;
	}

    /**
	 * BusinessNameWordManager Constructor.
	 */
	public function __construct() {
        $this->config = Config::instance();
        $this->wordmanager = new WordManager;
        $this->define_constants();
		$this->includes();
	}

    public function define_constants() {
        define("BNWM_ABSPATH", $this->config->getPluginPath());
    }


	/**
	 * Include required core files used in admin and on the frontend.
	 */
	public function includes() {
        /**
		 * Class autoloader.
		 */
		$config = $this->config;
		include_once BNWM_ABSPATH . '/inc/bnw-core-functions.php';

    }
}

?>