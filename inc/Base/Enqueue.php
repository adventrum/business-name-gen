<?php

/**
 * @package BusinessNameWordManager
 *
 */

namespace BNWM\Base;

class Enqueue
{
    public function register()
    {
        add_action('admin_enqueue_scripts', array($this, 'enqueue'));
        add_action('wp_enqueue_scripts', array($this, 'frontendEnqueue'));
    }

    public function enqueue()
    {  
        wp_enqueue_media();
        wp_enqueue_style('word-manager-style', WORD_MANAGER_PLUGIN_URL . 'assets/admin/word-manager.css');
        wp_enqueue_script('word-manager-script', WORD_MANAGER_PLUGIN_URL . 'assets/admin/word-manager.js');
        wp_localize_script( 'word-manager-script', 'wordAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));   
    }

    public function frontendEnqueue()
    {   
        if( get_page_template_slug() == 'template-domains.php' ||  get_page_template_slug() == 'template-nameideas.php' ){
          wp_enqueue_style('font-awesome',  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css?ver=5.6.2', array(), '');
         // wp_enqueue_script('jquery-min-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'); 
          wp_enqueue_script('cookie-cdn-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js');
          wp_enqueue_style('font-awesome',  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css?ver=5.15.3', array(), '');
          wp_enqueue_style('filter-css', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/filter.css');
          wp_enqueue_style('result-page-css', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/result-page.css');
          wp_enqueue_style('domains-page-css', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/domains-page.css');
          wp_enqueue_style('asRange-css', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/asRange.css');
          wp_enqueue_style('elegant-css', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/elegant.css');
          wp_enqueue_style('bootstrap-min-css', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/bootstrap.min.css');
          wp_enqueue_style('icons-elegant-css', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/icons-elegant.css');
          wp_enqueue_style('exit-popup-css', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/exit-popup.css');
          wp_enqueue_script('bootstrap-min-js', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/bootstrap.min.js');
          wp_enqueue_script('jquery-cookie-js', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/jquery.cookie.js');
          wp_enqueue_script('jquery-asRange-js', WORD_MANAGER_PLUGIN_URL . 'assets/frontend/resultpage/jquery-asRange.js');
        }
    }
}
