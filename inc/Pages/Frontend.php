<?php 
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Pages;
use BNWM\Base\BaseController;
class Frontend extends BaseController
{   
    /**
     * A reference to an instance of this class.
     */
    private static $instance;
    /**
     * The array of templates that this plugin tracks.
     */
    protected $templates;
  
    private $textDomain;
    public function register() 
    {
        // $this->textDomain = 'business_name_word_manager';
        // add_filter( 'theme_page_templates', [ $this, 'createTemplates' ] );
        $this->templates = array();
      
        add_action('wp_ajax_ajax_saved_idea', [ $this, 'ajax_saved_idea' ] );
        add_action('wp_ajax_nopriv_ajax_saved_idea', [ $this, 'ajax_saved_idea' ] );
       
        add_action('wp_ajax_ajax_con', [ $this, 'change_url' ] );
        add_action('wp_ajax_nopriv_ajax_con', [ $this, 'change_url' ]);
       
        add_action('wp_ajax_send_saved_ideas', [ $this, 'send_saved_ideas' ] );
        add_action('wp_ajax_nopriv_send_saved_ideas', [ $this, 'send_saved_ideas' ] );

        add_action('wp_ajax_get_pagination', [ $this, 'get_pagination' ] );
        add_action('wp_ajax_nopriv_get_pagination', [ $this, 'get_pagination' ] );

        add_action('domainify_data', [ $this, 'domainify_pull_data' ] );
      
        add_action('init', [ $this, 'start_session' ] );
        // Add a filter to the attributes metabox to inject template into the cache.

        //Load template from specific page
        add_filter( 'page_template', [$this, 'pageTemplates'] );

        /**
         * Add "Custom" template to page attirbute template section.
         */
        add_filter( 'theme_page_templates', [$this, 'addCustomPageTemplate'], 10, 4 );
    }

    public function add_new_template( $posts_templates ) {
        $posts_templates = array_merge( $posts_templates, $this->templates );
        return $posts_templates;
    }
    
    public function start_session()
    {
        if (!session_id()) {
            session_start();
        }
        add_action('wp_logout', 'end_session');
        add_action('wp_login', 'end_session');
        add_action('end_session_action', 'end_session');
    }
    
    public function end_session()
    {
        session_destroy();
    }
    public function send_saved_ideas(){
       include_once(WORD_MANAGER_PLUGIN_PATH . '/templates/frontend/action.php');
    }

    public function pageTemplates( $page_template ){

        if ( get_page_template_slug() == 'template-domains.php' ) {
            $page_template = WORD_MANAGER_PLUGIN_PATH . 'templates/frontend/template-domains.php';
        }
        if ( get_page_template_slug() == 'template-nameideas.php' ) {
            $page_template = WORD_MANAGER_PLUGIN_PATH . 'templates/frontend/template-nameideas.php';
        }
        return $page_template;
    }

    public function addCustomPageTemplate( $post_templates, $wp_theme, $post, $post_type ) {

        // Add custom template named template-custom.php to select dropdown 
        $post_templates['template-domains.php'] = __('Domainsv2');
        $post_templates['template-nameideas.php'] = __('Nameideasv2');

        return $post_templates;
    }

    public function ajax_saved_idea(){
        echo json_encode($_SESSION['invite_modal_value']);
        exit;
    }

    public function change_url()
    {
        //session_start();
        $data_invite1 = $_REQUEST['id'];
        $data_name    = $_REQUEST['name'];
        $data_invite2 = $_REQUEST['active'];
        $check_invite_value = $_SESSION['invite_modal_value'];
        if (!empty($check_invite_value)) {
            $check_invite_value1 = $check_invite_value;
        }
        //$check_invite_value1[$data_name]=$data_invite1;
        // $check_invite_value1[$data_invite1]=$data_name;
        // print_r($check_invite_value1);
        if ($data_invite2 == 1) {
            $check_invite_value1[$data_name] = $data_invite1;
            $_SESSION['invite_modal_value'] = $check_invite_value1;
            print_r(json_encode($_SESSION['invite_modal_value']));
        }
        if ($data_invite2 == 0) {
            //    $result = array_flip($check_invite_value1);     
            // unset($result[$data_name]);
            // $result = array_flip($result);
            $result = $check_invite_value1;
            unset($result[$data_name]);
            $_SESSION['invite_modal_value'] = $result;
            print_r(json_encode($_SESSION['invite_modal_value']));
        }
        exit;
    }
    
    function domainify_pull_data()
    { 

        $handle = curl_init();

        // $url = 'https://domainify.com/wc-api/v3/products?filter[limit]=450&consumer_key=ck_956a4395152f3de8172f8e3d2d23851ae2bef3ac&consumer_secret=cs_dfbfff7e664bab942b795601cbb9420a67868d5a';
        $url = 'https://domainify.com/wc-api/v3/products?filter[limit]=650&consumer_key=ck_9bd4fc6fa4e2cfe56359ae9bfb943c782846a627&consumer_secret=cs_31927dcd544e3552e65cb9f007ac9970dda46876';

        // Set the url
        curl_setopt($handle, CURLOPT_URL, $url);
        // Set the result output to be a string.
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($handle);

        curl_close($handle);

        $domains = json_decode($output, true);
        // $file = file_get_contents(dirname(__FILE__).'/name.json');

        // $domains = json_decode($file, true);

        global $wpdb;

        $domain_list = array();

        $all_res = $wpdb->get_results("SELECT `product_id` from domains", ARRAY_N);
        $all_name = $wpdb->get_results("SELECT `title` from domains", ARRAY_N);

        $previous_data = array();
        $previous_name_data = array();
        $new_data = array();

        foreach ($all_res as $key => $value) {
            foreach ($value as $id) {
                array_push($previous_data, $id);
            }
        }

        foreach ($all_name as $key => $value) {
            foreach ($value as $title) {
                array_push($previous_name_data, $title);
            }
        }

        foreach ($domains as $key => $domain) {

            foreach ($domain as $data) {
                array_push($new_data, $data['id']);
            }
        }

        foreach ($previous_data as $key => $data_id) {
            if (!in_array($data_id, $new_data) && !empty($previous_data)) {
                $wpdb->query($wpdb->prepare(
                    "DELETE FROM `domains`
                        WHERE product_id = %d
                        ",
                    $data_id
                ));
            }
        }


        $result = '';
        $image = '';
        $ctg = '';
        $tags = '';
        foreach ($domains as $key => $value) {

            foreach ($value as $ts) {
                $concat_title = strtolower(str_replace(".","",$ts['title']));
                if (in_array($ts['id'], $previous_data) && in_array($ts['title'], $previous_name_data) && !empty($previous_data)) {

                    if (stripos($ts['featured_src'], '.jpg')) {
                        $image = str_replace('.jpg', '-100x100.jpg', $ts['featured_src']);
                    }

                    if (stripos($ts['featured_src'], '.png')) {
                        $image = str_replace('.png', '-100x100.png', $ts['featured_src']);
                    }

                    if (count($ts['categories']) > 0 && is_array($ts['categories'])) {
                        $ctg = implode(',', $ts['categories']);
                    }

                    if (count($ts['tags']) > 0 && is_array($ts['tags'])) {
                        $tags = implode(',', $ts['tags']);
                    }
                    $uploaddir = wp_get_upload_dir();
                    $filename = basename($image);
                    copy($image, $uploaddir['basedir'].'/domains/'.$concat_title.$filename);
                    $image = $uploaddir['baseurl'].'/domains/'.$concat_title.$filename;

                    if( isset($ts['images'][0]['alt']) && !empty($ts['images'][0]['alt']) ){
                        $alt_tag = $ts['images'][0]['alt'];
                    } else {
                        $alt_tag = str_replace('.', '-', $ts['title']);
                    }

                    $result = $wpdb->query($wpdb->prepare(
                        "UPDATE `domains` SET `title` = '%s', `product_id` = '%d', `price`= '%s', `regular_price`= '%s', `sale_price`= '%s', `status`= '%s', `featured_src`= '%s', `tags`= '%s', `categories`= '%s', `permalink`= '%s', `price_html`= '%s', `alt` = '%s' WHERE `product_id` = '%d'",

                        $ts['title'],
                        $ts['id'],
                        $ts['price'],
                        $ts['regular_price'],
                        $ts['sale_price'],
                        $ts['in_stock'],
                        $image,
                        $tags,
                        $ctg,
                        $ts['permalink'],
                        $ts['price_html'],
                        $alt_tag,
                        $ts['id']
                        
                    ));
                } else {

                    if (stripos($ts['featured_src'], '.jpg')) {
                        $image = str_replace('.jpg', '-100x100.jpg', $ts['featured_src']);
                    }

                    if (stripos($ts['featured_src'], '.png')) {
                        $image = str_replace('.png', '-100x100.png', $ts['featured_src']);
                    }

                    if (count($ts['categories']) > 0 && is_array($ts['categories'])) {
                        $ctg = implode(',', $ts['categories']);
                    }

                    if (count($ts['tags']) > 0 && is_array($ts['tags'])) {
                        $tags = implode(',', $ts['tags']);
                    }

                    $uploaddir = wp_get_upload_dir();
                    $filename = basename($image);
                    copy($image, $uploaddir['basedir'].'/domains/'.$concat_title.$filename);
                    $image = $uploaddir['baseurl'].'/domains/'.$concat_title.$filename;

                    if( isset($ts['images'][0]['alt']) && !empty($ts['images'][0]['alt']) ){
                        $alt_tag = $ts['images'][0]['alt'];
                    } else {
                        $alt_tag = str_replace('.', '-', $ts['title']);
                    }

                    $wpdb->query($wpdb->prepare(
                        "INSERT INTO domains (`title`, `product_id`,`price`, `regular_price`, `sale_price`, `status`, `featured_src`, `tags`, `categories`, `permalink`, `price_html`, `alt`)
                        VALUES (%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",

                        $ts['title'],
                        $ts['id'],
                        $ts['price'],
                        $ts['regular_price'],
                        $ts['sale_price'],
                        $ts['in_stock'],
                        $image,
                        $tags,
                        $ctg,
                        $ts['permalink'],
                        $ts['price_html'],
                        $alt_tag
                    ));
                }
            }
        }
    }
    
    public function get_pagination()
    {  
        
        $limit = isset($_POST['limit']) && !empty($_POST['limit']) ? $_POST['limit'] : 96;
        $total_records = isset($_POST['total_record']) && !empty($_POST['total_record']) ? $_POST['total_record'] : '';
        $strURL = $_SERVER['HTTP_REFERER'];
        $arrVals = explode("?", $strURL);
        $arrVals2 = explode("/", $arrVals[0]);
        $numkey = str_word_count(stripslashes($_GET['bname']));
        $translations = json_decode(get_option('bnwm_string_translations'));
        if ($numkey <= '1') {
          $bname = stripslashes($_GET['bname']);
        } else {
          $fword = explode(' ', stripslashes($_GET['bname']));
          $bname = $fword[0] . ' ' . $fword[1];
        }
        if (isset($_GET['related'])) {
          $element = '?bname=' . $bname . "&related=" . $_GET['related'];
        } else {
          $element = '?bname=' . $bname;
        }
        $found = 0;
        $paged = isset($_POST['paged']) && !empty($_POST['paged']) ? $_POST['paged'] : 5;
        if ($arrVals2 == '' || $paged < 5) {
            $found = $found + 1;
        } else {
            $found = $arrVals2[$paged];
        }
        //$pagination=$strURL;
        $current_page = (!empty($found) && $found != 1) ? $found : 1;
        $total_pages = ceil($total_records / $limit);
    
        $pagination = '';
        if ($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages) { //verify total pages and current page number
            $pagination .= '<ul>';
    
            if (isset($_POST['device']) && $_POST['device'] == 'mobile') {
                if ($current_page == 1) {
                    $right_links = $current_page + 3;
                } elseif ($current_page > 1 && $current_page < 3) {
                    $right_links = $current_page + 2;
                } else {
                    $right_links = $current_page;
                }
            } else {
                $right_links = $current_page + 3;
            }
    
            $previous = $current_page - 3; //previous link
            $next = $current_page + 1; //next link
            $first_link = true; //boolean var to decide our first link
    
            if ($current_page > 1) {
                $previous_link = $current_page - 1;
                $prev_url = $_POST['url'].'page/'.$previous_link.'/?'. $_POST['query_string'];
                //if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
                $pagination .= '<li><a href="' .$prev_url. '" data-page="' . $previous_link . '" title="Previous"><span class="arrow_carrot-left elegant-icon"></span>' .$translations->previous . '</a></li>'; //previous link
                // }else{
                //  $pagination .= '<li><a href="'.$url . $previous_link . '" data-page="' . $previous_link . '" title="Previous">&lt; Back</a></li>'; //previous link
                // }
                for ($i = ($current_page - 2); $i < $current_page; $i++) { //Create left-hand side links
                    $url = $_POST['url'].'page/'.$i.'/?'. $_POST['query_string'];
                    if ($i > 0) {
                        $pagination .= '<li><a href="' .$url. '" data-page="' . $i . '" title="Page' . $i . '">' . $i . '</a></li>';
                    }
                }
                $first_link = false; //set first link to false
            }
            $url = $_POST['url'].'page/'.$current_page.'/?'. $_POST['query_string'];
            if ($first_link) { //if current active page is first link
                $pagination .= '<li class="select" ><a class="select" href="' .$url. ' "> ' . $current_page . '</a></li>';
            } elseif ($current_page == $total_pages) { //if it's the last active link
                $pagination .= '<li class="select" ><a class="select" href="' .$url. ' "> ' . $current_page . '</a></li>';
            } else { //regular current link
                $pagination .= '<li class="select"><a class="select" href="' .$url. ' "> ' . $current_page . '</a></li>';
            }
    
            for ($i = $current_page + 1; $i < $right_links; $i++) { //create right-hand side links
                $url = $_POST['url'].'page/'.$i.'/?'. $_POST['query_string'];
                if ($i <= $total_pages) {
                    $pagination .= '<li><a href="' .$url. '"   title="Page ' . $i . '">' . $i . '</a></li>';
                }
            }
            if ($current_page < $total_pages) {
                $next_link = $current_page + 1;
                $next_url = $_POST['url'].'page/'.$next_link.'/?'. $_POST['query_string'];
                //if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
                $pagination .= '<li><a href="' .$next_url. '" data-page="' . $next_link . '" title="Next">' . $translations->next . ' <span class="arrow_carrot-right elegant-icon"></span></a></li>'; //next link
                // }else{
                //  $pagination .= '<li><a href="' .$url . $next_link . '" data-page="' . $next_link . '" title="Next">'.__('Next', 'thegem-child').' &gt;</a></li>'; //next link
                // }
            }
    
            $pagination .= '</ul>';
        }
        echo $pagination; die;
    
    }
}