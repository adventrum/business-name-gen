<?php

/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\DataMuse;


class Response {

    private $response;
    private $responseCode;

    function __construct($response, $responseCode) {
        $this->setResponse($response);
        $this->setResponseCode($responseCode);
    }
    
    /**
     * setResponse
     *
     * @param  mixed $resp
     * @return void
     */
    public function setResponse($resp) {
        $this->response = $resp;
    }
    
    /**
     * getResponse
     *
     * @return mixed
     */
    public function getResponse() {
        return $this->response;
    }


    public function setResponseCode($respCode) {
        $this->responseCode = $respCode;
    }
    
    
    public function getResponseCode() {
        return $this->responseCode;
    }

}