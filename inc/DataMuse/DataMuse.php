<?php

/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\DataMuse;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Iterator;

/**
 *
 *
 * @since      1.0.0
 * @package    BusinessNameWordManager
 * @author     Sumit Sehgal <sumit@cybertrontechnologies.com>
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'DataMuse' ) ) :

/**
 * DataMuse
 */
final class DataMuse {

    const BASE_URI = "https://api.datamuse.com/";
	
	const WORD_ENDPOINT = "/words";

	const OP_MEANS_LIKE 			= 	"means-like",
		OP_SOUND_LIKE				= 	"sound-like",
		OP_SPELL_LIKE				= 	"spell-like",
		OP_VOCABULARY				=	"vocabulary",
		OP_ARROUND_TOPIC			=	"arround-topic",
		OP_LEFT_CONTEXT				=	"left-context",
		OP_RIGHT_CONTEXT			=	"right-context",
		OP_MAXIMUM_LIMIT			=	"maximum-limit",
		OP_META_DATA				=	"meta-data", //d	Definitions; p	Parts of speech; s	Syllable count; r	Pronunciation; f	Word frequency
		OP_RELATED_WORD				=	"related-word",
		OP_RELATED_NOUN				=	"noun",
		OP_RELATED_ADJECTIVE		=	"adjective",
		OP_RELATED_SYNONYMS			=	"synonyms",
		OP_RELATED_TRIGGERS			=	"associated-triggers",
		OP_RELATED_ANTONYMS			=	"antonyms",
		OP_RELATED_HYPERNYMS		=	"hypernyms", // Kind Of- supertype
		OP_RELATED_HYPONYMS			=	"hyponyms", // More-General -subtype
		OP_RELATED_HOLONYMS			=	"holonyms", // Comprises - whole object -> parts
		OP_RELATED_MERONYMS			=	"meronyms", // Parts - parts -> whole object
		OP_RELATED_FOLLOWERS		=	"followers",
		OP_RELATED_PREDECESSOR		=	"predecessors",
		OP_RELATED_RHYMES			=	"perfect-rhymes", // Perfect Rhymes
		OP_RELATED_APPROX_RHYMES	=	"approximate-rhymes", // Approximate Rhymes 
		OP_RELATED_HOMOPHONES		=	"homophones",
		OP_RELATED_CONSONENT		=	"consonent-match";

	const OPTIONS = [
		self::OP_MEANS_LIKE 	=> 	"ml",
		self::OP_SOUND_LIKE 	=> 	"sl",
		self::OP_SPELL_LIKE 	=> 	"sp",
		self::OP_VOCABULARY		=> 	"v",
		self::OP_ARROUND_TOPIC	=> 	"topics",
		self::OP_LEFT_CONTEXT	=> 	"lc",
		self::OP_RIGHT_CONTEXT	=> 	"rc",
		self::OP_MAXIMUM_LIMIT	=> 	"max",
		self::OP_META_DATA		=>	"md",
		self::OP_RELATED_WORD	=>	"rel_"
	];

	const FILTER_OPTIONS = [
		self::OP_MEANS_LIKE,
		self::OP_SPELL_LIKE,
		self::OP_META_DATA
	];

	const RELATED_OPTIONS = [
		self::OP_RELATED_NOUN			=>	"jja",
		self::OP_RELATED_ADJECTIVE		=>	"jjb",
		self::OP_RELATED_SYNONYMS		=>	"syn",
		self::OP_RELATED_TRIGGERS		=>	"trg",
		self::OP_RELATED_ANTONYMS		=>	"ant",
		self::OP_RELATED_HYPERNYMS		=>	"spc", // Kind Of- supertype
		self::OP_RELATED_HYPONYMS		=>	"gen", // More-General -subtype
		self::OP_RELATED_HOLONYMS		=>	"com", // Comprises - whole object -> parts
		self::OP_RELATED_MERONYMS		=>	"par", // Parts - parts -> whole object
		self::OP_RELATED_FOLLOWERS		=>	"bga",
		self::OP_RELATED_PREDECESSOR	=>	"bgb",
		self::OP_RELATED_RHYMES			=>	"rhy", // Perfect Rhymes
		self::OP_RELATED_APPROX_RHYMES	=>	"nry", // Approximate Rhymes 
		self::OP_RELATED_HOMOPHONES		=>	"hom",
		self::OP_RELATED_CONSONENT		=>	"cns"
	];

	const ADDON_PARAMS = [
		self::OP_RELATED_ADJECTIVE 	=> "adj",
		self::OP_RELATED_NOUN		=>	"n",

	];

	const DEFAULT_LIMIT = 1000;

	const VOCAB = [
		"",
		"enwiki",
		"es"
	];

	private $request = [];

	private $requestedFields = ["word"];

	private $addOnFields = [];

    /**
	 * @var Instance
	 * @since 1.0
	 */
	protected static $instance = null;

    /**
	 * @var Client
	 * @since 1.0
	 */
	protected ClientInterface $client;

    /**
	 * @var ErrorHandlerInterface
	 * @since 1.0
	 */
	protected ErrorHandlerInterface $err;

	/**
	 * @var string
	 * @since 1.0
	 */
	protected string $word;

	/**
	 * @var array
	 * @since 1.0
	 */
	protected array $appliedFilters = [];

	/**
	 * @var string
	 * @since 1.0
	 */
	protected string $vocab = "";

	/**
	 * @var int
	 * @since 1.0
	 */
	protected int $limit = self::DEFAULT_LIMIT;

	/**
	 * @var array
	 * @since 1.0
	 */
	public static array $excludedWords;

    
    /**
     * __construct
     *
     * @param  mixed $client
     * @return void
     */
    public function __construct() {
		self::$excludedWords = [];
        $this->setupGuzzle();
        $this->setupErrorHandler();
        
    }
    
    /**
     * instance
     *
     * @return DataMuse
     */
    public static function instance(): DataMuse {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * setExcludedWords
	 *
	 * @param  mixed $list
	 * @return void
	 */
	public static function setExcludedWords(array $list = []): void {
		$result = array_map('trim', $list);
		$result = array_map('strtolower', $result);
		self::$excludedWords = $result;
	}
	
	/**
	 * clearExcludeWords
	 *
	 * @return void
	 */
	public static function clearExcludeWords(): void {
		self::$excludedWords = [];
	}
	
	/**
	 * getExcludedWords
	 *
	 * @return array
	 */
	public static function getExcludedWords(): array {
		return self::$excludedWords;
	}
    
    /**
     * setupGuzzle
     *
     * @return void
     */
    private function setupGuzzle(): void {
        $this->client = new Client([
            'base_uri' => self::BASE_URI,
            'timeout'  => 15,
        ]);
    }

    private function setupErrorHandler(): void {
        // TODO: Switch to WP or other platform
        $this->err = WpErrorHandler::instance();
    }

    /**
	 * Throw error on object clone.
	 *
	 * The whole idea of the singleton design pattern is that there is a single object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0
	 * @access protected
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden.
		$this->err->actionErr(__FUNCTION__, 'Cheatin&#8217; huh?', '1.0');
	}

	/**
	 * Disable unserializing of the class.
	 *
	 * @since 1.0
	 * @access protected
	 * @return void
	 */
	public function __wakeup(): void {
		// Unserializing instances of the class is forbidden.
		$this->err->actionErr(__FUNCTION__, 'Cheatin&#8217; huh?', '1.0');
	}
	
	/**
	 * getWord
	 *
	 * @return string
	 */
	public function getWord(): string {
		return $this->word;
	}
	
	/**
	 * setWord
	 *
	 * @param  string $word
	 * @return void
	 */
	public function setWord(string $word): void {
		if(!empty($word))
			$this->word = $word;
	}
	
	/**
	 * word
	 *
	 * @param  string $word
	 * @return DataMuse
	 */
	public function word(string $word): DataMuse {
		if(is_array($word)) {
			$word = implode(",", $word);
		}
		$this->setWord($word);
		
		return $this;
	}
	
	/**
	 * clearRequest
	 *
	 * @return void
	 */
	public function clearRequest (): void {
		$this->request = [];
	}

	public function getVocab(): string {
		return $this->vocab;
	}

	public function setVocab($vocab): void {
		$this->vocab = $vocab;
	}

	public function vocab($vocab): DataMuse {
		if(!empty($vocab) && in_array($vocab, self::VOCAB)) {
			$this->setVocab($vocab);
		}
		return $this;
	}
	
	/**
	 * addRequest
	 *
	 * @param  string $key
	 * @param  string $value
	 * @return void
	 */
	private function addRequest(string $key, string $value): void {
		$this->request[$key] = $value;
	}
	
	/**
	 * getUrl
	 *
	 * @param  string $endpoint
	 * @param  array $query
	 * @return string
	 */
	private function getUrl(string $endpoint, array $query): string {
		$url = self::BASE_URI.$endpoint."?".http_build_query($query);
		return $url;
	}

	public function makeRequest(string $method, string $endpoint, string $params="", array $query = [], array $headers = []): Request {

		$query = array_merge(
			$query, 
			$this->getFilter(), 
			[self::OPTIONS[self::OP_MAXIMUM_LIMIT] => $this->getLimit() ] 
		);
		if(!empty($this->getVocab())) {
			$query[self::OPTIONS[self::OP_VOCABULARY]] = $this->getVocab(); 
		}
		$request = new Request($method, $this->getUrl($endpoint, $query), $headers, $params);
		$this->resetRequest();
		return $request;

	}
	
	/**
	 * executeRequest
	 *
	 * @param  string $method
	 * @param  string $endpoint
	 * @param  string $params
	 * @param  array $query
	 * @param  array $headers
	 * @return Response
	 */
	private function executeRequest(Request $request): Response  {
		try{
			$promise = $this->client->sendAsync($request)->then(function ($response) {
				$response = new Response($response->getBody()->getContents(), $response->getStatusCode());
				return $response;
			})->otherwise(function($response){
				$response = new Response("{}", $response->getCode());
				return $response;
			});
		}catch(\GuzzleHttp\Exception\ConnectException $e) {
			$response = new Response(`{$e->getMessage()}`, 503);
			return $response;
		}

		return $promise->wait();
	}
	
	/**
	 * getFields
	 *
	 * @return array
	 */
	public function getFields(): array {
		return $this->requestedFields;
	}
	
	/**
	 * setFields
	 *
	 * @param  array $fields
	 * @return void
	 */
	public function setFields(array $fields = []): void{
		$this->requestedFields = $fields;
	}

	/**
	 * getAddOns
	 *
	 * @return array
	 */
	public function getAddOns(): array {
		return $this->addOnFields;
	}
	
	/**
	 * setAddOns
	 *
	 * @param  array $fields
	 * @return void
	 */
	public function setAddOns(array $fields = []): void{
		$this->addOnFields = $fields;
	}

	
	
	/**
	 * fields
	 *
	 * @param  array $fields
	 * @return DataMuse
	 */
	public function fields(array $fields = []): DataMuse {
		$this->setFields($fields);
		return $this;
	}
	
	/**
	 * getFilter
	 *
	 * @return array
	 */
	public function getFilter(): array {
		return $this->appliedFilters;
	}
	
	/**
	 * setFilter
	 *
	 * @param  mixed $filterType
	 * @param  mixed $filterValue
	 * @return void
	 */
	public function setFilter($filterType, $filterValue): void {
		if(in_array($filterType, self::FILTER_OPTIONS)) {
			$this->appliedFilters[self::OPTIONS[$filterType]] = $filterValue;
		}
	}
	
	/**
	 * filter
	 *
	 * @param  mixed $filters
	 * @return DataMuse
	 */
	public function filter(array $filters = []): DataMuse {
		if(!empty($filters)) {
			foreach($filters as $type=>$filter) {
				$this->setFilter($type, $filter);
			}
		}
		return $this;
	}
	
	/**
	 * getLimit
	 *
	 * @return int
	 */
	public function getLimit(): int {
		return $this->limit;
	}
	
	/**
	 * setLimit
	 *
	 * @param  mixed $limit
	 * @return void
	 */
	public function setLimit(int $limit): void {
		$this->limit = $limit;
	}
	
	/**
	 * limit
	 *
	 * @param  mixed $limit
	 * @return DataMuse
	 */
	public function limit(int $limit): DataMuse {
		if(is_numeric($limit)) {
			$this->setLimit($limit);
		}
		return $this;
	}
	
	/**
	 * addMeaningLike
	 *
	 * @return void
	 */
	private function addMeaningLike(): void {
		$this->addRequest(self::OPTIONS[self::OP_MEANS_LIKE], $this->word);
	}
	
	/**
	 * meanings
	 *
	 * @param  string $word
	 * @return DataMuse
	 */
	public function meanings(): DataMuse {
		$this->addMeaningLike();
		return $this;
	}

	/**
	 * addSoundLike
	 *
	 * @return void
	 */
	private function addSoundLike(): void {
		$this->addRequest(self::OPTIONS[self::OP_SOUND_LIKE], $this->word);
	}
	
	
	/**
	 * homophones : Sound Same
	 *
	 * @return DataMuse
	 */
	public function homophones(): DataMuse {
		$this->addSoundLike();
		return $this;
	}

	
	/**
	 * addTopics
	 *
	 * @return void
	 */
	public function addTopics(): void {
		$this->addRequest(self::OPTIONS[self::OP_ARROUND_TOPIC], $this->word);
	}
	
	/**
	 * associatedTopics
	 *
	 * @return DataMuse
	 */
	public function associatedTopics(): DataMuse {
		$this->addTopics();
		return $this;
	}
	
	/**
	 * addLeftContext
	 *
	 * @return void
	 */
	public function addLeftContext(): void {
		$this->addRequest(self::OPTIONS[self::OP_LEFT_CONTEXT], $this->word);
	}
	
	/**
	 * leftContext
	 *
	 * @return DataMuse
	 */
	public function leftContext(): DataMuse {
		$this->addLeftContext();
		return $this;
	}

	
	/**
	 * addRightContext
	 *
	 * @return void
	 */
	public function addRightContext(): void {
		$this->addRequest(self::OPTIONS[self::OP_RIGHT_CONTEXT], $this->word);
	}
	
	/**
	 * rightContext
	 *
	 * @return DataMuse
	 */
	public function rightContext(): DataMuse {
		$this->addRightContext();
		return $this;
	}

	public function addRelatedParam(string $param): void {
		if(isset(self::RELATED_OPTIONS[$param])){
			$requestKey = self::OPTIONS[self::OP_RELATED_WORD].self::RELATED_OPTIONS[$param];
			$this->addRequest($requestKey, $this->word);
			
			if(array_key_exists($param, self::ADDON_PARAMS)) {
				$this->setAddOns(["tags"=> [self::ADDON_PARAMS[$param]]]);
			}


		}
	}

	public function relatedWords(array $params = ["noun"]): DataMuse {
		if(!empty($params)) {
			foreach($params as $param) {
				$this->addRelatedParam($param);
			}
		}
		return $this;
	}
		
	/**
	 * resetVocab
	 *
	 * @return void
	 */
	private function resetVocab(): void {
		$this->setVocab("");
	}
	
	/**
	 * resetLimit
	 *
	 * @return void
	 */
	private function resetLimit(): void {
		$this->setLimit(self::DEFAULT_LIMIT);
	}
	
	/**
	 * resetRequest
	 *
	 * @return void
	 */
	private function resetRequest(): void {
		$this->clearRequest();
		$this->resetVocab();
		$this->resetLimit();
	}

	public function addToRequest(): Request {
		return $this->makeRequest("GET", self::WORD_ENDPOINT, "", $this->request);
	}
	
	/**
	 * getResults
	 *
	 * @param  mixed $req
	 * @return array
	 */
	public function getResults(Request $req): array {
		$response = $this->executeRequest($req);
		$datamuseResponse = new DataMuseResponse($response);

		$this->resetRequest();
		return $datamuseResponse->extract($this->getFields(), $this->getAddOns(), self::getExcludedWords())->getResult();
	}

	/**
	 * execute
	 *
	 * @return array
	 */
	public function execute(): array {
		$req = $this->addToRequest();
		return $this->getResults($req);
	}

	public static function getRequests(array $requests): Iterator {
		foreach($requests as $key => $request) {
			yield $requests[$key];
		}
	}

	// TODO: Need to Implement in Best Way including getFields params
	public static function batchExecute(array $requests) {
		$client = new Client([
			'http_errors'     => false,
			'connect_timeout' => 3.00, 
			'timeout'         => 10.00
		]);

		$response = Pool::batch($client, $requests);

		return $response;
	}

	// TODO: Need to Implement in Best Way including getFields params
	public static function poolExecute(array $requests) {
		$details = [];
		$client = new Client([
			'http_errors'     => false,
			'connect_timeout' => 5.00, 
			'timeout'         => 10.00
		]);

		$pool = new Pool($client, self::getRequests($requests), [
			'concurrency' => 10,
			'fulfilled' => function ($response, $index) {
				// this is delivered each successful response
				$response = new Response($response->getBody()->getContents(), $response->getStatusCode());
				
				
			},
			'rejected' => function ($reason, $index) {
				// this is delivered each failed request
				$response = new Response("{}", 503);
				return $response;
			},
		]);
		// Initiate the transfers and create a promise
		$promise = $pool->promise();
		// Force the pool of requests to complete.
		return $promise->wait();

	}

	public function getPartsOfSpeech(): DataMuse {
		$this->addRequest(self::OPTIONS[self::OP_META_DATA], "p");
		$this->addRequest(self::OPTIONS[self::OP_SPELL_LIKE], $this->word);
		return $this;
	}

	
	/**
	 * query
	 *
	 * @param  mixed $url
	 * @return array
	 */
	public function query(string $url): array {
		$req = new Request("GET", $url, []);
		return $this->getResults($req);
	}


}

endif;