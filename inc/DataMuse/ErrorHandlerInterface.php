<?php

/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\DataMuse;

interface ErrorHandlerInterface {
    public function getErr($key): string;
    public function setErr($key, $value): void;
    public function actionErr($function, $message, $version): void; 
}

?>