<?php 
/**
 * @package  BusinessNameWordManager
 */
namespace BNWM\Api\Callbacks;

use BNWM\Base\Config;

class AdvertisementCallbacks
{
	protected static $instance = null;
    private $db;
    public $config;

    /**
     * __construct
     *
     * @param  mixed $client
     * @return void
     */
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
        $this->loadAssets();
        $this->config = Config::instance();
    }

    /**
     * instance
     *
     * @return AdvertisementCallbacks
     */
    public static function instance(): AdvertisementCallbacks {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
		}
		return self::$instance;
	}

    public function addAdvertisement($data) {

        $positions = [];
        $positions['top_val']    = $data['top_val'];  
        $positions['bottom_val'] = $data['bottom_val'];  
        $positions['right_val']  = $data['right_val'];  
        $positions['left_val']   = $data['left_val'];  
        

        $id = $this->db->insert( $this->config->getAdvertisementTableName(), [
            'name' => $data['name'],
            'title' => $data['title'],
            'subtitle' => $data['subtitle'],
            'img' => $data['img'],
            'button_caption' => $data['button_caption'],
            'button_link' => $data['button_link'],
            'content' => htmlentities($data['content'], ENT_QUOTES),
            'advertisement_type' => ( isset($data['advertisement_type']) && !empty($data['advertisement_type']) ) ? $data['advertisement_type'] : 'normal',
            'positions' => json_encode($positions),
        ] );

        return $id;

    }

    public function updateAdvertisement($data, $id) {
        
        $positions = [];
        $positions['top_val']    = $data['top_val'];  
        $positions['bottom_val'] = $data['bottom_val'];  
        $positions['right_val']  = $data['right_val'];  
        $positions['left_val']   = $data['left_val'];

        $id = $this->db->update( $this->config->getAdvertisementTableName(), [
            'name' => $data['name'],
            'title' => $data['title'],
            'subtitle' => $data['subtitle'],
            'img' => $data['img'],
            'button_caption' => $data['button_caption'],
            'button_link' => $data['button_link'],
            'content' => htmlentities(stripslashes($data['content']), ENT_QUOTES),
            'advertisement_type' => ( isset($data['advertisement_type']) && !empty($data['advertisement_type']) ) ? $data['advertisement_type'] : 'normal',
            'positions' => json_encode($positions),
        ],[
            'id' => $id
        ] );

    }

    public function getAdvertisements($fields, $where = "") {

        $fieldStr = (is_array($fields) && !empty($fields)) ? implode(", ", $fields) : " * ";
        
        $results = $this->db->get_results("SELECT $fieldStr from ".$this->config->getAdvertisementTableName()." $where" );

		return $results;

    }
    
    public function addAdvertisementScripts() {
        wp_enqueue_script( 'bnwm-generator-script', WORD_MANAGER_PLUGIN_URL.'assets/admin/main.js' );
    }

    /**
     * loadAssets
     */
    public function loadAssets(): void {
        // wp_enqueue_media();
        // echo "ZTest";exit;
        add_action( 'wp_enqueue_scripts', [ $this, 'addAdvertisementScripts' ] );
    }

    public function advertisementPages() {
        
        if(isset($_GET['add_advertisement']) && $_GET['add_advertisement'] == 'true' ){
			if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
				$this->addAdvertisement($_POST);
			}
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/advertisements/add.php" );
		}elseif( isset($_GET['edit_advertisement']) ){
			if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
				$this->updateAdvertisement($_POST, $_GET['edit_advertisement']);
			}
			$advertisements = $this->getAdvertisements('*', 'WHERE id = '.$_GET['edit_advertisement']);
			$advertisement = !empty($advertisements) ? $advertisements[0] : [];
			return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/advertisements/add.php" );
		}else {
            $fields = ['id', 'name', 'advertisement_type', 'content'];
            $advertisements = $this->getAdvertisements($fields);
            return require_once( WORD_MANAGER_PLUGIN_PATH."/templates/advertisements/index.php" );
        }

    }

    public function find($advertisement_id) {

        $advertisements = $this->getAdvertisements('*', 'WHERE id = '.$advertisement_id);
		$advertisement = !empty($advertisements) ? $advertisements[0] : [];
        return $advertisement;

    }


}
