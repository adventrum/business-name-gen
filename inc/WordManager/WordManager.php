<?php

/**
 *
 * @link       http://cybertrontechnologies.com/
 * @since      1.0.0
 */

namespace BNWM\WordManager;

use BNWM\Base\Config;
use BNWM\Base\Pager;
use BNWM\Utility\ArrayUtility;
use Brick\Db\Bulk\BulkInserter;

/**
 * WordManager
 * TODO: Insert Word Function should be stand alone
 */
class WordManager extends DataBaseManager
{
    use ArrayUtility;

    const WT_KEYWORDS = "Keywords",
        WT_CATEGORIES = "Category-Based",
        WT_MATCHING = "Matching-Based",
        WT_GENERAL = "General",
        WT_WITH_ATTR = "Word-With-Attributes",
        WT_CATEGORY = "Category"; 

    const WORD_TYPES = [
        self::WT_KEYWORDS   => 1,
        self::WT_CATEGORIES => 2,
        self::WT_MATCHING   => 3,
        self::WT_GENERAL    => 4,
        self::WT_WITH_ATTR    => 5,
        self::WT_CATEGORY    => 6,
    ];

    const COL_MASCULINE = "masculine",
        COL_FEMININE = "feminine",
        COL_PLURAL = "plural",
        COL_MASCULINE_PLURAL = "masculine_plural",
        COL_FEMININE_PLURAL = "feminine_plural";

    const GENDER_TYPES = [
        self::COL_MASCULINE => self::COL_MASCULINE,
        self::COL_FEMININE => self::COL_FEMININE,
        self::COL_PLURAL => self::COL_PLURAL,
        self::COL_MASCULINE_PLURAL => self::COL_MASCULINE_PLURAL,
        self::COL_FEMININE_PLURAL => self::COL_FEMININE_PLURAL,
    ];

    const REPLACERS = [
        "ment",
        "sh",
        "ic",
        "es",
        "al",
        "ion"
    ];

    public string $old_eng_generator_table = "wp_eng_generator";
    public string $old_wp_synonym_table = "wp_synonym";
    


    public array $searchParams = [];
    public int $limit = 96;
    public int $pageNo = 1;
    public int $length = 100;
    public array $categoryIds = [];
    public string $word = "";
    public string $position = "after";
    public bool $isRhym = false;
    public array $word_types = ["one_word", "two_word"];
    public array $populatedWordIds = [];
    public $populatedCategories = [];
    public array $genderTypes = [];
    public string $separator = " ";
    public string $tld = "";
    public string $wrapTag = "";
    public bool $nameAlgo2 = true;
    public int $numKey;
    public int $oldTotalCount = 0;
    public string $oldLang = "en";
    
    /**
     * loadCategories
     *
     * @return void
     */
    public function loadCategories(string $lang = "en"): void {
        $this->config->setLanguage($lang);
        $csv = new Csv($this->config->getCsvDirectory(), $this->config->getCategoriesCsvFileName());
        $csv->headerStatus(true)
                    ->readCSV();
        $headers  = $csv->getHeader();
        $this->insertCategories($headers);
        $this->updateLoadedStatus($lang, self::WORD_TYPES[self::WT_CATEGORY]);

    }
    
    /**
     * loadCategoryAndMatchingWords
     *
     * @param  mixed $lang
     * @return void
     */
    public function loadCategoryAndMatchingWords(string $lang = "en"): void {
        $this->loadMatchingWords($lang);
        $this->loadCategoryWords($lang);
    }
        
    /**
     * loadGeneralAndKeywords
     *
     * @param  mixed $lang
     * @return void
     */
    public function loadGeneralAndKeywords(string $lang = "en"): void {
        $this->loadGeneralWords($lang);
        $this->loadKeywords($lang);

    }

    /**
     * loadGeneralWords
     *
     * @return void
     */
    public function loadGeneralWords(string $lang = "en"): void {
        $this->config->setLanguage($lang);
        $csv = new Csv($this->config->getCsvDirectory(), $this->config->getCategoryWordCSVFilename());
        $data = $csv->headerStatus(true)
                    ->specialHeaders([1])
                    ->excludeSpecialHeaderStatus(false)
                    ->readCSV();
        $headers  = $csv->getHeader();
        $columns = [
            "name" => null,
            "selected_keyword" => 0,
            "status" => "Pending",
        ];
        $this->insertWords($data, $headers, [], self::WORD_TYPES[self::WT_GENERAL], $columns);
        $this->updateLoadedStatus($lang, self::WORD_TYPES[self::WT_GENERAL]);
    }
    
    /**
     * loadMatchingWords
     *
     * @return void
     */
    public function loadMatchingWords(string $lang = "en"): void {
        $this->config->setLanguage($lang);
        $csv = new Csv($this->config->getCsvDirectory(), $this->config->getMatchingWordCSVFilename());
        $data = $csv->headerStatus(true)
                    ->readCSV();
        $headers  = $csv->getHeader();
        $this->insertCategories($headers);
        $categories = $this->getCategories();
        $columns = [
            "name" => null,
            "selected_keyword" => 0,
            "status" => "Pending",
        ];
        $this->insertWords($data, $headers, $categories, self::WORD_TYPES[self::WT_MATCHING], $columns);
        $this->updateLoadedStatus($lang, self::WORD_TYPES[self::WT_MATCHING]);
    }

    /**
     * loadKeywords
     *
     * @return void
     */
    public function loadKeywords(string $lang = "en"): void {
        $this->config->setLanguage($lang);
        $csv = new Csv($this->config->getCsvDirectory(), $this->config->getKeywordCSVFilename());
        $data = $csv->readCSV();
        $fHeaders = $csv->getFakeHeader();
        $columns = [
            "name" => null,
            "selected_keyword" => 1,
            "status" => "Pending",
        ];

        $this->insertWords($data, $fHeaders, [], self::WORD_TYPES[self::WT_KEYWORDS], $columns);
        $this->updateLoadedStatus($lang, self::WORD_TYPES[self::WT_KEYWORDS]);

    }    

    /**
     * loadCategoryWords
     *
     * @return void
     */
    public function loadCategoryWords(string $lang = "en"): void {
        $this->config->setLanguage($lang);
        $csv = new Csv($this->config->getCsvDirectory(), $this->config->getCategoryWordCSVFilename());
        $data = $csv->headerStatus(true)
                    ->specialHeaders([0, 1])
                    ->excludeSpecialHeaderStatus(true)
                    ->readCSV();
        $headers  = $csv->getHeader();
        $this->insertCategories($headers);
        $categories = $this->getCategories();
        $columns = [
            "name" => null,
            "selected_keyword" => 0,
            "status" => "Pending",
        ];
        $this->insertWords($data, $headers, $categories, self::WORD_TYPES[self::WT_CATEGORIES], $columns);
        $this->updateLoadedStatus($lang, self::WORD_TYPES[self::WT_CATEGORIES]);
    }


    public function loadAttributes(string $lang = "en"): void {
        $this->config->setLanguage($lang);
        $csv = new Csv($this->config->getCsvDirectory(), $this->config->getAttributeFilename());
        $data = $csv->headerStatus(true)
                    ->readCSV();
        $attributes = $csv->getHeader();

        $columns = [
            "name" => null,
            "selected_keyword" => 0,
            "status" => "Pending",
        ];
        $this->insertWords($data, ["keywords"], [], self::WORD_TYPES[self::WT_WITH_ATTR], $columns);

        
        if(!empty($data)) {
            foreach(array_chunk($data, 100) as $dt) {
                $sql = "UPDATE ".$this->config->getWordDictionaryTableName()." T1 INNER JOIN ".$this->config->getLanguageWordTableName()." T2 ON T1.id = T2.word_id SET ";
                $use_before = [];
                $use_after = [];
                $before_after = [];
                $use_end_only = [];
                $masculine = [];
                $feminine = [];
                $plural = [];
                $masculine_plural = [];
                $feminine_plural = [];
                $names = [];

                foreach($dt as $position=>$row) {
                    $name = strtolower(trim($row[0]));
                    $names[] = $name;

                    $use_before[]               = ' WHEN "'.$name.'" THEN "'.$row[1].'" ';
                    $use_after[]                = ' WHEN "'.$name.'" THEN "'.$row[2].'" ';
                    $before_after[]             = ' WHEN "'.$name.'" THEN "'.$row[3].'" ';
                    $use_end_only[]             = ' WHEN "'.$name.'" THEN "'.$row[4].'" ';
                    $masculine[]                = ' WHEN "'.$name.'" THEN "'.$row[5].'" ';
                    $feminine[]                 = ' WHEN "'.$name.'" THEN "'.$row[6].'" ';
                    $plural[]                   = ' WHEN "'.$name.'" THEN "'.$row[7].'" ';
                    $masculine_plural[]         = ' WHEN "'.$name.'" THEN "'.$row[8].'" ';
                    $feminine_plural[]          = ' WHEN "'.$name.'" THEN "'.$row[9].'" ';
                }

                $sql .= " T2.use_before = CASE T1.name ".implode(" ", $use_before)." END, ";
                $sql .= " T2.use_after = CASE T1.name ".implode(" ", $use_after)." END, ";
                $sql .= " T2.before_after = CASE T1.name ".implode(" ", $before_after)." END, ";
                $sql .= " T2.use_end_only = CASE T1.name ".implode(" ", $use_end_only)." END, ";
                $sql .= " T2.masculine = CASE T1.name ".implode(" ", $masculine)." END, ";
                $sql .= " T2.feminine = CASE T1.name ".implode(" ", $feminine)." END, ";
                $sql .= " T2.plural = CASE T1.name ".implode(" ", $plural)." END, ";
                $sql .= " T2.masculine_plural = CASE T1.name ".implode(" ", $masculine_plural)." END, ";
                $sql .= " T2.feminine_plural = CASE T1.name ".implode(" ", $feminine_plural)." END ";
                $namesString = implode('", "', $names);
                $sql .= ' WHERE T1.name IN ("'.$namesString.'") ';

                $this->db->query($sql);

            }
        }

        $this->updateLoadedStatus($lang, self::WORD_TYPES[self::WT_WITH_ATTR]);


    }
    
    /**
     * updateLoadedStatus
     *
     * @param  mixed $lang
     * @param  mixed $word_type_id
     * @return void
     */
    public function updateLoadedStatus(string $lang = "en", int $word_type_id) {
        $updateQuery = "UPDATE ".$this->config->getWordTypeTableName()." SET loaded_languages = if(find_in_set('".$lang."', loaded_languages), loaded_languages, CONCAT_WS(',', loaded_languages, '".$lang."') ) WHERE id = ".$word_type_id;
        $this->db->query($updateQuery);
    }
    
    /**
     * getLoadedLanguages
     *
     * @param  mixed $word_type_ids
     * @return array
     */
    public function getLoadedLanguages(array $word_type_ids = []): array {
        $wordIds = implode(", ", $word_type_ids);
        $getQuery = "SELECT name FROM ".$this->config->getLanguageTableName()." WHERE 
        find_in_set(code, (SELECT GROUP_CONCAT(loaded_languages) FROM ".$this->config->getWordTypeTableName()." WHERE id IN (".$wordIds.")) );";
        return $this->db->get_results($getQuery, ARRAY_A);
    } 
    
    /**
     * insertCategories
     *
     * @param  mixed $categories
     * @return void
     */
    public function insertCategories(array $categories): void {
        $inserter = new BulkInserter($this->pdo, $this->config->getWordCategoriesTableName(), ['name'], 1000);
        if(!empty($categories)) {
            foreach($categories as $name) {
                $inserter->queue(strtolower(trim($name)));
            }
            $inserter->flush();
        }

        $names = implode('", "', $categories);
        $this->insertLanguageCategoryAssociation($names, $this->config->getLanguage());
    }
    
    /**
     * getCategories
     *
     * @return array
     */
    public function getCategories(): array {
        $cats = $this->db->get_results("SELECT id, name FROM ".$this->config->getWordCategoriesTableName(), ARRAY_A);
        $categories = [];
        if(!empty($cats)){
            foreach($cats as $category){
                $categories[$category['id']] = $category['name'];
            }
        }
        return $categories;
    }

    /**
     * getActiveCategories
     *
     * @return array
     */
    public function getActiveCategories(): array {
        $cats = $this->db->get_results("SELECT id, name FROM ".$this->config->getWordCategoriesTableName()." WHERE status ='Active' ", ARRAY_A);
        $categories = [];
        if(!empty($cats)){
            foreach($cats as $category){
                $categories[$category['id']] = $category['name'];
            }
        }
        return $categories;
    }
    
    /**
     * arrangeData
     *
     * @param  array $data
     * @param  array $headers
     * @param  array $categories
     * @param  int $word_type_id
     * @return array
     */
    public function arrangeData(array $data, array $headers, array $categories, int $word_type_id): array {
        $result = [];
        if(!empty($headers)) {
            foreach($headers as $position => $header_category) {
                $category_id = array_search($header_category, $categories);
                $result[$position]["category_id"] = $category_id;
                $result[$position]["category_name"] = $header_category;
                $result[$position]["word_type_id"] = $word_type_id;
                $result[$position]["language_code"] = $this->config->getLanguage();
                foreach($data as $word){
                    $cleanWord = strtolower(trim($word[$position]));
                    if($cleanWord != "")
                        $result[$position]["words"][] = $cleanWord;
                }
            }
        }
        return $result;
    }
    
    /**
     * insertOnlyWords
     *
     * @param  array $words
     * @return void
     */
    public function insertOnlyWords(array $words, array $columns = ['name'=>null, 'selected_keyword'=> 0, 'status'=>"Pending"]): void {
        $inserter = new BulkInserter($this->pdo, $this->config->getWordDictionaryTableName(), array_keys($columns), 1000);
        if(!empty($words)) {
            foreach($words as $word) {
                $inserter->queue(
                    $this->cleanData("name", $columns, trim($word)), 
                    $this->cleanData("selected_keyword", $columns, 0),
                    $this->cleanData("status", $columns, "Pending"),
                );
            }
            $inserter->flush();
        }
    }

    public function cleanData($key, $dict, $realValue) {
        return (isset($dict[$key]) && !empty($dict[$key])) ? $dict[$key] : $realValue;
    }
    
    /**
     * insertOnlyWordsTypeAssociation
     *
     * @param  string $names
     * @param  int $word_type_id
     * @return void
     */
    public function insertOnlyWordsTypeAssociation(string $names, int $word_type_id): void {
        $sql = ' INSERT INTO '.$this->config->getWordTypeAssocTableName().' (word_id, word_type_id) SELECT wdt.id, '.$word_type_id.' FROM '.$this->config->getWordDictionaryTableName().' wdt where wdt.id NOT IN ( SELECT wta.word_id FROM '.$this->config->getWordDictionaryTableName().' wd INNER JOIN '.$this->config->getWordTypeAssocTableName().' wta on wta.word_id = wd.id where wd.name IN ("'.$names.'") AND wta.word_type_id = '.$word_type_id.' ) AND wdt.name IN ("'.$names.'"); ';
        // Word Type Association
        $this->db->query($sql);
    }
    
    /**
     * insertOnlyCategoryAssociation
     *
     * @param  string $names
     * @param  int $category_id
     * @return void
     */
    public function insertOnlyCategoryAssociation(string $names, int $category_id): void {
        if($category_id) {
            $sql = ' INSERT INTO '.$this->config->getWordCatAssocTableName().' (word_id, category_id) SELECT wdt.id, '.$category_id.' FROM '.$this->config->getWordDictionaryTableName().' wdt where wdt.id NOT IN ( SELECT wca.word_id FROM '.$this->config->getWordDictionaryTableName().' wd INNER JOIN '.$this->config->getWordCatAssocTableName().' wca on wca.word_id = wd.id where wd.name IN ("'.$names.'") AND wca.category_id = '.$category_id.' ) AND wdt.name IN ("'.$names.'"); ';
            // Word Category Association
            $this->db->query($sql);
        }
    }


    /**
     * insertLanguageCategoryAssociation
     *
     * @param  string $names
     * @param  string $language_code
     * @return void
     */
    public function insertLanguageCategoryAssociation(string $names, string $language_code = "en"): void {
        if($language_code) {
            $sql = ' INSERT INTO '.$this->config->getLanguageCategoryTableName().' (category_id, language_code) SELECT category.id, "'.$language_code.'" FROM '.$this->config->getWordCategoriesTableName().' category where category.id NOT IN ( SELECT lca.category_id FROM '.$this->config->getWordCategoriesTableName().' cat INNER JOIN '.$this->config->getLanguageCategoryTableName().' lca on lca.category_id = cat.id where cat.name IN ("'.$names.'") AND lca.language_code = "'.$language_code.'" ) AND category.name IN ("'.$names.'"); ';
            // Word Category Association
            $this->db->query($sql);
        }
    }

    /**
     * insertLanguageWordAssociation
     *
     * @param  string $names
     * @param  string $language_code
     * @return void
     */
    public function insertLanguageWordAssociation(string $names, string $language_code = "en"): void {
        if($language_code) {
            $sql = ' INSERT INTO '.$this->config->getLanguageWordTableName().' (word_id, language_code) SELECT word.id, "'.$language_code.'" FROM '.$this->config->getWordDictionaryTableName().' word where word.id NOT IN ( SELECT lwa.word_id FROM '.$this->config->getWordDictionaryTableName().' wo INNER JOIN '.$this->config->getLanguageWordTableName().' lwa on lwa.word_id = wo.id where wo.name IN ("'.$names.'") AND lwa.language_code = "'.$language_code.'" ) AND word.name IN ("'.$names.'"); ';
            // Word Category Association
            $this->db->query($sql);
        }
    }

    /**
     * insertSingleWordBatch
     *
     * @param  array $list
     * @return void
     */
    public function insertSingleWordBatch(array $list, array $columns): void {
        $words = $this->arrayWithDefault($list, "words", []);
        $this->insertOnlyWords($words, $columns);
        $names = implode('", "', $words);
        $this->insertOnlyWordsTypeAssociation($names, $this->arrayWithDefault($list, "word_type_id", 0));
        $this->insertOnlyCategoryAssociation($names, $this->arrayWithDefault($list, "category_id", 0));
        $this->insertLanguageWordAssociation($names, $this->arrayWithDefault($list, "language_code", "en")); 
    }
    
    /**
     * insertWords
     *
     * @param  array $data
     * @param  array $headers
     * @param  array $categories
     * @param  int $word_type_id
     * @return void
     */
    public function insertWords(array $data, array $headers, array $categories, int $word_type_id, array $columns): void {
        $arrangedWords = $this->arrangeData($data, $headers, $categories, $word_type_id);
        if(!empty($arrangedWords)) {
            foreach($arrangedWords as $list) {
                $this->insertSingleWordBatch($list, $columns);
            }
        }
    }
    
    

    private function syncWordTypeForKeywords() {
        $sql = "INSERT INTO ".$this->config->getWordTypeAssocTableName()." (word_id, word_type_id)SELECT id, 1 FROM ".$this->config->getWordDictionaryTableName()." wd
        WHERE NOT EXISTS (
            SELECT assoc.id FROM ".$this->config->getWordTypeAssocTableName()." assoc INNER JOIN ".$this->config->getWordDictionaryTableName()." dict ON assoc.word_id = dict.id WHERE assoc.word_type_id = 1
        ) AND wd.selected_keyword = 1;";

        $this->db->query($sql);

    }

    public function addNewKeys(&$array, $newKeyValues) {
        if(!empty($array)) {
            foreach($array as &$element) {
                foreach($newKeyValues as $key=>$value) {
                    $element[$key] = $value; 
                }
            }
        }
    }

    public function getLanguages(): array {
        $langs = $this->db->get_results("SELECT code, name FROM ".$this->config->getLanguageTableName(), ARRAY_A);
        $languages = [];
        if(!empty($langs)){
            foreach($langs as $language){
                $languages[$language['code']] = $language['name'];
            }
            asort($languages);
        }
        return $languages;
    }


    public function setSearchParams(array $params) {
        $this->searchParams = $params;
    }

    public function queryParams(array $params): WordManager {
        $this->setSearchParams($params);
        return $this;
    }

    public function getLimitAndOffsetClause(): string {
        $limit = !empty($this->searchParams["limit"]) ? $this->searchParams["limit"] : 100;
        return "";
    }

    public function getQueriedWord(): string {
        return $this->searchParams["word"];
    }

    public function setLimit(int $limit) {
        $this->limit = $limit;
    }

    public function getLimit(): int {
        return $this->limit;
    }

    public function setPage(int $pageNo) {
        $this->pageNo = $pageNo;
    }

    public function getPage(): int {
        return $this->pageNo;
    }

    public function limit(int $limit): WordManager {
        $this->setLimit($limit);
        return $this;
    }

    public function page(int $page) : WordManager {
        $this->setPage($page);
        return $this;
    }

    public function getOffset() : int {
        return $this->getLimit() * ($this->getPage() - 1);
    }

    public function setLength(int $length) {
        $this->length = $length;
    }

    public function getlength(): int {
        return $this->length;
    }

    public function length(int $length): WordManager {
        $this->setLength($length);
        return $this;
    }

    public function setCategoryIds(array $categoryIds): void {
        $this->categoryIds = $categoryIds;
    }

    public function getCategoryIds(): array {
        return $this->categoryIds;
    }

    public function categoryIds(array $categoryIds): WordManager {
        $this->setCategoryIds($categoryIds);
        return $this; 
    }

    public function setWord(string $word): void {
        $this->word = $word;
    }

    public function getWord(): string {
        return strtolower(trim($this->word));
    }

    private function sanitizeWord(string $word) {
        $word = str_replace(' ','', $word);
        $word = str_replace('www.','', $word);
        $word = str_replace('http://','', $word);
        $word = str_replace('https://','', $word);

        if(stripos($word, ',')){
            $word = str_replace(',', ' ', $word);
            $word = str_replace(', ', ' ', $word);
        }if(stripos($word, '.com')){
            $word = str_replace('.com', '', $word);
        }if(stripos($word, '.org')){
            $word = str_replace('.org', '', $word);
        }
        
        $word = preg_replace('/[^a-zA-Z0-9-\s-]/','', $word);

        return $word;
    }

    public function word(string $word): WordManager {
        $word = $this->sanitizeWord($word);
        $this->setWord($word);
        return $this;
    }

    public function setPosition(string $position): void {
        $this->position = $position;
    }
    
    public function getPosition(): string{
        return $this->position;
    }

    public function position(string $position) : WordManager {
        $this->setPosition($position);
        return $this;
    }

    public function setIsRhym(bool $isRhym): void {
        $this->isRhym = $isRhym;
    }

    public function getIsRhym(): bool {
        return $this->isRhym;
    }

    public function isRhym(bool $rhym): WordManager {
        $this->setIsRhym($rhym);
        return $this;
    }

    public function setWordTypes(array $word_types) : void {
        $this->word_types = $word_types;
    }

    public function getWordTypes(): array {
        return $this->word_types;
    }

    public function wordTypes(array $word_types): WordManager {
        $this->setWordTypes($word_types);
        return $this;
    }

    public function getGenderTypes(): array {
        return $this->genderTypes;
    }

    public function setGenderTypes(array $genderTypes): void {
        $this->genderTypes = $genderTypes;
    }

    public function genderFilter(array $genderTypes): WordManager {
        $this->setGenderTypes($genderTypes);
        return $this;
    }

    public function getSeparator(): string {
        return $this->separator;
    }

    public function setSeparator(string $separator): void {
        $this->separator = $separator;
    }

    public function separator(string $separator): WordManager {
        $this->setSeparator($separator);
        return $this;
    }

    public function getTld(): string {
        return $this->tld;
    }

    public function setTld(string $tld): void {
        $this->tld = $tld;
    }

    public function tld(string $tld): WordManager {
        $this->setTld($tld);
        return $this;
    }

    public function getTldString(): string {
        return (!empty($this->getTld())) ? ".".$this->getTld() : "";
    }

    public function isNameAlgo2(): bool {
        return $this->nameAlgo2;
    }

    public function setNameAlgo2(bool $status ): void {
        $this->nameAlgo2 = $status;
    }

    public function nameIdeaAlgo2(bool $status = true): WordManager {
        $this->setNameAlgo2($status);
        return $this; 
    }

    public function getWrapTag(): string {
        return $this->wrapTag;
    }

    public function getStartWrapTag(): string {
        return (!empty($this->getWrapTag())) ? "<".$this->getWrapTag().">" : "";
    }

    public function getEndWrapTag(): string {
        return (!empty($this->getWrapTag())) ? "</".$this->getWrapTag().">" : "";
    }

    public function setWrapTag(string $tag): void {
        $this->wrapTag = $tag;
    }

    public function wrapTag(string $tag): WordManager {
        $this->setWrapTag($tag);
        return $this;
    }

    public function enhanceWord(string $word) {
        $word = ucwords($word);
        return $word;
    }


    public function getPopulatedWordIds(): array {
        return $this->populatedWordIds;
    }

    public function setPopulatedWordIds(array $populatedWordIds): void {
        $this->populatedWordIds = $populatedWordIds;
    }

    public function getPopulatedCategories(): array {

        if(!$this->isNameAlgo2()) {
            // old Version
            if(isset($_SESSION['filterd_columns']) && $_SESSION['filterd_columns']) {
                $cats = $this->getActiveCategories();
                foreach($_SESSION['filterd_columns'] as  $key=>$catname) {
                    if(!in_array($catname, $cats)) {
                        unset($_SESSION['filterd_columns'][$key]);
                    }
                }

                return $_SESSION['filterd_columns'];
            }
            return [];
        }


        if(!empty($this->populatedCategories)) {
            return $this->populatedCategories;
        }

        $wordIds = $this->getPopulatedWordIds();
        $populatedCategories = [];
        if(!empty($wordIds)) {
            $wordIdsStr = implode(",", $wordIds);
            $sql = " SELECT DISTINCT WCA.category_id AS category_ids FROM ".$this->config->getWordCatAssocTableName()." WCA WHERE WCA.word_id IN (".$wordIdsStr.") ";
            $data = $this->db->get_results($sql, ARRAY_A);
            $data = $this->extractArrayOut($data);
            $populatedCategories =  $data;
        }
        $this->populatedCategories = $populatedCategories;
        return $populatedCategories;

    }

    public function getOldTotalCount() : int {
        return $this->oldTotalCount;
    }

    public function setOldTotalCount(int $count): void {
        $this->oldTotalCount = $count;
    }

    public function getOldLanguage(): string {
        return $this->oldLang;
    }

    public function setOldLanguage(string $lang = "en"): void {
        $this->oldLang = $lang;
    }

    public function oldLang(string $lang = "en"): WordManager {
        $this->setOldLanguage($lang);
        return $this;
    }

    public function getSearchResults() {

        $wordTypes = $this->getWordTypes(); // One Word, Two Word
        $word = $this->getWord();
        $genderT = $this->getGenderTypes();

        if(!$this->isNameAlgo2()) {
            // old Version
            $data["words"] = $this->oldNameAlgoLogic();
            return $data;

        }

        // When No Word Provided, Then get General List
        if(empty($word)) {
            $finalGenList = $this->generalList();
            $finalList = $finalGenList["words"];
            return $this->buildResult($finalList);
            // return $finalList;
        }

        if(in_array('one_word', $wordTypes) && count($wordTypes) == 1) {
            // Only Suffix or Prefix
            return $this->getSuffixOrPrefixResults();
            exit;
        }

        // Masculine etc
        if(!empty($genderT) && array_key_exists($genderT[0], self::GENDER_TYPES)) {
            return $this->getGenderResults();
        }

        return $this->basicStrategy();

    }

    public function getTotalCount() {

        if(!$this->isNameAlgo2()) {
            // old Version 
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $data["words"] = $this->oldNameAlgoLogic(); // with ajax
            }
            return $this->getOldTotalCount();
        }

        $word = $this->getWord();
        $wordTypes = $this->getWordTypes(); // One Word, Two Word
        // When No Word Provided, Then get General List
        if(empty($word)) {
            $finalGenList = $this->generalList();
            $count = $finalGenList["total_count"];
            return $count;
        }
        
        if(in_array('one_word', $wordTypes) && count($wordTypes) == 1) {
            // Only Suffix or Prefix
            return $this->getSuffixOrPrefixTotalResults();
            exit;
        }
        // Masculine etc
        if(!empty($genderT) && array_key_exists($genderT[0], self::GENDER_TYPES)) {
            return $this->getGenderResults(true);
        }
        
        return $this->basicStrategyCount();
    }

    public function getSuffixOrPrefixTotalResults() {
        $word = $this->getWord();

        $replacers = self::REPLACERS;

        foreach($replacers as $replace) {
            if($this->stringEndsWith($word, $replace)) {
                $word = $this->stringReplaceAtEnd($word, $replace, "");
                $this->setWord($word);
                break;
            }
        }

        $WhereConcat = "CONCAT('".$word."', WD.name)";
        $concat = "CONCAT('".$word."', '".$this->getStartWrapTag()."',  WD.name, '".$this->getEndWrapTag()."' )";

        $countSql = " SELECT COUNT(".$concat.") as total_count FROM ".$this->config->getWordDictionaryTableName()." WD WHERE WD.id IN ( SELECT WL.word_id FROM ".$this->config->getWordTypeAssocTableName()." WTA INNER JOIN ".$this->config->getLanguageWordTableName()." WL ON WL.word_id = WTA.word_id WHERE WTA.word_type_id = ".self::WORD_TYPES[self::WT_WITH_ATTR] ." AND WL.use_end_only IS NOT NULL ) AND LENGTH(".$WhereConcat.") <= ".$this->getlength()." limit ".$this->getOffset().", ".$this->getLimit();

        $totalCount = $this->db->get_results($countSql, ARRAY_A);
        $totalCount = $this->extractArrayOut($totalCount, true);

        // if($totalCount["total_count"] == 0) {
        //     $totalCount = $this->generalList();
        // }

        return $totalCount["total_count"];

    }

    private function stringReplaceAtEnd(string $str, string $endswith = '', string $replace = ''): string {
        $length = strlen($endswith);
        $newStr = substr_replace($str, $replace, -$length);
        return $newStr;
    }

    private function stringEndsWith(string $str, string $endswith = ''): bool {

        $length = strlen($endswith);

        return substr_compare($str, $endswith, -$length) === 0; 
    }
 
    public function getSuffixOrPrefixResults() {

        $word = $this->getWord();
        $replacers = self::REPLACERS;

        foreach($replacers as $replace) {
            if($this->stringEndsWith($word, $replace)) {
                $word = $this->stringReplaceAtEnd($word, $replace, "");
                $this->setWord($word);
                break;
            }
        }


        // Currently For Suffix
        // TODO: Set for Prefix ??
        // Separator will not work here
        $tldString = $this->getTldString();
        $concat = "CONCAT('".$word."', '".$this->getStartWrapTag()."', WD.name, '".$this->getEndWrapTag()."', '".$tldString."')";
        $WhereConcat = "CONCAT('".$word."', WD.name)";
        $sql = " SELECT ".$concat." as word FROM ".$this->config->getWordDictionaryTableName()." WD WHERE WD.id IN ( SELECT WL.word_id FROM ".$this->config->getWordTypeAssocTableName()." WTA INNER JOIN ".$this->config->getLanguageWordTableName()." WL ON WL.word_id = WTA.word_id WHERE WTA.word_type_id = ".self::WORD_TYPES[self::WT_WITH_ATTR] ." AND WL.use_end_only IS NOT NULL ) AND LENGTH(".$WhereConcat.") <= ".$this->getlength()." limit ".$this->getOffset().", ".$this->getLimit();

        $words = $this->db->get_results($sql, ARRAY_A);
        $words = $this->extractArrayOut($words);
        $words = array_map(array($this, "enhanceWord"), $words);

        // if(empty($words)) {
        //     $this->setSeparator("");
        //     $result = $this->generalList();
        //     $words = $result["words"];
        // }
        
        return $this->buildResult($words);
    }

    public function basicStrategyCount() {
        return $this->basicStrategy(true);
    }

    public function getGenderResults($getCount = false) {
        $word = $this->getWord();

        $genderT = $this->getGenderTypes();
        $condt = "";
        if(!empty($genderT)) {
            foreach($genderT as $k=>$g) {
                if($k == 0) {
                    $condt .= "( WLA.".$g." IS NOT NULL ";    
                }else{
                    $condt .= " OR WLA.".$g." IS NOT NULL ";
                }
            }
            $condt .= " ) ";
        }

        $categoryBasedIds = $this->getCategoryIds();
        $categoryCondition = " ";
        if(!empty($categoryBasedIds)) {
            $categoryIdsStr = implode(", ", $categoryBasedIds);
            $categoryCondition = " AND WCA.category_id IN (".$categoryIdsStr.") "; 
        }

        $sql = "SELECT WLA.*, WD.name as word FROM ".$this->config->getLanguageWordTableName()." WLA INNER JOIN ".$this->config->getWordDictionaryTableName()." WD ON WD.id = WLA.word_id LEFT JOIN ".$this->config->getWordCatAssocTableName()." WCA ON WCA.word_id = WD.id WHERE 1=1 AND ".$condt." " .$categoryCondition." LIMIT 10000";
        // Get all Words
        $rows = $this->db->get_results($sql, ARRAY_A);
        $words = [];
        if(!empty($rows)) {
            foreach($rows as $row) {
                $words[] = $row["word"];
                if(in_array(self::COL_MASCULINE, $genderT) && !empty($row[self::COL_MASCULINE])) {
                    $words[] = $row[self::COL_MASCULINE];
                }
                if(in_array(self::COL_FEMININE, $genderT) && !empty($row[self::COL_FEMININE])) {
                    $words[] = $row[self::COL_FEMININE];
                }
                if(in_array(self::COL_PLURAL, $genderT) && !empty($row[self::COL_PLURAL])) {
                    $words[] = $row[self::COL_PLURAL];
                }
                if(in_array(self::COL_MASCULINE_PLURAL, $genderT) && !empty($row[self::COL_MASCULINE_PLURAL])) {
                    $words[] = $row[self::COL_MASCULINE_PLURAL];
                }
                if(in_array(self::COL_FEMININE_PLURAL, $genderT) && !empty($row[self::COL_FEMININE_PLURAL])) {
                    $words[] = $row[self::COL_FEMININE_PLURAL];
                }
            }
        }
        $uniqueWords = array_values(array_unique($words));
        unset($words);
        $finalList = [];
        for($i = 0; $i < count($uniqueWords); $i++ ) {
            $single_word = $uniqueWords[$i];
            // Remove Searched Word as It should not be the part of concated string and That don't meet length
            if((strlen($single_word) > $this->getlength()) || $single_word == $word ) {
                unset($uniqueWords[$i]);
                continue;
            }
            // Replace Spaces
            $single_word = str_replace(" ", "", $single_word);
            // Append Before or After
            $single_word = $this->enhanceWord( (!empty($position) && $position == "before") ? ($word . $this->getSeparator() . $this->getStartWrapTag(). $single_word . $this->getEndWrapTag() ): ($single_word.$this->getSeparator(). $this->getStartWrapTag(). $word. $this->getEndWrapTag() ) ); 
            
            // Tld Added
            $tldString = $this->getTldString();
            $single_word .= $tldString;
            // Add to the Final List
            $finalList[] = $single_word;
            if(count($finalList) >= ($this->getLimit() +  $this->getOffset()) && !$getCount ) {
                break;
            }
        }

        // Placed All words if No Result Found
        if(empty($finalList)) {
            $finalGenList = $this->generalList();
            $finalList = $finalGenList["words"];
            if($getCount) {
                return $finalGenList["total_count"];
            }else {
                return $this->buildResult($finalGenList["words"]);
            }
        }

        $requiredWords = array_slice($finalList, $this->getOffset(), $this->getLimit());
        if($getCount) {
            return count($finalList);
        }
        // Free Memory
        unset($uniqueWords);
        unset($finalList);

        if(!empty($categoryWordIds)) {
            $this->setPopulatedWordIds($categoryWordIds);
        }

        return $this->buildResult($requiredWords);
        
    }

    public function basicStrategy($getCount = false) {

        $word = $this->getWord();
        $position = $this->getPosition();
        $rhym = $this->getIsRhym();
        $categoryBasedIds = $this->getCategoryIds();

        // Either Synonyms or Rhymes
        $wordEffectiveness = (!empty($rhym)) ? "homophones" : "meanings";
        // Either Before Or After; Before -> Nouns, After -> Adjectives
        $positionEffectiveness = (!empty($position) && $position == "before") ?  "nouns": "adjectives";
        // Associated Topics
        $associatedTopics = "associated_topics";

        $categoryCondition = " ";
        if(!empty($categoryBasedIds)) {
            $categoryIdsStr = implode("', '", $categoryBasedIds);
            $categoryCondition = " AND WCA.category_id IN ('".$categoryIdsStr."') "; 
        }

        $categoriesSum = array_sum($categoryBasedIds);
        $consistentNo = $categoriesSum % 30;
        $newLimit = 500 + $consistentNo;

        // New Start

        $baseResultQ = " SELECT WD.id, WD.name, ".$wordEffectiveness."->'$[*].word' as wordEffectiveness, ".$positionEffectiveness."->'$[*].word' as positionEffectiveness, ".$associatedTopics."->'$[*].word' as associatedTopics  FROM ".$this->config->getWordDictionaryTableName()." WD LEFT JOIN ".$this->config->getWordCatAssocTableName()." WCA ON WCA.word_id = WD.id WHERE WD.name = '".$word."' ".$categoryCondition."  LIMIT 1";
        // Base Dictionaries
        $baseDictionaries = $this->db->get_results($baseResultQ, ARRAY_A);

        // Combined Query
        // $sql = " SELECT WD.id, WD.name, ".$wordEffectiveness."->'$[*].word' as wordEffectiveness, ".$positionEffectiveness."->'$[*].word' as positionEffectiveness, ".$associatedTopics."->'$[*].word' as associatedTopics  FROM ".$this->config->getWordDictionaryTableName()." WD LEFT JOIN ".$this->config->getWordCatAssocTableName()." WCA ON WCA.word_id = WD.id WHERE (WD.name = '".$word."' OR ".$wordEffectiveness."->'$[*].word' LIKE '%".$word."%' OR ".$positionEffectiveness."->'$[*].word' LIKE '".$word."%' OR ".$associatedTopics."->'$[*].word' LIKE '".$word."%' )  ".$categoryCondition." group by WD.id ORDER BY WD.name='".$word."' DESC  LIMIT ".$newLimit;

        $sql = " SELECT WD.id, WD.name, ".$wordEffectiveness."->'$[*].word' as wordEffectiveness, ".$positionEffectiveness."->'$[*].word' as positionEffectiveness, ".$associatedTopics."->'$[*].word' as associatedTopics  FROM ".$this->config->getWordDictionaryTableName()." WD LEFT JOIN ".$this->config->getWordCatAssocTableName()." WCA ON WCA.word_id = WD.id WHERE WD.name != '".$word."' AND (  ".$wordEffectiveness."->'$[*].word' LIKE '%".$word."%' OR ".$positionEffectiveness."->'$[*].word' LIKE '%".$word."%' OR ".$associatedTopics."->'$[*].word' LIKE '%".$word."%' )  ".$categoryCondition." group by WD.id LIMIT ".$newLimit;
        // Get all Words
        $dictionaries = $this->db->get_results($sql, ARRAY_A);
        if(!empty($baseDictionaries)) {
            $dictionaries = array_merge($baseDictionaries, $dictionaries);
        }
        $words= [];
        $categoryWordIds = [];
        foreach($dictionaries as $row) {
            $wordEffectivenessData = json_decode($row["wordEffectiveness"])?:[];
            // If rhyming On No need to get Position Data like Noun or Adjective
            if(empty($this->getIsRhym())) {
                $positionEffectivenessData = json_decode($row["positionEffectiveness"])?:[];
            }else {
                $positionEffectivenessData = [];
            }
            $associatedTopicsData = json_decode($row["associatedTopics"])?:[];

            $isValid = true;
            if(!in_array($word, $wordEffectivenessData) && !in_array($word, $associatedTopicsData) && !in_array($word, $positionEffectivenessData)) {
                $isValid = false;
            }
            if($word == $row["name"]) {
                $isValid = true;
            }

            if($isValid){
                // All Words Set
                $words = array_merge($words, $wordEffectivenessData, $positionEffectivenessData, $associatedTopicsData, [$row["name"]]);

                // Category Word Id used to extract Selected Category
                if(empty($categoryBasedIds)) {
                    $categoryWordIds[] = $row["id"];
                }
            }
                
        }

        // Free Memory
        unset($dictionaries);
        // Remove Duplicate Words
        $uniqueWords = array_values(array_unique($words));
        // Free Memory
        unset($words);
        $finalList = [];
        for($i = 0; $i < count($uniqueWords); $i++ ) {
            $single_word = $uniqueWords[$i];
            // Remove Searched Word as It should not be the part of concated string and That don't meet length
            if((strlen($single_word) > $this->getlength()) || $single_word == $word ) {
                unset($uniqueWords[$i]);
                continue;
            }
            // Replace Spaces
            $single_word = str_replace(" ", "", $single_word);
            // Append Before or After
            $single_word = $this->enhanceWord( (!empty($position) && $position == "before") ? ($word . $this->getSeparator() .$this->getStartWrapTag(). $single_word. $this->getEndWrapTag() ): ($single_word.$this->getSeparator(). $this->getStartWrapTag(). $word . $this->getEndWrapTag()) );     
            // Add Tld if exists
            $tldString = $this->getTldString();
            $single_word .= $tldString;
            // Add to the Final List
            $finalList[] = $single_word;
            if(count($finalList) >= ($this->getLimit() +  $this->getOffset()) && !$getCount ) {
                break;
            }
        }

        if(!empty($categoryWordIds)) {
            $this->setPopulatedWordIds($categoryWordIds);
        }

        // Placed All words if No Result Found
        if(empty($finalList)) {
            $finalGenList = $this->generalList();
            $finalList = $finalGenList["words"];
            if($getCount) {
                return $finalGenList["total_count"];
            }else {
                return $this->buildResult($finalGenList["words"]);
            }
        }

        $requiredWords = array_slice($finalList, $this->getOffset(), $this->getLimit());
        if($getCount) {
            return count($finalList);
        }
        // Free Memory
        unset($uniqueWords);
        unset($finalList);

        return $this->buildResult($requiredWords);
    }

    public function buildResult(array $words) {
        shuffle($words);
        $result["words"] = $words;
        return $result;
    }

    public function generalList() {

        // Populated Category based Word
        //TODO: Better Result Based on Categories

        // if(!empty($this->getPopulatedCategories())) {
        //     $categoryIds = $this->getPopulatedCategories();
        //     $categoryIdsStr = implode(",", $categoryIds);

        $categoryBasedIds = $this->getCategoryIds();
        $categoryCondition = " ";
        if(!empty($categoryBasedIds)) {
            $categoryIdsStr = implode("', '", $categoryBasedIds);
            $categoryCondition = " AND WCA.category_id IN ('".$categoryIdsStr."') "; 
        }

        $tldString = $this->getTldString();

        $concat = (!empty($this->getPosition()) && $this->getPosition() == "before") ?  " CONCAT( '".$this->getWord()."', '".$this->getSeparator()."' , '".$this->getStartWrapTag()."', REPLACE(TRIM(WD.name), ' ', ''), '".$this->getEndWrapTag()."', '".$tldString."' ) " : " CONCAT( REPLACE(TRIM(WD.name), ' ', ''), '".$this->getSeparator()."' , '".$this->getStartWrapTag()."', '".$this->getWord()."', '".$this->getEndWrapTag()."', '".$tldString."' ) ";

        $sql = " SELECT ".$concat." AS word FROM ".$this->config->getWordDictionaryTableName()." WD LEFT JOIN ".$this->config->getWordCatAssocTableName()." WCA ON WD.id = WCA.word_id WHERE LENGTH(WD.name) <= ".$this->getlength()." ".$categoryCondition."  group by WD.name ORDER BY rand() LIMIT ".$this->getOffset().", ".$this->getLimit();

        $results = $this->db->get_results($sql);
        $words = $this->extractArrayOut($results);
        $words = array_map(array($this, 'enhanceWord'), $words);

        $sql = " SELECT WD.name AS total_count FROM ".$this->config->getWordDictionaryTableName()." WD LEFT JOIN ".$this->config->getWordCatAssocTableName()." WCA ON WD.id = WCA.word_id WHERE LENGTH(WD.name) <= ".$this->getlength()." ".$categoryCondition." group by WD.name ORDER BY rand() ";
        

        $countResults = $this->db->get_results($sql);
        // $countResults = $this->extractArrayOut($countResults, true);
        
        $finalResults = [
            "words" => $words,
            "total_count" => count($countResults)
        ];
        
        return $finalResults;
        
    }


    public function getRequestsData()
    {
        $response = $this->db->get_results("SELECT req.id, req.name, req.status, count(res.id) AS total_results FROM ".$this->config->getRequestTableName()." req LEFT JOIN ".$this->config->getWordDictionaryTableName()." res ON req.id=res.request_id GROUP BY req.id");
        $p = new Pager; 
        
        $limit = 20; 

        $baseUrl = '<?php echo home_url(); ?>/wp-admin/admin.php?page=business_name_word_manager';
        
        $start = $p->findStart($baseUrl, $limit); 
        
        $count = count($response); 
        
        $pages = $p->findPages($count, $limit); 
        
        $response = $this->db->get_results("SELECT req.id, req.name, req.status, req.language_code, count(res.id) AS total_results FROM ".$this->config->getRequestTableName()." req LEFT JOIN ".$this->config->getWordDictionaryTableName()." res ON req.id=res.request_id GROUP BY req.id LIMIT $start, $limit");

        $processed_words = $this->db->get_results("SELECT count(res.id) as processed, req.id FROM ".$this->config->getRequestTableName()." req LEFT JOIN ".$this->config->getWordDictionaryTableName()." res ON req.id=res.request_id WHERE res.status='Pending Approval' GROUP BY req.id LIMIT $start, $limit");

        $res = [];

        foreach( $response as $rr ){
            $res[$rr->id]['name'] = $rr->name;
            $res[$rr->id]['status'] = $rr->status;
            $res[$rr->id]['total_results'] = $rr->total_results;
            $res[$rr->id]['language_code'] = $rr->language_code;
        }

        foreach( $processed_words as $pw ){
            $res[$pw->id]['processed'] = $pw->processed;
        }

        
        $pagelist = $p->pageList($_REQUEST['nav'], $pages);
        $response['data'] = $res;
        $response['pagination'] = $pagelist;
        $response['count'] = $count;

        return $response;
    }

    public function getAllWordCategories()
    {
        $total = $this->db->get_col("SELECT COUNT(id) FROM ".$this->config->getWordCategoriesTableName());

        $p = new Pager; 
        
        $limit = 20;
        
        $baseUrl = home_url().'/wp-admin/admin.php?page=bnwm_word_categories';
        
        $start = $p->findStart($baseUrl, $limit); 
        
        $count = $total[0]; 
        
        $pages = $p->findPages($count, $limit); 
        
        $res = $this->db->get_results("SELECT cat.id, cat.name, cat.status, count(word.word_id) AS total_words FROM ".$this->config->getWordCategoriesTableName()." cat LEFT JOIN ".$this->config->getWordCatAssocTableName()." word ON cat.id=word.category_id GROUP BY cat.id LIMIT $start, $limit");
        
        $pagelist = $p->pageList($_REQUEST['nav'], $pages); 
        
        $response['data'] = $res;
        $response['pagination'] = $pagelist;
        $response['count'] = $count;
        return $response;
    }

    public function getConfig(): Config {
        return $this->config;
    }

    public function oldNameAlgoLogic() {

        $total = 0; 
        if (!isset($_SESSION['invite_modal_value'])){
            $_SESSION['invite_modal_value']=array();
        }
        if(isset($_REQUEST['industry'])){
            unset($_SESSION['filterd_columns']);
        }

        // Need to Improve this
        $search_word = $_REQUEST["bname"];
        $position = $this->getPosition();
        $length = $this->getlength();
        // Need to Improve
        $industry = (isset($_REQUEST['industry']) && !empty($_REQUEST['industry'])) ? $_REQUEST['industry']: '';
        $general_liststring = [];
        $synonyms_list = [];
        $suffix_list = [];

        if($this->getOldLanguage() == "en") {

            $rhyming_list = $this->rhyming_words_list($search_word, $position, $length);
            $synonyms_list = $this->synonyms_words_list($search_word, $position, $length);
            $suffix_list = $this->suffix_words_list( $search_word, $length );
            

            if( is_array($industry) && count($industry) == 1 ) {
                $mainstring = $this->single_filter_wordgen($search_word, $industry, $position, $length);
                $general_liststring = $this->general_list($search_word, $position, $length);
                $domains = $this->domains_list( $industry );
            }
            if(is_array($industry) && count($industry) > 1){
                $mainstring = $this->multi_filter_wordgen($search_word, $industry, $position, $length);
                $general_liststring = $this->general_list($search_word, $position, $length);
                $domains = $this->domains_list( $industry );
            }

            if(empty($industry) &&  preg_match('#^[a-z0-9\s]+$#i', $search_word)) {
                
                $mainstring = $this->all_wordgen($search_word, $industry, $position, $length);
                if( isset($_SESSION['filterd_columns']) ){
                    $general_liststring = $this->general_list( $search_word, $position, $length );
                    $domains = $this->domains_list( $_SESSION['filterd_columns'] );
                }
                if( !isset($_SESSION['filterd_columns']) && !isset($_REQUEST['industry'])){
                    $domains = $this->domains_list( $search_word );
                }
            }
        }else {
            // International
            $grammar = (isset($_REQUEST['grammar']) && !empty($_REQUEST['grammar'])) ? $_REQUEST['grammar'] : '';
            $numkey=str_word_count(stripslashes($_REQUEST['bname']));

            $lang = $this->getOldLanguage();
            $tbl = '';

            if ($lang == 'it')
                $tbl = 'wp_' . $lang . '_generator';
            if ($lang == 'es')
                $tbl = 'wp_' . $lang . '_generator';
            if ($lang == 'de')
                $tbl = 'wp_' . $lang . '_generator';
            if ($lang == 'fr')
                $tbl = 'wp_' . $lang . '_generator';
            if ($lang == 'pt-pt')
                $tbl = 'wp_pt_generator';
            if ($lang == 'pt-br')
                $tbl = 'wp_pt_generator';

            
            if ($numkey >= 1 || !empty($grammar)) {
                if (!empty($search_word) && is_array($industry) && count($industry) == 1) {
                    $mainstring = array();
                    $mainstring = $this->international_single_filter_wordgen($search_word, $industry, $position, $tbl, $grammar, $lang);

                    $general_liststring = $this->international_general_list($search_word, $position, $length);

                    $domains = $this->international_domains_list($industry);
                }

                if (!empty($search_word) && is_array($industry) && count($industry) > 1) {
                    $mainstring = array();
                    $mainstring = $this->international_multi_filter_wordgen($search_word, $industry, $position, $tbl, $grammar, $lang);

                    $general_liststring = $this->international_general_list($search_word, $position, $length);
                    $domains = $this->international_domains_list($industry);

                }

                if (!empty($search_word) && empty($industry)) {
                    $mainstring = array();
                    $mainstring = $this->international_all_wordgen($search_word, $industry, $position, $tbl, $lang, $grammar);
                    if (isset($_SESSION['filterd_columns'])) {
                        $general_liststring = $this->international_general_list($search_word, $position, $length);
                        $domains = $this->international_domains_list($_SESSION['filterd_columns']);
                    }
                    if (!isset($_SESSION['filterd_columns']) && !isset($_REQUEST['industry'])) {
                        $domains = $this->international_domains_list($search_word);
                    }
                }


            }


        }

        // Logic PHP
        $_SESSION['synonyms_set'] = ( !empty($synonyms_list) ) ? 'yes' : 'no';
        $page = $this->getPage();
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        
        if(!empty($mainstring) && ($mainstring || $general_liststring) ){

            if( !empty($_REQUEST['one_word']) && $_REQUEST['one_word'] == 'on' && $_REQUEST['two_word'] == 'off' && $_REQUEST['rhyming'] == 'off' && $_REQUEST['synonyms'] == 'on' ){
                $mainstringnew = $suffix_list;
                shuffle($mainstringnew);
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );
                $total= count( $mainstringnew );
            }

            if( !empty($_REQUEST['one_word']) && $_REQUEST['one_word'] == 'on' && $_REQUEST['two_word'] == 'off' && $_REQUEST['rhyming'] == 'off' && $_REQUEST['synonyms'] == 'off' ){
                $mainstringnew = $suffix_list;
                shuffle($mainstringnew);
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );
            }

            if( !empty($_REQUEST['two_word']) && $_REQUEST['two_word'] == 'on' && $_REQUEST['one_word'] == 'off' && $_REQUEST['rhyming'] == 'off' && $_REQUEST['synonyms'] == 'off'){

                $mainstringnew = array_merge($mainstring, $general_liststring);
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );
            }

            if( !empty($_REQUEST['one_word']) && $_REQUEST['one_word'] == 'on' && $_REQUEST['two_word'] == 'on' && $_REQUEST['synonyms'] == 'on' && $_REQUEST['rhyming'] == 'off'){
                $mainstringnew = array_merge( $mainstring, $general_liststring );

                if( !empty($synonyms_list) ){

                    /* backup
                    $limit = ($page >= 4) ? 72: 56;

                    $synlimit = 20;
                    $synofset = ( !empty($page) && $page !=1 ) ? ($page * $synlimit-$synlimit) :0;

                    $suflimit = 20;
                    $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
                    */

                    if( $limit == 96 ){ 
                     $limit = ($page >= 4) ? 72: 56;

                     $synlimit = 20;
                     $synofset = ( !empty($page) && $page !=1 ) ? ($page * $synlimit-$synlimit) :0;

                     $suflimit = 20;
                     $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;

                    }
                    
                    if( $limit == 30  ){
                      $limit = 10;

                      $synlimit = 10;
                      $synofset = ( !empty($page) && $page !=1 ) ? ( $page * $synlimit-$synlimit ) :0;

                      $suflimit = 10;
                      $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
                    }

                    $arr = array_slice( $synonyms_list, $synofset, $synlimit, true );

                    $arr2 = array_slice( $suffix_list, $sufofset, $suflimit, true );

                    if( count($arr2) == 0 ){
                        $arr = array_slice( $synonyms_list, $sufofset, 24, true );
                    }

                    if( empty($arr) && empty($arr2)) { $limit = 96; }
                    
                    $arr3 = array_slice( $mainstringnew, $offset, $limit, true );
                    

                    $mix = array_merge( $arr, $arr2 );

                    $mainstring2 = array_merge( $arr3, $mix );

                    shuffle( $mainstring2 );

                    $total= count( $mainstringnew ) + count( $synonyms_list ) + count($suffix_list);

                }else{
                    $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );
                    $total= count( $mainstringnew ) + count($suffix_list);
                }
            }

            if( !empty($_REQUEST['one_word']) && $_REQUEST['one_word'] == 'on' && $_REQUEST['two_word'] == 'on' && $_REQUEST['rhyming'] == 'off' && !isset($_REQUEST['synonyms']) ){

                $mainstringnew = array_merge( $mainstring, $general_liststring );

                $suflimit = 20;
                $sufofset = ( !empty($page) && $page !=1 ) ? ($page * $suflimit-$suflimit) :0;
                $suffix_listn = array_slice( $suffix_list, $sufofset, 20, true );
                
                $mainstrin = array_slice( $mainstringnew, $offset, $limit, true );

                $mainstring2 = array_merge( $mainstrin, $suffix_listn );
                
                shuffle( $mainstring2 );

                $total= count( $mainstringnew ) + count( $suffix_list );

            }

            if( !empty($_REQUEST['one_word']) && $_REQUEST['one_word'] == 'on' && $_REQUEST['two_word'] == 'on' && $_REQUEST['synonyms'] == 'off' && $_REQUEST['rhyming'] == 'off' ){

                $mainstringnew = array_merge($mainstring, $suffix_list);
                shuffle($mainstringnew);
                shuffle($mainstringnew);

                $mainstringnew = array_merge($mainstringnew, $general_liststring);
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );

            }

            if( !empty($_REQUEST['one_word']) && $_REQUEST['one_word'] == 'off' && $_REQUEST['two_word'] == 'on' && $_REQUEST['synonyms'] == 'on' && $_REQUEST['rhyming'] == 'off'){
                $mainstringnew = array_merge($mainstring, $synonyms_list);
                shuffle($mainstringnew);

                $mainstringnew = array_merge($mainstringnew, $general_liststring);
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );
            }

            if( !empty($_REQUEST['one_word']) && $_REQUEST['one_word'] == 'off' && $_REQUEST['two_word'] == 'off' && $_REQUEST['synonyms'] == 'on' && $_REQUEST['rhyming'] == 'off' ){

                $mainstringnew = $synonyms_list;
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );

            }

            if( !empty($_REQUEST['rhyming']) && $_REQUEST['rhyming'] == 'on' && $_REQUEST['one_word'] == 'off' && $_REQUEST['two_word'] == 'off' && $_REQUEST['synonyms'] == 'off' ){
                $mainstringnew = $rhyming_list;
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );
            }

            if( !empty($_REQUEST['rhyming']) && $_REQUEST['rhyming'] == 'on' && $_REQUEST['one_word'] == 'on' && $_REQUEST['two_word'] == 'off' && $_REQUEST['synonyms'] == 'off' ){
                $mainstringnew = array_merge($suffix_list, $rhyming_list);
                shuffle($mainstringnew);

                $mainstringnew = array_merge($mainstringnew, $general_liststring);
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );
            }

            if(  !empty($_REQUEST['rhyming']) && $_REQUEST['rhyming'] == 'on' && $_REQUEST['one_word'] == 'on' && $_REQUEST['two_word'] == 'on' && $_REQUEST['synonyms'] == 'off' ){

                $mainstringnew = array_merge($suffix_list, $rhyming_list);
                shuffle($mainstringnew);

                $mainstringnew = array_merge($mainstringnew, $mainstring);
                shuffle($mainstringnew);

                $mainstringnew = array_merge($mainstringnew, $general_liststring);
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );
            }

            if( !empty($_REQUEST['rhyming']) && $_REQUEST['rhyming'] == 'on' && $_REQUEST['one_word'] == 'off' && $_REQUEST['two_word'] == 'on' && $_REQUEST['synonyms'] == 'off' ){

                $mainstringnew = array_merge($mainstring, $rhyming_list);
                shuffle($mainstringnew);

                $mainstringnew = array_merge($mainstringnew, $general_liststring);
                $mainstring2 = array_slice( $mainstringnew, $offset, $limit, true );

                $total= count( $mainstringnew );
            }

            if( isset($_REQUEST['bname']) && !isset( $_REQUEST['one_word'] ) && !isset( $_REQUEST['two_word']) && !isset($_REQUEST['synonyms']) && !isset($_REQUEST['rhyming']) ){
                $mainstringnew = array_merge( $mainstring, $general_liststring );
                if( !empty($synonyms_list) ){

                    // $limit = ($page >= 4) ? 72: 56;

                    // $synlimit = 20;
                    // $synofset = ( !empty($page) && $page !=1 ) ? ($page * $synlimit-$synlimit) :0;

                    // $suflimit = 20;
                    // $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;

                    // $synlimit = 20;
                    // $synofset = ( !empty($page) && $page !=1 ) ? ($page * $synlimit-$synlimit) :0;

                    // $suflimit = 20;
                    // $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;

                    if( $limit == 96 ){ 
                     $limit = ($page >= 4) ? 72: 56;

                     $synlimit = 20;
                     $synofset = ( !empty($page) && $page !=1 ) ? ($page * $synlimit-$synlimit) :0;

                     $suflimit = 20;
                     $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;

                    }

                    if( $limit == 72 ){
                      $limit = 52;

                      $synlimit = 10;
                      $synofset = ( !empty($page) && $page !=1 ) ? ( $page * $synlimit-$synlimit ) :0;

                      $suflimit = 10;
                      $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
                    }
                    
                    if( $limit == 30  ){
                      $limit = 10;

                      $synlimit = 10;
                      $synofset = ( !empty($page) && $page !=1 ) ? ( $page * $synlimit-$synlimit ) :0;

                      $suflimit = 10;
                      $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
                    }

                    $arr = array_slice( $synonyms_list, $synofset, $synlimit, true );

                    $arr2 = array_slice( $suffix_list, $sufofset, $suflimit, true );

                    if( count($arr2) == 0 ){
                        $arr = array_slice( $synonyms_list, $sufofset, 24, true );
                    }

                    if( empty($arr) && empty($arr2)) { $limit = 96; }

                    $arr3 = array_slice( $mainstringnew, $offset, $limit, true );

                    $mix = array_merge( $arr, $arr2 );

                    $mainstring2 = array_merge( $arr3, $mix );

                    shuffle( $mainstring2 );

                    $total= count( $mainstringnew ) + count( $synonyms_list ) + count($suffix_list);
                }else {
                    $suflimit = 20;
                    $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
                    $suffix_listn = array_slice( $suffix_list, $sufofset, 20, true );
          
                    $mix = array_merge( $suffix_listn, $mainstringnew );
          
                    $mainstrin = array_slice( $mix, $offset, $limit, true );
          
                    $mainstring2 = array_slice( array_merge($mix, $mainstrin), $offset, $limit, true );
                    shuffle( $mainstring2 );
          
                    $total= count( $mainstringnew ) + count($suffix_list);
                }
            }

        }else{
            $mainstring2 = array_slice( $mainstring, $offset, $limit );
            $total = count( $mainstring );
        }

        $this->setOldTotalCount($total);

        return $mainstring2;

    }

    function get_url_var(){
        $strURL = $_SERVER['REQUEST_URI'];
        $arrVals = explode("?",$strURL);
        $arrVals2 = explode("/",$arrVals[0]);
          
        $numkey=str_word_count(stripslashes($_REQUEST['bname']));
  
        if($numkey<=1){
          $bname=stripslashes($_REQUEST['bname']);
        }
        else{
          $fword=explode(' ',stripslashes($_REQUEST['bname']));
          $bname=$fword[0].' '.$fword[1];
        }
        if(isset($_REQUEST['related'])){
            $element='?bname='.$bname."&related=".$_REQUEST['related'];
        }
        else{
            $element='?bname='.$bname;
        }
        $found=0;
        if($arrVals2==''){
          $found=$found+1;
        }
        else{
  
            if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE != 'en' ) {
                $found=$arrVals2[3];
            }
            if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
                $found=$arrVals2[2];
            }
  
        }
        return $found;
    }

    function serach_word_senitize( $query_word ){
        $patterns = array(
            '#^https?\:\/\/#',
            '/www./',
            '/^\s\s+/',
            '/\s\s+$/',
            '/\s\s+/u',
            '/.co.uk/',
            '/.net/',
        );

        $replace = array('','','', '', ' ','','','','','');
        $query_word = preg_replace($patterns, $replace, $query_word);
        if(stripos($query_word, ',')){
            $query_word = str_replace(',', ' ', $query_word);
            $query_word = str_replace(', ', ' ', $query_word);
        }if(stripos($query_word, '.com')){
            $query_word = str_replace('.com', '', $query_word);
        }if(stripos($query_word, '.org')){
            $query_word = str_replace('.org', '', $query_word);
        }
        
        $query_word = preg_replace('/[^a-zA-Z0-9-\s-]/','', $query_word);

        return $query_word;
    }

    function rhyming_words_list($search_word, $position, $length){
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $list = array();
        $myrows='';
        $table = $this->old_eng_generator_table;

        $myrows = $this->db->get_results("select * from $table", ARRAY_A);
        if( count(preg_split('/\s+/', $search_word))>=1 ){
            
            $word = explode(' ', $search_word);

            foreach ($word as $key => $value) {
                $char = mb_substr($value, 0, 1, "UTF-8");
                if( $myrows != 0 || $myrows != false) {
                    foreach ($myrows as $word) {
                        foreach($word as $key => $row ){
                            
                            if(strtolower($char) == strtolower(mb_substr($row, 0, 1, "UTF-8")) ){

                                if( $length < 15 && ( strlen($row) == $length || strlen($row) < $length ) ){
                                    
                                    $result = ($position === 'after') ? ucwords($row.$separator.$startWrap.$value.$endWrap) : ucwords($value.$separator.$startWrap.$row.$endWrap);
                                    $result .= $tldString; 
                                    
                                    if(!in_array($result, $list)){
                                      array_push($list, $result);

                                    } 
                                }
                                if( $length == 15 && ( strlen($row) < $length || strlen($row) > $length ) ){
                                    
                                    $result = ($position === 'after') ? ucwords($row.$separator.$startWrap.$value.$endWrap) : ucwords($value.$separator.$startWrap.$row.$endWrap);
                                    $result .= $tldString;

                                    if(!in_array($result, $list)){
                                      array_push($list, $result);

                                    }

                                }
                                if( $length == '' || $length > 15){
                                    
                                    $result = ($position === 'after') ? ucwords($row.$separator.$startWrap.$value.$endWrap) : ucwords($value.$separator.$startWrap.$row.$endWrap);
                                    $result .= $tldString;
                                                                        
                                    if(!in_array($result, $list)){
                                      array_push($list, $result);

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        shuffle($list);

        return $list;
    }

    function synonyms_words_list($search_word, $position, $length){
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $list = array();
        $temp = array();
        $temp_word = array();
        $word_for_synonyms = array();
        $mainstringN = array();
        $myrows = '';
        $table = $this->old_wp_synonym_table;

        $listcolumns = $this->db->get_results("SELECT COLUMN_NAME 
            FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = '$table'", ARRAY_A);
        
        foreach ($listcolumns as $key => $value) {
            array_push($temp, $value['COLUMN_NAME']);
        }

        if( count(preg_split('/\s+/', $search_word))>=1 ){
            $word = explode(' ', $search_word);
            foreach ($word as $key => $value) {
                $k = array_search(strtolower($value), $temp);
                if( $temp[$k] && $k !=0 ){
                    array_push($temp_word, $temp[$k]);
                }
            }
        }

        if( !empty($temp_word) ){
            if(count($temp_word)>1){
                
                $wordcol = implode(',', $temp_word);
                $myrows = $this->db->get_results("select $wordcol from $table", ARRAY_A);
            }else{
                $wordcol = $temp_word[0];
                $myrows = $this->db->get_results("select $wordcol from $table", ARRAY_A);
            }
        }

        if( $myrows != 0 || $myrows != false) {
            foreach ($myrows as $word) {
                foreach($word as $key => $row ){
                    array_push($list, $row);
                }
            }
        }

        $alldata = $this->db->get_results("select * from `wp_eng_generator`", ARRAY_A);

        foreach ($list as $keword) {
            if($keword){
                foreach( $alldata as $key => $row ){
                    foreach( $row as $in => $value ){
                        if($in == 'id'){ continue;}
                        if( !empty($value) ){

                            if($length < 15 && (strlen($value) == $length || strlen($value) < $length) ){
                                $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                            }

                            if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
                                
                                $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                            }
                            
                            if(!$length || $length > 15){
                                $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;

                            }
                        }
                    }
                }
            }
        }

        shuffle($mainstringN);

        return $mainstringN;
    }

    function check_for_suffix($keword){
        if( substr($keword, -2) === 'sh' ){
            return true;
        }else if( substr($keword, -2) === 'ic' ){
            return true;
        }else if( substr($keword, -2) === 'es' ){
            return true;
        }else if( substr($keword, -2) === 'al' ){
            return true;
        }
        // else if( substr($keword, -3) == 'ing' ){
        //  return true;
        // }
        else if( substr($keword, -3) == 'ion' ){
            return true;
        }else if( substr($keword, -4) == 'ment' ){
            return true;
        }else if( 'beauty' == strtolower($keword) ){
            return true;
        }else if( 'technology' == strtolower($keword) ){
            return true;
        }else if( 'fitness' == strtolower($keword)  ){
            return true;
        }else if( 'luxury' == strtolower($keword)  ){
            return true;
        }else{
            return false;
        }
    }

    function remove_word($keword){

        if( substr($keword, -2) === 'sh' ){
            return substr_replace($keword, "",-2);
        }else if( substr($keword, -2) === 'ic' ){
            return substr_replace($keword, "",-2);
        }else if( substr($keword, -2) === 'es' ){
            return substr_replace($keword, "",-2);
        }else if( substr($keword, -2) === 'al' ){
            return substr_replace($keword, "",-2);
        }
        // else if( substr($keword, -3) == 'ing' ){
        //  return substr_replace($keword, "",-3);
        // }
        else if( substr($keword, -3) == 'ion' ){
            return substr_replace($keword, "",-3);
        }else if( substr($keword, -4) == 'ment' ){
            return substr_replace($keword, "",-4);
        }else if( 'beauty' == strtolower($keword) ){
            return 'beaut';
        }else if( 'technology' == strtolower($keword)){
            return 'tech';
        }else if( 'fitness' == strtolower($keword) ){
            return 'fit';
        }else if( 'luxury' == strtolower($keword) ){
            return 'lux';
        }else{
            return $keword;
        }
    
    }

    function suffix_words_list( $search_word, $length ) {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $list = array();
        $myrows = '';

        // GET will replace with the generic function
        if( ( !empty($_REQUEST['one_word']) && $_REQUEST['one_word'] == 'on') && $_REQUEST['synonyms'] == 'on' && $_REQUEST['two_word'] == 'off' && $_REQUEST['rhyming'] == 'off'){

            $lists = array();
            $temp = array();
            $temp_word = array();
            $word_for_synonyms = array();
            $myrows = '';
            $table = $this->old_wp_synonym_table;

            $listcolumns = $this->db->get_results("SELECT COLUMN_NAME 
                FROM INFORMATION_SCHEMA.COLUMNS 
                WHERE TABLE_NAME = '$table'", ARRAY_A);
            
            foreach ($listcolumns as $key => $value) {
                array_push($temp, $value['COLUMN_NAME']);
            }

            if( count(preg_split('/\s+/', $search_word))>=1 ){
                $word = explode(' ', $search_word);
                foreach ($word as $key => $value) {
                    $k = array_search(strtolower($value), $temp);
                    if( $temp[$k] && $k !=0 ){
                        array_push($temp_word, $temp[$k]);
                    }
                }
            }
            if( !empty($temp_word) ){

                if( count($temp_word)>1 ){
                    $wordcol = implode(',', $temp_word);
                    $res = $this->db->get_results("select $wordcol from $table", ARRAY_A);
                }else{
                    $wordcol = $temp_word[0];
                    $res = $this->db->get_results("select $wordcol from $table", ARRAY_A);
                }

                if( $res != 0 || $res != false ) {
                    foreach ($res as $word) {
                        foreach($word as $key => $row ){
                            array_push($lists, $row);
                        }
                    }
                }
            }
        }

        $myrows = $this->db->get_results("select suffix from `wp_eng_generator`", ARRAY_A);

        if( count(preg_split('/\s+/', $search_word)) >= 1 ){

            $word = explode(' ', $search_word);

            $words = ( !empty($lists) ) ? array_merge( $lists, $word ) : $word;

            foreach ($words as $key => $value) {
                if($value){
                    if( ($myrows != 0 || $myrows != false) && $this->check_for_suffix($value) ) {
                        $keword = $this->remove_word($value);

                        foreach ($myrows as $word) {
                            if( $word['suffix'] == '') continue;

                            $list[] = ucwords($keword.''.$startWrap.strtolower($word['suffix']).$endWrap).$tldString;
                        }

                    }else{
                        foreach ($myrows as $word) {
                            foreach($word as $key => $row ){
                                if(!$row) continue;
                                
                                $list[] = ucwords($value.''.$startWrap.strtolower($row).$endWrap).$tldString;
                            }
                        }
                    }
                }
            }
        }

        shuffle($list);

        $temp = array();
        $extraaLength = strlen($startWrap) + strlen($endWrap) + strlen($tldString);
        foreach ($list as $key => $value) {
            $exactLength = strlen($value) - $extraaLength;
            if( $length < 15 && ( $exactLength == $length || $exactLength < $length ) ){
                array_push($temp, $value);
            }
            if( $length == 15 && ( $exactLength < $length || $exactLength > $length ) ){
                array_push($temp, $value);
            }
            if( $length == '' || $length > 15){
                array_push($temp, $value);
            }
        }
        $list = $temp;
        return $list;
    }


    function domains_list( $search_word ){

        $exist_ids = [];    
        $indusrows = [];
        $firstletterows = [];
        // New Code 4 September 2020

        if(isset($_REQUEST['bname']) && preg_match('#^[a-z0-9\s]+$#i', $_REQUEST['bname']) )
        {

            $search_key = $_REQUEST['bname'];
            $search_key = mb_convert_encoding($search_key, 'UTF-8', 'Windows-1252');
            
            // For Get Entryies By Title Start //
            $condition = '';
            
            $condition = ' price < 4000) AND ( title LIKE "%'.$search_key.'%") AND status = "1"';
            
            $condition .= ' ORDER BY price DESC ';

            $titlerows = $this->db->get_results("SELECT DISTINCT `id`, `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A);

            if($titlerows){
                foreach ($titlerows as $row) {
                    array_push($exist_ids, $row['id']);
                }
            }
            // End //
            
            // For Get Entryies By Industries Start //
            if( is_array($search_word) ){
                
                if( count($search_word)>1 ){
                    
                    $words = $search_word;
                    $condition = '';

                    foreach ($words as $value) {
                        if ( end($words) != $value ) {
                            $condition .= ' price < 4000) AND (title LIKE "%'.$value.'%" OR tags LIKE "%'.$value.'%" OR categories LIKE "%'.$value.'%" OR';
                        }else{
                            $condition .= ' title LIKE "%'.$value.'%" OR tags LIKE "%'.$value.'%" OR categories LIKE "%'.$value.'%") AND status = "1"';

                            if($exist_ids)
                                $condition .= ' AND id NOT IN('.implode(',', $exist_ids).') '; 

                            $condition .= ' ORDER BY price DESC ';
                        }
                    }

                    $indusrows = $this->db->get_results("SELECT DISTINCT `id`,`title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE ($condition", ARRAY_A);
                    
                } else {

                    $condition = '';
                    
                    $condition .= 'price < 4000) AND (title LIKE "%'.$search_word[0].'%" OR tags LIKE "%'.$search_word[0].'%" OR categories LIKE "%'.$search_word[0].'%") AND status = "1"';
                    
                    if($exist_ids)
                        $condition .= ' AND id NOT IN('.implode(',', $exist_ids).') '; 

                    $condition .= ' ORDER BY price DESC ';

                    $indusrows = $this->db->get_results("SELECT DISTINCT `id`,`title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A);

                }
                    
                if($indusrows){
                    foreach ($indusrows as $row) {
                        array_push($exist_ids, $row['id']);
                    }
                } 
            }
            // End //

                // For Get Entryies By Categories Start //
            $condition = ''; 
            
            $condition .= ' price < 4000) AND ( categories LIKE "%'.$search_key.'%") AND status = "1"'; 
                
            if($exist_ids)
                $condition .= ' AND id NOT IN('.implode(',', $exist_ids).') '; 

            $condition .= ' ORDER BY price DESC ';

            $catrows = $this->db->get_results("SELECT DISTINCT `id`, `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A);

            if($catrows){
                foreach ($catrows as $row) {
                    array_push($exist_ids, $row['id']);
                }
            }
            // End // 

            
            // For Get Entryies By Tags Start //
            $condition = ''; 
            
            $condition .= ' price < 4000) AND ( tags LIKE "%'.$search_key.'%") AND status = "1"'; 
            
            if($exist_ids)
                $condition .= ' AND id NOT IN('.implode(',', $exist_ids).') '; 

            $condition .= ' ORDER BY price DESC ';

            $tagrows = $this->db->get_results("SELECT DISTINCT `id`, `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A); 

            if($tagrows){
                foreach ($tagrows as $row) {
                    array_push($exist_ids, $row['id']);
                }
            }
            // End //
                
            // For Get Entryies By Static As a dynamically Start // 
            $condition = '';
            
            $condition .= ' price < 4000) AND categories LIKE "%Brandable%" AND status = "1" ';
            
            if($exist_ids)
                $condition .= ' AND id NOT IN('.implode(',', $exist_ids).') ';
                
            
            $condition .= ' ORDER BY price DESC ';
            
            $myrows = $this->db->get_results(" SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A );
                
            // End //  

            $domainslist  = array_merge( $titlerows,$indusrows,$catrows,$tagrows,$myrows);
            
            
            shuffle( $domainslist );
            return $domainslist;    
        } 
    }

    function general_list($search_word, $position, $length)
    {  
        $tldString = $this->getTldString(); 
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $mainstringN = array();
        $start = 0;
        $myrows = $this->db->get_results("select general_list from `wp_eng_generator` ", ARRAY_A);

        if( count(preg_split('/\s+/', $search_word) ) > 1 ){
                $kewords = explode(' ', $search_word);

                foreach ($kewords as $keword) {
                    $keword = $this->serach_word_senitize( $keword );

                    foreach( $myrows as $key => $row ){
                        foreach( $row as $in => $value ){
                            if( !empty($value) ){

                                if($length < 15 && (strlen($row[$in]) == $length || strlen($row[$in]) < $length ) ){
                                    $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                                }

                                if($length == 15 && (strlen($row[$in]) == $length || strlen($row[$in]) > $length || strlen($row[$in]) < $length)){
                                    $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                                }

                                if(!$length || $length > 15){
                                    $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                                }
                            }
                        }
                    }
                }
            }else{
                $keword = $this->serach_word_senitize( $search_word );
                
                foreach( $myrows as $key => $row ){
                    foreach( $row as $in => $value ){
                        if( !empty($value) ){
                            if($length < 15 && (strlen($row[$in]) == $length || strlen($row[$in]) < $length ) ){
                                $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                            }

                            if($length == 15 && (strlen($row[$in]) == $length || strlen($row[$in]) > $length || strlen($row[$in]) < $length)){
                                $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                            }

                            if(!$length || $length > 15){
                                $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                            }

                        }
                    }
                }
            }
        return $mainstringN;
    }

    function single_filter_wordgen($search_word, $industry, $position, $length)
    {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $mainstringN=array();
        $col = '';
        $start = 0;
        $industry[count($industry) - 1] = ( end($industry) == "Real Estate" ) ? "realestate" : $industry[count($industry) - 1];

        foreach ($industry as $value) {
                
            $value = ($value == "Real Estate") ? "realestate" : $value;
            $col = trim( strtolower($value) );
        }
    
        $myrows = $this->db->get_results("select $col from `wp_eng_generator`", ARRAY_A);

        

        /**
        *   Instance 1) Joining the Search keywords with filters
        *   
        **/

        /**
        * @param $search_word more than one
        **/
        if( count( preg_split('/\s+/', $search_word) )> 1 ){
            $kewords = explode(' ', $search_word);

            foreach ($kewords as $keword) {
                foreach( $myrows as $row ){
                    if( !empty($row[$col]) ){

                        /**
                        * charaters count filters length less or equal to 15
                        **/
                        if($length < 15 && (strlen($row[$col]) == $length || strlen($row[$col]) < $length ) ){
                            if($col == 'suffix'){
                                $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($row[$col]).$endWrap).$tldString;
                            }else{
                                $mainstringN[] = ($position === 'after') ? ucwords( $row[$col].$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$row[$col].$endWrap ).$tldString;
                            }
                        }

                        /**
                        * charaters count filters lenght more than 15
                        **/
                        if($length == 15 && (strlen($row[$col]) == $length || strlen($row[$col]) > $length || strlen($row[$col]) < $length)){
                            if($col == 'suffix'){
                                $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($row[$col]).$endWrap).$tldString;
                            }else{
                                $mainstringN[] = ($position === 'after') ? ucwords( $row[$col].$separator.$startWrap.$keword. $endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$row[$col].$endWrap ).$tldString;
                            }
                        }

                        /**
                        * if charaters count filters not applied default
                        **/
                        if(!$length || $length > 15){
                            if($col == 'suffix'){
                                $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($row[$col]).$endWrap).$tldString;
                            }else{
                                $mainstringN[] = ($position === 'after') ? ucwords($row[$col].$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$row[$col].$endWrap).$tldString;
                            }
                        }
                    }
                }
            }
        }else{

        /**
        * @param $search_word equals to one
        **/
            $keword = $search_word;
            if($col == 'suffix'){
                if( substr($keword, -2) === 'es' ){
                    $keword = substr_replace($keword, "",-2);
                }
                else if( substr($keword, -3) == 'ing' ){
                    //$keword = substr_replace($keword ,"",-3);
                    $keword = $keword;
                }else{
                    $keword = $search_word;
                }
            }

            foreach( $myrows as $row ){
                if( !empty($row[$col]) ){

                    if($length < 15 && (strlen($row[$col]) == $length || strlen($row[$col]) < $length ) ){
                        if($col == 'suffix'){
                            $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($row[$col]).$endWrap).$tldString;
                        }else{
                            $mainstringN[] = ($position === 'after') ? ucwords( $row[$col].$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$row[$col].$endWrap ).$tldString;
                        }
                    }

                    if($length == 15 && (strlen($row[$col]) == $length || strlen($row[$col]) > $length || strlen($row[$col]) < $length)){
                        if($col == 'suffix'){
                            $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($row[$col]).$endWrap).$tldString;
                        }else{
                            $mainstringN[] = ($position === 'after') ? ucwords( $row[$col].$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$row[$col].$endWrap ).$tldString;
                        }
                    }

                    if(!$length || $length > 15){

                        if($col == 'suffix'){
                            $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($row[$col]).$endWrap).$tldString;
                        }else{
                            $mainstringN[] = ($position === 'after') ? ucwords( $row[$col].$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$row[$col].$endWrap ).$tldString;
                        }

                    }
                }
            }
        }
        shuffle($mainstringN);
        
        return $mainstringN;
    }

    function multi_filter_wordgen($search_word, $industry, $position, $length)
    {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $mainstringN=array();
        $col = '';
        $start = 0;
        $industry[count($industry) - 1] = ( end($industry) == "Real Estate" ) ? "realestate" : $industry[count($industry) - 1];
        foreach ($industry as $value) {
            
            $value = ($value == "Real Estate") ? "realestate" : $value;
            if ( end($industry) != $value) {
                $col .= trim( strtolower($value) ).', ';
            }else{
                $col .= trim( strtolower($value) );
            }
        }

        $myrows = $this->db->get_results("select $col from `wp_eng_generator`", ARRAY_A);


        /**
        *   Instance 2) Joining the Search keywords with filters
        *   
        **/
        if( count( preg_split('/\s+/', $search_word) )> 1 ){
            $kewords = explode(' ', $search_word);
            foreach ($kewords as $keword) {
                foreach( $myrows as $key => $row ){
                    foreach( $row as $column => $value ){
                        if( !empty($value) ){

                            if( $length < 15 && (strlen($value) == $length || strlen($value) < $length) ){

                                if($column == 'suffix'){
                                    if( substr($keword, -2) === 'es' ){
                                        $newkeword = substr_replace($keword, "",-2);
                                    }
                                    else if( substr($keword, -3) == 'ing' ){
                                        //$newkeword = substr_replace($keword ,"",-3);
                                        $newkeword = $keword;
                                    }else{
                                        $newkeword = $keword;
                                    }
                                    
                                    $mainstringN[] = ucwords($newkeword.''.$startWrap.strtolower($value).$endWrap).$tldString;

                                }else{
                                    
                                    $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                                    
                                }

                            }
                            if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
                                if($column == 'suffix'){
                                    if( substr($keword, -2) === 'es' ){
                                        $newkeword = substr_replace($keword, "",-2);
                                    }
                                    else if( substr($keword, -3) == 'ing' ){
                                        $newkeword = $keword;
                                    }else{
                                        $newkeword = $keword;
                                    }
                                    
                                    $mainstringN[] = ucwords($newkeword.''.$startWrap.strtolower($value).$endWrap).$tldString;
                                }else{
                                    $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                                }
                            }

                            if(!$length || $length > 15){
                                if($column == 'suffix'){
                                    if( substr($keword, -2) === 'es' ){
                                        $nkeword = substr_replace($keword, "",-2);
                                    }
                                    else if( substr($keword, -3) == 'ing' ){
                                        //$nkeword = substr_replace($keword ,"",-3);
                                        $nkeword = $keword;
                                    }else{
                                        $nkeword = $keword;
                                    }
                                    
                                    $mainstringN[] = ucwords($nkeword.''.$startWrap.strtolower($value).$endWrap).$tldString;
                                }else{ 
                                    $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                                }
                                
                            }
                        }
                    }
                }
            }
        }else{
            $keword = $search_word;
            foreach( $myrows as $key => $row ){
                foreach( $row as $column => $value ){
                    if( !empty($value) ){

                        if( $length < 15 && (strlen($value) == $length || strlen($value) < $length) ){
                            if($column == 'suffix'){
                                if( substr($keword, -2) === 'es' ){
                                    $keword = substr_replace($keword, "",-2);
                                }

                                if( substr($keword, -3) == 'ing' ){
                                    $keword = $keword;
                                }
                                
                                $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($value).$endWrap).$tldString;
                            }else{

                                $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                            }

                        }

                        if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
                            if($column == 'suffix'){
                                if( substr($keword, -2) === 'es' ){
                                    $keword = substr_replace($keword, "",-2);
                                }

                                if( substr($keword, -3) == 'ing' ){
                                    $keword = $keword;
                                }
                                
                                $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($value).$endWrap).$tldString;
                            }else{
                                    
                                $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                            }

                        }

                        if(!$length || $length > 15){
                            if($column == 'suffix'){
                                if( substr($keword, -2) === 'es' ){
                                    $keword = substr_replace($keword, "",-2);
                                }

                                if( substr($keword, -3) == 'ing' ){
                                    //$keword = substr_replace($keword ,"",-3);
                                    $keword = $keword;
                                }
                                
                                $mainstringN[] = ucwords($keword.''.$startWrap.strtolower($value).$endWrap).$tldString;
                            }else{
                                $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                            }
                        }
                    }
                }
            }
        }

        shuffle($mainstringN);
        
        return $mainstringN;
    }


    function all_wordgen($search_word, $industry, $position, $length)
    {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $mainstringN=array();
        $start = 0;
        $columns = array();

        // if(!isset($_REQUEST['fltr'])):
        if(true):
            $columsname = $this->db->get_results("SELECT column_name FROM information_schema.columns WHERE table_name = 'wp_eng_generator'", ARRAY_A);

        /**
        *   Instance 3) If there are multiple serach keywords
        **/
        $nsearch_word = '';
        if( strpos($search_word, ',') ){
            $nsearch_word = str_replace(',', ' ', $search_word);
        }
        
        if( count( preg_split('/\s+/', $search_word) )> 1 ){
            $query_word='';
            $kewords = array_filter(explode(' ', $search_word));
            $kewords = array_unique($kewords);
            foreach ($kewords as $value) {
                if ( end($kewords) != $value) {
                    $query_word .= "keyword ='".$value."' OR ";
                }else{
                    $query_word .= "keyword ='".$value."'";
                }
            }

            $mycols = $this->db->get_results("select industry from `wp_matching_list` where ".$query_word."", ARRAY_A);
        }else{
            $mycols = $this->db->get_results("select industry from `wp_matching_list` where keyword ='".$search_word."' AND industry !=''", ARRAY_A);
        }
        if($mycols !== 0 || $mycols !== false){

            foreach( $mycols as $key => $col ){
                if(!empty($col)){
                    $arr = explode('/', strtolower($col['industry']) );
                }

                foreach( $columsname as $key => $column ){
                    $clname = isset($column['column_name']) ? $column['column_name'] : $column['COLUMN_NAME'];
                    if( in_array($clname, $arr) ){
                        array_push($columns, $clname);
                    }
                }
            }
            unset($_SESSION['filterd_columns']);
            
            reset($columns);
            
            $_SESSION['filterd_columns'] = array_unique($columns);
            
            $columns = (count($columns)>1 AND is_array($columns))? implode(',', $columns) : current($columns);
        }
        if( strpos($columns, ',') || $columns !='' || is_string($columns)){
            $myrows = $this->db->get_results("select ".$columns." from `wp_eng_generator` ", ARRAY_A);
        }else{
            /**
            *   Instance 3) There is no industry filter applied in [insertgenerator] shortcode
            *   Apply all filters to the keywords and displaying
            **/
            unset($_SESSION['filterd_columns']);
            $myrows = $this->db->get_results("select * from `wp_eng_generator` ", ARRAY_A);
        }else:
            unset($_SESSION['filterd_columns']);
            $myrows = $this->db->get_results("select * from `wp_eng_generator` ", ARRAY_A);
        endif;

        
        /**
        *   Instance 3) Joining the Search keywords with filters
        **/

        if( count( preg_split('/\s+/', $search_word) )> 1 ){
            $kewords = explode(' ', $search_word);

            foreach ($kewords as $keword) {

                if( $keword ){

                    $keword = $this->serach_word_senitize( $keword );

                    foreach( $myrows as $key => $row ){

                        foreach( $row as $in => $value ){

                            if($in == 'id'){ continue; }

                            if( $value !== '' ){

                                if($length < 15 && (strlen($value) == $length || strlen($value) < $length) ){
                                    if($in == 'suffix'){
                                        
                                        if( substr($keword, -2) == 'es' ){
                                            $keword = substr_replace($keword ,"",-2);
                                            $mainstringN[] = ucwords( $keword.''.$startWrap.$value.$endWrap ).$tldString;
                                        }

                                        if( substr($keword, -3) == 'ing' ){
                                            //$keword = substr_replace($keword ,"",-3);
                                            $keword = $keword;
                                            $mainstringN[] = ucwords( $keword.''.$startWrap.$value.$endWrap ).$tldString;
                                        }
                                    }else{
                                        $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                                    }
                                }

                                if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
                                    if($in == 'suffix'){
                                        
                                        if( substr($keword, -2) == 'es' ){
                                            $keword = substr_replace($keword ,"",-2);
                                            $mainstringN[] = ucwords( $keword.''.$startWrap.$value.$endWrap ).$tldString;
                                        }

                                        if( substr($keword, -3) == 'ing' ){
                                            //$keword = substr_replace($keword ,"",-3);
                                            $keword = $keword; 
                                            $mainstringN[] = ucwords( $keword.''.$startWrap.$value.$endWrap ).$tldString;
                                        }
                                    }else{
                                        $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                                    }
                                }
                                
                                if(!$length || $length > 15){
                                    $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;

                                }
                            }
                        }
                    }
                }
            }
        }else{

            $keword = $this->serach_word_senitize( $search_word );
            
            foreach( $myrows as $key => $row ) {
                foreach( $row as $in => $value ) {
                    if( $in == 'id' ){ continue; }

                    if( !empty($value) ){

                        if($length < 15 && (strlen($value) == $length || strlen($value) < $length) ){
                            if($in == 'suffix'){
                                
                                if( substr($keword, -2) == 'es' ){
                                    $keword = substr_replace($keword ,"",-2);
                                    $mainstringN[] = ucwords( $keword.''.$startWrap.$value.$endWrap ).$tldString;
                                }

                                if( substr($keword, -3) == 'ing' ){
                                    //$keword = substr_replace($keword ,"",-3);
                                    $keword = $keword;
                                    $mainstringN[] = ucwords( $keword.''.$startWrap.$value.$endWrap ).$tldString;
                                }
                            }else{

                                $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                            }
                            
                        }

                        if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
                            
                            if($in == 'suffix'){
                                
                                if( substr($keword, -2) == 'es' ){
                                    $keword = substr_replace($keword ,"",-2);
                                    $mainstringN[] = ucwords( $keword.''.$startWrap.$value.$endWrap ).$tldString;
                                }

                                if( substr($keword, -3) == 'ing' ){
                                    //$keword = substr_replace($keword ,"",-3);
                                    $keword = $keword;
                                    $mainstringN[] = ucwords( $keword.''.$startWrap.$value.$endWrap ).$tldString;
                                }
                            }else{

                                $mainstringN[] = ($position === 'after') ? ucwords( $value.$separator.$startWrap.$keword.$endWrap ).$tldString: ucwords( $keword.$separator.$startWrap.$value.$endWrap ).$tldString;
                            }
                            
                        }
                        
                        if(!$length || $length > 15){

                            $mainstringN[] = ($position === 'after') ? ucwords($value.$separator.$startWrap.$keword.$endWrap).$tldString: ucwords($keword.$separator.$startWrap.$value.$endWrap).$tldString;
                        }
                    }
                }
            }
        }

        shuffle($mainstringN);
        
        return $mainstringN;
    }



    // Internation Old Stuff

    function international_domains_list($search_word)
    {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();

        $domainslist = array();
        $myrows = '';

        if (is_array($search_word)) {
            if (count($search_word) > 1) {
                $words = $search_word;
                $condition = '';

                foreach ($words as $value) {
                    if (end($words) != $value) {
                        $condition .= ' price < 4000) AND (title LIKE "%' . $value . '%" OR tags LIKE "%' . $value . '%" OR categories LIKE "%' . $value . '%" OR';
                    } else {
                        $condition .= ' title LIKE "%' . $value . '%" OR tags LIKE "%' . $value . '%" OR categories LIKE "%' . $value . '%") AND status = "1" ORDER BY price DESC LIMIT 8';
                    }
                }

                $myrows = $this->db->get_results("SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE ($condition", ARRAY_A);
            } else {

                $condition = '';

                $condition .= 'price < 4000) AND (title LIKE "%' . $search_word[0] . '%" OR tags LIKE "%' . $search_word[0] . '%" OR categories LIKE "%' . $search_word[0] . '%") AND status = "1" ORDER BY price DESC LIMIT 8';

                $myrows = $this->db->get_results("SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (" . $condition . "", ARRAY_A);
            }
        } else {

            $condition = '';

            $condition .= 'price < 4000) AND ( title LIKE "%' . $search_word . '%" OR tags LIKE "%' . $search_word . '%" OR categories LIKE "%' . $search_word . '%") AND status = "1" ORDER BY price DESC LIMIT 8';

            $myrows = $this->db->get_results("SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (" . $condition . "", ARRAY_A);
        }


        if ($myrows == 0 || $myrows == false) {

            $condition = '';
            $condition .= ' price < 4000) AND categories LIKE "%Brandable%" AND status = "1" ORDER BY price DESC LIMIT 8';

            $myrowsres = $this->db->get_results(" SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (" . $condition . "", ARRAY_A);
        }


        if ($myrows != 0 || $myrows != false || count($myrows) < 8) {

            $condition = '';

            $condition .= ' price < 4000) AND categories LIKE "%Brandable%" AND status = "1" ORDER BY price DESC';

            $myrowsres = $this->db->get_results(" SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (" . $condition . "", ARRAY_A);
        }


        if (!empty($myrowsres) && count($myrows) < 8 && !empty($myrows)) {

            return $domainslist = array_merge($myrows, $myrowsres);
        } else if (!empty($myrowsres) && empty($myrows)) {

            return $domainslist = $myrowsres;
        } else {

            return $domainslist = $myrows;
        }
    }

    function international_general_list($search_word, $position, $length)
    {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $mainstringN = array();
        $start = 0;
        $myrows = $this->db->get_results("select general_list from `wp_eng_generator` ", ARRAY_A);

        if (preg_split('/\s+/', $search_word) > 1) {
            $kewords = explode(' ', $search_word);
            $cnt = 0;

            foreach ($kewords as $keword) {
                $keword = $this->serach_word_senitize($keword);

                foreach ($myrows as $key => $row) {
                    foreach ($row as $in => $value) {
                        if (!empty($value)) {

                            if ($length < 15 && (strlen($row[$in]) == $length || strlen($row[$in]) < $length)) {
                                if ($position == 'after' || $position == 'before') {
                                    $mainstringN[] = ($position === 'after') ? ucwords($value . $separator .$startWrap. $keword.$endWrap).$tldString : ucwords($keword . $separator .$startWrap. $value.$endWrap).$tldString;
                                } else {
                                    switch ($cnt % 2) {
                                        case 0:
                                            $mainstringN[] = ucwords($value . $separator .$startWrap. $keword. $endWrap).$tldString;
                                            break;

                                        default:
                                            $mainstringN[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                            break;
                                    }
                                }
                                $cnt++;
                            }

                            if ($length == 15 && (strlen($row[$in]) == $length || strlen($row[$in]) > $length || strlen($row[$in]) < $length)) {
                                if ($position == 'after' || $position == 'before') {
                                    $mainstringN[] = ($position === 'after') ? ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString : ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                } else {
                                    switch ($cnt % 2) {
                                        case 0:
                                            $mainstringN[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                            break;

                                        default:
                                            $mainstringN[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                            break;
                                    }
                                }
                                $cnt++;
                            }

                            if (!$length || $length > 15) {
                                if ($position == 'after' || $position == 'before') {
                                    $mainstringN[] = ($position === 'after') ? ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString : ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                } else {
                                    switch ($cnt % 2) {
                                        case 0:
                                            $mainstringN[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                            break;

                                        default:
                                            $mainstringN[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                            break;
                                    }
                                }
                                $cnt++;
                            }
                        }
                    }
                }
            }
        } else {
            $keword = $this->serach_word_senitize($search_word);
            $cnt = 0;

            foreach ($myrows as $key => $row) {
                foreach ($row as $in => $value) {
                    if (!empty($value)) {
                        if ($length < 15 && (strlen($row[$in]) == $length || strlen($row[$in]) < $length)) {
                            if ($position == 'after' || $position == 'before') {
                                $mainstringN[] = ($position === 'after') ? ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString : ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                            } else {
                                switch ($cnt % 2) {
                                    case 0:
                                        $mainstringN[] = ucwords($value . $separator . $startWrap.  $keword . $endWrap).$tldString;
                                        break;

                                    default:
                                        $mainstringN[] = ucwords($keword . $separator . $startWrap .$value . $endWrap).$tldString;
                                        break;
                                }
                            }
                            $cnt++;
                        }

                        if ($length == 15 && (strlen($row[$in]) == $length || strlen($row[$in]) > $length || strlen($row[$in]) < $length)) {
                            if ($position == 'after' || $position == 'before') {
                                $mainstringN[] = ($position === 'after') ? ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString : ucwords($keword . $separator . $startWrap . $value . $endWrap).$tldString;
                            } else {
                                switch ($cnt % 2) {
                                    case 0:
                                        $mainstringN[] = ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString;
                                        break;

                                    default:
                                        $mainstringN[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                        break;
                                }
                            }
                            $cnt++;
                        }

                        if (!$length || $length > 15) {
                            if ($position == 'after' || $position == 'before') {
                                $mainstringN[] = ($position === 'after') ? ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString : ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                            } else {
                                switch ($cnt % 2) {
                                    case 0:
                                        $mainstringN[] = ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString;
                                        break;

                                    default:
                                        $mainstringN[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                        break;
                                }
                            }
                            $cnt++;
                        }
                    }
                }
            }
        }

        return $mainstringN;
    }


    function international_single_filter_wordgen($search_word, $industry, $position, $tbl, $grammar, $lang)
    {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $mainstringN = array();
        $mainif = [];
        $mainElseIf = [];
        $mainElse = [];
        $col = '';

        foreach ($industry as $value) {
            $col = '"' . trim($value) . '"';
        }
        if (!empty($grammar)) {
            $grammarcol = "";
            foreach ($grammar as $value) {
                if (end($grammar) != $value) {
                    $grammarcol .= str_replace(" ", "_", trim($value)) . ', ';
                } else {
                    $grammarcol .= str_replace(" ", "_", trim($value)) . '';
                }
            }
            $myrows = $this->db->get_results("select $grammarcol, use_before_only, use_after_only, use_before_after, use_as_ending_only from $tbl where industry=$col", ARRAY_A);
        } else {

            if ($lang == 'de' || $lang == 'es' || $lang == 'fr') {
                $myrows = $this->db->get_results("select keyword,use_before_only, use_after_only, use_before_after, use_as_ending_only,masculine,feminine,plural from $tbl where industry=$col", ARRAY_A);
            } else {
                $myrows = $this->db->get_results("select keyword,use_before_only, use_after_only, use_before_after, use_as_ending_only,masculine, feminine, masculine_plural, feminine_plural from $tbl where industry=$col", ARRAY_A);
            }
        }

        /**
         *  Instance 1) Joining the Search keywords with filters
            **/

        /**
         * @param $search_word more than one
         **/



        $cnt = 0;
        if (!empty($grammar)) {

            if (count(preg_split('/\s+/', $search_word)) > 1) {
                $kewords = explode(' ', $search_word);
                foreach ($kewords as $keword) {
                    foreach ($myrows as $key => $row) {
                        foreach ($row as $column => $value) {
                            if (!empty($value) && strtolower($value) != 'yes') {
                                if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                    } else
                                        $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                    if (strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        $mainElseIf[] = ucwords($keword . '-' .$startWrap. $hiphen. $endWrap).$tldString;
                                    } else
                                        $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                } else {
                                    if (strtolower($row['use_before_after']) == 'yes') {
                                        if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                            $hiphen = str_replace('-', '', $value);
                                            switch ($cnt % 2) {
                                                case 0:
                                                    $mainElse[] = ucwords($hiphen . '-' . $startWrap.  $keword. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($keword . '-' . $startWrap.  $hiphen . $endWrap).$tldString;
                                                    break;

                                                default:
                                                    $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword . $endWrap).$tldString;
                                                    break;
                                            }
                                        } else {
                                            switch ($cnt % 2) {
                                                case 0:
                                                    $mainElse[] = ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString;
                                                    $mainElse[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                                    break;

                                                default:
                                                    $mainElse[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                                    $mainElse[] = ucwords($value . $separator . $startWrap .  $keword. $endWrap).$tldString;
                                                    break;
                                            }
                                        }
                                    }
                                }
                                $cnt++;
                            }
                        }
                    }
                }
            } else {
                $keword = $search_word;
                foreach ($myrows as $key => $row) {
                    foreach ($row as $column => $value) {
                        if (!empty($value) && strtolower($value) != 'yes') {
                            if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                } else
                                    $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                            } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                if (strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen . $endWrap).$tldString;
                                } else
                                    $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                            } else {
                                if (strtolower($row['use_before_after']) == 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword . $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . '-' . $startWrap . $hiphen .$endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                break;
                                        }
                                    } else {
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                break;
                                        }
                                    }
                                }
                            }
                            $cnt++;
                        }
                    }
                }
            }
        } else {
            if (count(preg_split('/\s+/', $search_word)) > 1) {
                $kewords = explode(' ', $search_word);
                foreach ($kewords as $keword) {
                    foreach ($myrows as $key => $row) {
                        foreach ($row as $column => $value) {
                            if (!empty($value) && strtolower($value) != 'yes') {
                                if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                    } else
                                        $mainif[] = ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString;
                                } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                    if (strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                    } else
                                        $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                } else {
                                    if (strtolower($row['use_before_after']) == 'yes') {
                                        if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                            $hiphen = str_replace('-', '', $value);
                                            switch ($cnt % 2) {
                                                case 0:
                                                    $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                    break;

                                                default:
                                                    $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen . $endWrap).$tldString;
                                                    $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                    break;
                                            }
                                        } else {
                                            switch ($cnt % 2) {
                                                case 0:
                                                    $mainElse[] = ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString;
                                                    $mainElse[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                                    break;

                                                default:
                                                    $mainElse[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                                                    $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                    break;
                                            }
                                        }
                                    }
                                }
                                $cnt++;
                            }
                        }
                    }
                }
            } else {
                $keword = $search_word;
                foreach ($myrows as $key => $row) {
                    foreach ($row as $column => $value) {
                        if (!empty($value) && strtolower($value) != 'yes') {
                            if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword.$endWrap).$tldString;
                                } else
                                    $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                            } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                if (strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                } else
                                    $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                            } else {
                                if (strtolower($row['use_before_after']) == 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                break;
                                        }
                                    } else {
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                break;
                                        }
                                    }
                                }
                            }
                            $cnt++;
                        }
                    }
                }
            }
        }

        if ($position == 'after')
            $mainstringN = $mainif;
        elseif ($position == 'before')
            $mainstringN = $mainElseIf;
        else
            $mainstringN = $mainElse;

        shuffle($mainstringN);

        return array_unique($mainstringN);
    }

    function international_multi_filter_wordgen($search_word, $industry, $position, $tbl, $grammar, $lang)
    {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $mainstringN = array();
        $mainif = [];
        $mainElseIf = [];
        $mainElse = [];
        $col = '';
        $start = 0;
        foreach ($industry as $value) {
            if (end($industry) != $value) {
                $col .= 'industry="' . trim($value) . '" OR ';
            } else {
                $col .= 'industry="' . trim($value) . '"';
            }
        }

        if (!empty($grammar)) {
            $grammarcol = "";
            foreach ($grammar as $value) {
                if (end($grammar) != $value) {
                    $grammarcol .= str_replace(" ", "_", trim($value)) . ', ';
                } else {
                    $grammarcol .= str_replace(" ", "_", trim($value)) . '';
                }
            }
            $myrows = $this->db->get_results("select $grammarcol, use_before_only, use_after_only, use_before_after, use_as_ending_only from $tbl where $col", ARRAY_A);
        } else {

            if ($lang == 'de' || $lang == 'es' || $lang == 'fr') {
                $myrows = $this->db->get_results("select keyword,use_before_only, use_after_only, use_before_after, use_as_ending_only,masculine,feminine,plural from $tbl where $col", ARRAY_A);
            } else {
                $myrows = $this->db->get_results("select keyword,use_before_only, use_after_only, use_before_after, use_as_ending_only,masculine, feminine, masculine_plural, feminine_plural from $tbl where $col", ARRAY_A);
            }
        }

        /**
         *  Instance 2) Joining the Search keywords with filters
            *   
            **/
        $cnt = 0;
        if (!empty($grammar)) {

            if (count(preg_split('/\s+/', $search_word)) > 1) {
                $kewords = explode(' ', $search_word);
                foreach ($kewords as $keword) {
                    foreach ($myrows as $key => $row) {
                        foreach ($row as $column => $value) {
                            if (!empty($value) && strtolower($value) != 'yes') {
                                if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        $mainif[] = ucwords($hiphen . '-' . $startWrap.$keword. $endWrap).$tldString;
                                    } else
                                        $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                    if (strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                    } else
                                        $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                } else {
                                    if (strtolower($row['use_before_after']) == 'yes') {
                                        if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                            $hiphen = str_replace('-', '', $value);
                                            switch ($cnt % 2) {
                                                case 0:
                                                    $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($keword . '-' . $startWrap.$hiphen. $endWrap).$tldString;
                                                    break;

                                                default:
                                                    $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($hiphen . '-' . $startWrap.$keword. $endWrap).$tldString;
                                                    break;
                                            }
                                        } else {
                                            switch ($cnt % 2) {
                                                case 0:
                                                    $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($keword . $separator .  $startWrap. $value. $endWrap).$tldString;
                                                    break;

                                                default:
                                                    $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                    break;
                                            }
                                        }
                                    }
                                }
                                $cnt++;
                            }
                        }
                    }
                }
            } else {
                $keword = $search_word;
                foreach ($myrows as $key => $row) {
                    foreach ($row as $column => $value) {
                        if (!empty($value) && strtolower($value) != 'yes') {
                            if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                } else
                                    $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                            } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                if (strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                } else
                                    $mainElseIf[] = ucwords($keword . $separator . $startWrap.  $value. $endWrap).$tldString;
                            } else {
                                if (strtolower($row['use_before_after']) == 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap ).$tldString;
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                $mainElse[] = ucwords($hiphen .  '-' . $startWrap. $keword. $endWrap).$tldString;
                                                break;
                                        }
                                    } else {
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                break;
                                        }
                                    }
                                }
                            }
                            $cnt++;
                        }
                    }
                }
            }
        } else {
            if (count(preg_split('/\s+/', $search_word)) > 1) {
                $kewords = explode(' ', $search_word);
                foreach ($kewords as $keword) {
                    foreach ($myrows as $key => $row) {
                        foreach ($row as $column => $value) {
                            if (!empty($value) && strtolower($value) != 'yes') {
                                if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                    } else
                                        $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                    if (strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                    } else
                                        $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                } else {
                                    if (strtolower($row['use_before_after']) == 'yes') {
                                        if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                            $hiphen = str_replace('-', '', $value);
                                            switch ($cnt % 2) {
                                                case 0:
                                                    $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword.$endWrap).$tldString;
                                                    $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                    break;

                                                default:
                                                    $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                    break;
                                            }
                                        } else {
                                            switch ($cnt % 2) {
                                                case 0:
                                                    $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                    break;

                                                default:
                                                    $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                    $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                    break;
                                            }
                                        }
                                    }
                                }
                                $cnt++;
                            }
                        }
                    }
                }
            } else {
                $keword = $search_word;
                foreach ($myrows as $key => $row) {
                    foreach ($row as $column => $value) {
                        if (!empty($value) && strtolower($value) != 'yes') {
                            if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                } else
                                    $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                            } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                if (strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                } else
                                    $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value . $endWrap).$tldString;
                            } else {
                                if (strtolower($row['use_before_after']) == 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                break;
                                        }
                                    } else {
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString;
                                                break;
                                        }
                                    }
                                }
                            }
                            $cnt++;
                        }
                    }
                }
            }
        }
    
        if ($position == 'after')
            $mainstringN = $mainif;
        elseif ($position == 'before')
            $mainstringN = $mainElseIf;
        else
            $mainstringN = $mainElse;

        shuffle($mainstringN);

        return array_unique($mainstringN);
    }

    function international_all_wordgen($search_word, $industry, $position, $tbl, $lang, $grammar)
    {
        $tldString = $this->getTldString();
        $separator = $this->getSeparator();
        $startWrap = $this->getStartWrapTag();
        $endWrap = $this->getEndWrapTag();

        $mainstringN = array();
        $mainif = [];
        $mainElseIf = [];
        $mainElse = [];
        /**
         *  Instance 3) If there are multiple serach keywords
            **/

        if (!empty($grammar)) {
            $grammarcol = "";
            foreach ($grammar as $value) {
                if (end($grammar) != $value) {
                    $grammarcol .= str_replace(" ", "_", trim($value)) . ', ';
                } else {
                    $grammarcol .= str_replace(" ", "_", trim($value)) . '';
                }
            }
            $myrows = $this->db->get_results("select $grammarcol, use_before_only, use_after_only, use_before_after, use_as_ending_only from $tbl", ARRAY_A);
        } else {

            if ($lang == 'de' || $lang == 'es' || $lang == 'fr') {
                $myrows = $this->db->get_results("select keyword,use_before_only, use_after_only, use_before_after, use_as_ending_only,masculine,feminine,plural from $tbl", ARRAY_A);
            } else {
                $myrows = $this->db->get_results("select keyword,use_before_only, use_after_only, use_before_after, use_as_ending_only,masculine, feminine, masculine_plural, feminine_plural from $tbl", ARRAY_A);
            }
        }

        /**
         *  Instance 3) Joining the Search keywords with filters
            *   
            **/
        if (count(preg_split('/\s+/', $search_word)) > 1) {

            $kewords = explode(' ', $search_word);
            $cnt = 0;

            foreach ($kewords as $keword) {
                $keword = $this->serach_word_senitize($keword);

                foreach ($myrows as $key => $row) {
                    foreach ($row as $in => $value) {
                        if (!empty($value) && strtolower($value) != 'yes') {
                            if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                                if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                } else
                                    $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                            } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                                if (strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                } else
                                    $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                            } else {
                                if (strtolower($row['use_before_after']) == 'yes') {
                                    if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                        $hiphen = str_replace('-', '', $value);
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                                $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                                break;
                                        }
                                    } else {
                                        switch ($cnt % 2) {
                                            case 0:
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                break;

                                            default:
                                                $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                                $mainElse[] = ucwords($value . $separator . $startWrap. $keword . $endWrap).$tldString;
                                                break;
                                        }
                                    }
                                }
                            }
                            $cnt++;
                        }
                    }
                }
            }
        } else {

            $keword = $this->serach_word_senitize($search_word);
            $cnt = 0;

            foreach ($myrows as $key => $row) {
                foreach ($row as $in => $value) {
                    if (!empty($value) && strtolower($value) != 'yes') {
                        if ($position == 'after' && strtolower($row['use_after_only']) != 'yes') {
                            if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                $hiphen = str_replace('-', '', $value);
                                $mainif[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                            } else
                                $mainif[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                        } elseif ($position == 'before' && strtolower($row['use_before_only']) != 'yes') {
                            if (strpos($value, '-') === 0) {
                                $hiphen = str_replace('-', '', $value);
                                $mainElseIf[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                            } else
                                $mainElseIf[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                        } else {
                            if (strtolower($row['use_before_after']) == 'yes') {
                                if (strpos($value, '-') > 0 || strpos($value, '-') === 0) {
                                    $hiphen = str_replace('-', '', $value);
                                    switch ($cnt % 2) {
                                        case 0:
                                            $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                            $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                            break;

                                        default:
                                            $mainElse[] = ucwords($keword . '-' . $startWrap. $hiphen. $endWrap).$tldString;
                                            $mainElse[] = ucwords($hiphen . '-' . $startWrap. $keword. $endWrap).$tldString;
                                            break;
                                    }
                                } else {
                                    switch ($cnt % 2) {
                                        case 0:
                                            $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                            $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                            break;

                                        default:
                                            $mainElse[] = ucwords($keword . $separator . $startWrap. $value. $endWrap).$tldString;
                                            $mainElse[] = ucwords($value . $separator . $startWrap. $keword. $endWrap).$tldString;
                                            break;
                                    }
                                }
                            }
                        }
                        $cnt++;
                    }
                }
            }
        }
    

        if ($position == 'after')
            $mainstringN = $mainif;
        elseif ($position == 'before')
            $mainstringN = $mainElseIf;
        else
            $mainstringN = $mainElse;

        shuffle($mainstringN);

        return array_unique($mainstringN);
    }

    
}
