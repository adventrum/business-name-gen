<?php

/**
 * 
 * Trigger this file on Plugin Uninstall
 * 
 * @package BusinessNameWordManager
 * 
 */

 if( !defined( 'WP_UNINSTALL_PLUGIN' ) ){
     die;
 }

 // Clear data stored in database