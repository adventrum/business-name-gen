<div class="wrap word-manager-container">
    <h1 class="wp-heading-inline">Categories Manager</h1>
    <a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_word_categories&add_word_category=true" class="page-title-action">Add New</a>
    <form>
        <h2 class="wp-heading-inline">All Categories</h2>
        <div class="tablenav top">
            <div class="tablenav-pages"><span class="displaying-num"><?php echo $viewData['count']; ?> items</span>
                        <?php echo $viewData['pagination']; ?>
            </div>
            <br class="clear">
        </div>
        <table class="wp-list-table widefat fixed striped table-view-list pages">
            <thead>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>Name</span></th>
                    <th scope="col" class="manage-column column-status column-primary"><span>Status</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>No. of words</span></th>
                </tr>
            </thead>

            <tbody id="cats-list">
                <?php foreach( $viewData['data'] as $key => $rq ): ?>
                <tr id="cat-<?php echo $key; ?>" class="iedit author-self level-0 request-<?php echo $key; ?> type-page status-publish">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                        <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                        <strong><a class="row-title" href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_word_categories&edit_cat_id=<?php echo $rq->id; ?>" aria-label="“<?php echo $rq->name; ?>” (Edit)"><?php echo ucfirst($rq->name); ?></a></strong>
                        <button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
                    </td>
                    <td><?php echo $rq->status; ?></td>
                    <td><a class="row-title" href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words&word_category_id=<?php echo $rq->id; ?>" aria-label="“<?php echo $rq->name; ?>” (Edit)"><?php echo $rq->total_words; ?></a></td>
                </tr>
                <?php endforeach; ?>
            </tbody>

            <tfoot>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>Name</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>No. of words</span></th>
                </tr>
            </tfoot>

        </table>
        <div class="tablenav bottom">
            <div class="tablenav-pages"><span class="displaying-num"><?php echo $viewData['count']; ?> items</span>
                <?php echo $viewData['pagination']; ?>
            </div>
        </div>
    </form>
</div>