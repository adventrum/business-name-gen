<form method="post" id="saveEmailOptions" enctype="multipart/form-data">
    <fieldset>
        <legend><h2>Email Template Settings</h2></legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Email Subject</label>
                    </th>
                    <td>
                        <input type="text" name="email_subject" value="<?php echo isset($emailSettings->email_subject) ? $emailSettings->email_subject : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required header_logo">
                    <th scope="row">
                        <label for="name">Header Logo</label>
                    </th>
                    <td>
                        <?php 
                            $headerLogo = isset($emailSettings->header_logo) ? $emailSettings->header_logo : '';
                            $headerImgUrl = $headerLogo == '' ? '' : wp_get_attachment_url($headerLogo);
                        ?>
                        <img width="300" src="<?php echo $headerImgUrl; ?>" class="image">
                        <input type="button" value="Choose Logo" class="button-primary" id="header_logo">
                        <input type="hidden" name="header_logo" class="wp_attachment_id" value="<?php echo $headerLogo; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required header_logo">
                    <th scope="row">
                        <label for="name">Header right text</label>
                    </th>
                    <td>
                        <input type="text" name="header_right_text" value="<?php echo isset($emailSettings->header_right_text) ? $emailSettings->header_right_text : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required header_logo">
                    <th scope="row">
                        <label for="name">Header right URL</label>
                    </th>
                    <td>
                        <input type="text" name="header_right_url" value="<?php echo isset($emailSettings->header_right_url) ? $emailSettings->header_right_url : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required header_logo">
                    <th scope="row">
                        <label for="name">Saved Ideas Title</label>
                    </th>
                    <td>
                        <input type="text" name="saved_ideas_title" value="<?php echo isset($emailSettings->saved_ideas_title) ? $emailSettings->saved_ideas_title : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required header_logo">
                    <th scope="row">
                        <label for="name">Register Button Text</label>
                    </th>
                    <td>
                        <input type="text" name="register_button_text" value="<?php echo isset($emailSettings->register_button_text) ? $emailSettings->register_button_text : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Footer Title Text</label>
                    </th>
                    <td>
                        <input type="text" name="footer_title_text" value="<?php echo isset($emailSettings->footer_title_text) ? $emailSettings->footer_title_text : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label for="name">Footer Description</label>
                    </th>
                    <td>
                    <?php
                        $id = "footer_description";
                        $name = 'footer_description';
                        $content = isset($emailSettings->footer_description) ? stripslashes_deep($emailSettings->footer_description) : '';
                        $settings = array('tinymce' => true, 'textarea_name' => $name);
                        wp_editor($content, $id, $settings);
                    ?>
                    </td>
                </tr>
                <tr class="form-field form-required header_logo">
                    <th scope="row">
                        <label for="name">Footer Button Text</label>
                    </th>
                    <td>
                        <input type="text" name="footer_button_text" value="<?php echo isset($emailSettings->footer_button_text) ? $emailSettings->footer_button_text : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required footer_image">
                    <th scope="row">
                        <label for="name">Footer Image</label>
                    </th>
                    <td>
                        <?php 
                            $footerImg = isset($emailSettings->footer_image) ? $emailSettings->footer_image : '';
                            $footerImgUrl = $footerImg == '' ? '' : wp_get_attachment_url($footerImg);
                        ?>
                        <img width="300" src="<?php echo $footerImgUrl; ?>" class="image">
                        <input type="button" value="Choose Image" class="button-primary" id="footer_image">
                        <input type="hidden" name="footer_image" class="wp_attachment_id" value="<?php echo $footerImg; ?>">
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    

    <?php
    $btnText = (isset($id)) ? "Update Settings" : "Save Settings";
    ?>
    <p class="submit"><input type="submit" name="emailSettings" id="emailSettings-btn" class="button button-primary" value="<?php echo $btnText; ?>"></p>

</form>