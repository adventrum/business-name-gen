<?php

get_header();


require_once "Mobile_Detect.php";
$detect = new Mobile_Detect;


if( !$detect->isMobile() && !$detect->isTablet() ):
  $limit = 96;
  $_GET['limit'] = 96;
endif;

if( $detect->isTablet() ):
  $limit = 72;
  $_GET['limit'] = 72;
endif;

if( $detect->isMobile() && !$detect->isTablet() ):
  $limit = 30;
  $_GET['limit'] = 30;
endif;



include_once(dirname(__FILE__) . '/inc/Php-functions.php');

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$_GET['paged'] = $paged;

$results = bnwm_search_results($_GET);

$mainstring2 = ( !empty($results['result']['words']) ) ? $results['result']['words'] : ''; 

include_once(dirname(__FILE__) . '/inc/template-js.php');

$bname =   (!empty($_GET['bname'])) ? $_GET['bname'] : '';
$domains = (!empty($bname)) ? getDomainList($bname,$paged,8) : '';

$translations = json_decode(get_option('bnwm_string_translations'));
$total = '';


?>
 <div class="gen-search" style="display: none;">
        <div class="name-search">
            <form class="generator-form" method="GET" action="<?= home_url('/nameideas/'); ?>">
            
            <input type="hidden" value="<?= isset($_GET['home_id']) ? $_GET['home_id'] : '' ?>" name="home_id">
            
            <input class="inputMain" name="bname" type="text" value="<?= $bname; ?>" placeholder="<?= isset($translations->nameideasgplaceholdertext) ? $translations->nameideasgplaceholdertext : 'Enter words and click generate...';   ?>">  

            <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                <input type="hidden" name="stickyads" value="true">
            <?php endif;  ?>  
            
            <?php if(isset($_GET['sidebarads-category']) && !empty($_GET['sidebarads-category'])  ) : ?> 
                <input type="hidden" name="sidebarads-category" value="<?= $_GET['sidebarads-category']; ?>">
            <?php endif;  ?>  

            <?php if(isset($_GET['stickyads-category']) && !empty($_GET['stickyads-category'])  ) : ?> 
                <input type="hidden" name="stickyads-category" value="<?= $_GET['stickyads-category']; ?>">
            <?php endif;  ?>   

            <?php if(isset($_GET['device']) && !empty($_GET['device'])) : ?>
                <input type="hidden" name="device" value="<?=  trim($_GET['device']);  ?>">
            <?php endif; ?> 
            
            <?php if(isset($_GET['all-indus']) && $_GET['all-indus'] == 'all') : ?>
                <?php 
                    global $wpdb;
                    $columsname = $wpdb->get_col("SELECT count(*) FROM ".$wpdb->prefix."bnwm_word_categories  WHERE status = 'Active'");
                    $value = $columsname == count($_GET['industry'] ) ? ' value="all"': '';
                ?>
                    <input type="hidden" name="all-indus"<?php echo $value; ?>>
            <?php endif; ?>
            
            <input type="hidden" name="fltr" value="rsult">

            <input type="hidden" class="pos" name="position" value="<?php echo (isset($_GET['position']) && $_GET['position'] != 'before') ? trim($_GET['position']) : 'before'; ?>">

            <input type="hidden" class="one-word" name="one_word" value="<?php echo (isset($_GET['one_word']) && !empty($_GET['one_word']) ) ? trim($_GET['one_word']) : 'on'; ?>">

            <input type="hidden" class="two-word" name="two_word" value="<?php echo (isset($_GET['two_word']) && !empty($_GET['two_word']) ) ? trim($_GET['two_word']) : 'on'; ?>">

            <input type="hidden" class="synonyms-val" name="synonyms" value="<?php echo (isset($_GET['synonyms']) && $_GET['synonyms'] != 'on') ? trim($_GET['synonyms']) : 'on'; ?>">

            <input type="hidden" class="rhyming-val" name="rhyming" value="<?php echo (isset($_GET['rhyming']) && $_GET['rhyming'] == 'on') ? 'on' : 'off'; ?>">

            <?php if(isset($_GET['character']) && !empty($_GET['character'])) : ?>
                <input type="hidden" name="character" value="<?= $_GET['character']; ?>">
            <?php endif; ?> 

                <?php if( is_array($grammar) || is_object($grammar)):
                
                $grammar = (isset($_GET['grammar']) && !empty($_GET['grammar'])) ? $_GET['grammar'] : '';
                foreach ($grammar as $value) : 
                ?>
                <input type="hidden" name="grammar[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach; endif; ?>

                <?php

                if (is_array($industry) || is_object($industry)) :
                $industry = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry'] : '';
                foreach ($industry as $value) : ?>
                    <input type="hidden" name="industry[]" value="<?php $v = trim($value); echo $v; ?>">
                <?php endforeach;
                endif; ?>   

                <?php if( isset($_GET['tlds']) && !empty($_GET['tlds']) ) : ?>
                <input type="hidden" name="tlds" value="<?= $_GET['tlds']; ?>">
                <?php endif; ?> 

                <a href="javascript:void(0)" class="generatorbtn"><?= isset($translations->nameideasgeneratetext) ? $translations->nameideasgeneratetext : 'Generate';   ?></a>
        </form>
        </div>               
    </div>
  <div class="main_wraper2 business-eng result-page">
  
      <!-- Alert Message -->
      <div class="container alert-box">

          <div class="row d-flex">

             


              <!-- Filter html starts -->
                
              <div class="col-md-3">
                <div class="filter-wrap" id="stickThis">
                  
                  <div class="busi-filter search-filter">
                    
                    <div class="mar_set"> 
                    <?php  
                        $style = "style='display: none;'"; 
                        if( !getNameideaAlgo() ) : 
                          $style = '';
                          $total = $results['total'];
                          $totalFormat = number_format($total);
                        endif;
                     ?>
                    <div class="title-found hidden-xs" data-text="<?= $translations->nameideasresults; ?>" <?= $style; ?>>
                        <h4><?= $translations->nameideasfilter;  ?> 
                            <span>
                                <?php 
                                if ($total > 0) :
                                    echo sprintf('%s '.$translations->nameideasresults.'', $totalFormat);
                                else :
                                echo '0'.$translations->nameideasresults;
                                endif; ?>
                            </span>
                        </h4>
                    </div>
                      <?php showWordManagerFilters($results['category']); ?>
                          <div class="resultpage-sidebar-banner">
                            <?php sidebar_banner(); ?>
                          </div>  
                        </div>
                        <?php showWordManagerFilters($results['category'], 'nameideas-toggles'); ?>
                        </div>
                                
                        <?php 
                          $savedideas = $_SESSION['invite_modal_value'];
                        ?>

                                
                          <div class="col-md-12 mar_0 hide-section">

                            <?php if (isset($_SESSION['messages']) && !empty($_SESSION['messages'])) : ?>
                              <?php $alert = $_SESSION['messages'];
                              if( is_array($alert) || is_object($alert)):
                              foreach ($alert as $key => $value) :
                                if (!empty($value['message'])) :
                              ?>
                                  <?php if ($_SESSION['show'] !==  $value['message']) : ?>
                                    <div class="alert alert-<?php echo $value['type']; ?>">
                                      <a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>
                                      <p><?php echo $value['message']; ?></p>
                                    </div>
                                  <?php $_SESSION['show'] = $value['message'];
                                  endif; ?>
                                <?php endif; ?>
                              <?php endforeach; endif; ?>
                            <?php endif; ?>

                          </div>
                         </div> <!-- Filter html end -->

                         <!-- Generator result html starts -->
                              
                         <div class="col-sm-9 mar_0">
                            
                            <div class="title_info">
                                
                              <div class="filter_block eng">
                                <div class="imgloadingshow" style="display:none;">
                                  <div class="inner">
                                    <img src="<?= WORD_MANAGER_PLUGIN_URL.'assets/images/BNG-loader.gif'?>" />
                                  </div>
                                </div>
                                
                                <div class="check_avail">
                                     
                                    <?php $response = placement_banner();  
                                      if( in_array(getenv('HTTP_GEOIP_COUNTRY_CODE'), getGeoCountriesList()) && !empty($response)  ) : 
                                       headerbannerGeoBased();   
                                     else :
                                    ?>
                                       <h4 class="check-dom-txt">
                                         <?= $translations->nameideastextadvert; ?>
                                        </h4>
                                           
                                        <?php
                                          $affiliate_link = WixAffiliateLinks()['textadvertlink'];
                                        ?>

                                       <a class="outbound-link" href="<?= $affiliate_link; ?>" data-category="ResultsPage" data-action="Adverts" data-label="TextAdvert" target="_blank">
                                         <p>
                                           <?= $translations->nameideasaffiliateadverttext; ?>
                                         </p>
                                       </a> 
                                      
                                    <?php  endif; ?>

                                </div>
                                    
                                <div class="row above-row">
                                   <div class="col-md-12 pad_set">
                                     <ul class="list-star">
                                      <?php
                                       
                                       $tool_tip = $translations->nameideastooltip;

                                       if ( !empty($mainstring2) &&  is_array($mainstring2)   ) {
                                         
                                         $i = 0; $count = 0;
                                         $general_list_index = (!empty($_SESSION['general_list_index'])) ?  $_SESSION['general_list_index'] : 0;
                                         
                                         if( is_array($mainstring2) || is_object($mainstring2)):
                                           foreach ($mainstring2 as $index => $mastring) {
                                           $i++;
                                           $number = $i . '' . $page;
                                        ?>
                                          <?php if ($mastring != '1' && $mastring != '') { ?>
                                            <li class="stardomain <?php echo $i; ?>">
                                              <div class="inner">
                                                <span class="domain-title"><a class="ntbs click-event nameidea-time-to-click" data-category="ResultsPage" data-action="NameIdea" data-label="<?php echo $mastring; ?>" data-string="<?php echo $mastring; ?>" href="javascript:void(0);">
                                                  <span><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                    <?php echo $mastring; ?>
                                                  </a><span class="tool-tip <?= strlen(addslashes($tool_tip)) > 50 ? 'big-text' : '';  ?>"><?php echo addslashes($tool_tip)  ?>.</span></span>
                                                <a class="clickable-arrow result_<?php echo $number; ?> click-event" href="javascript:void(0);" data-category="ResultsPage" data-action="SavedIdeas" data-label="SaveStar" onclick="setSelectedTestPlan('<?php echo $mastring; ?>','<?php echo $number; ?>', this);">
                                                  <span class="tool-tip-2"><?php echo $translations->nameideastooltipclicktosave; ?></span><span class="icon_star_alt elegant-icon result_<?php echo $number; ?> click-event" data-category="ResultsPage" data-action="SavedIdeas" data-label="SaveStar"></span>
                                                </a>
                                              </div>
                                            </li>
                                            <?php }
                                            ?>
                                            <?php
                                              //Tablet view results rows
                                              if ($detect->isTablet()) : ?>
                                                <?php if ($count == 17) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                        //Destop domains results Starts Row 1
                                        if (!empty($domains)) :
                                          if (count($domains) > 3) {
                                            $newlist = array_slice($domains, 0, 4);
                                          } else {
                                            $newlist = $domains;
                                          }
                                    ?>
                                    <div class="row mobile-products">
                                      
                                      <?php before_carousel_ads_domains();   ?> 

                                      <div class="col-md-12 domainify">
                                          <!-- Tab Top Domainify Products -->
                                          <?php carousel_ads_new($newlist);   ?>
                                     </div>
                                     
                                     <div class="mobile-view-more">
                                        <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                     </div>

                                    </div>
                                    <?php endif;
                                       //Destop domains results end Row 1
                                    ?>

                                    <div class="row below-row">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>

                                        <?php if ($count == 35) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                     // For Tablet Top View
                                     banner_ad();
                                    ?>
                                    <div class="row below-row">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>


                                        <?php if ($count == 53) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                        //Destop domains results Starts Row 2
                                        if (!empty($domains)) :
                                          if (count($domains) > 3) {
                                            $newlist = array_slice($domains, 4, 4);
                                          } else {
                                            $newlist = $domains;
                                          }
                                          // print_r($newlist);
                                    ?>
                                    <div class="row mobile-products">
                                        
                                        <?php before_carousel_ads_domains();   ?>  

                                        <div class="col-md-12 domainify">
                                             <!-- Tab Bottom Domainify Products -->
                                             <?php carousel_ads_new($newlist);   ?>
                                        </div>

                                        <div class="mobile-view-more">
                                           <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                        </div>

                                      </div>
                                    <?php endif; ?>
                                    <div class="row below-row">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif;
                                                //Destop domains results end Row 2
                                        ?>

                                      <?php
                                              //Mobile View results
                                              elseif ($detect->isMobile() && !$detect->isTablet()) : ?>
                                        <?php if ($count == 9) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                        //Destop domains results Starts Row 1
                                        if (!empty($domains)) :
                                          if (count($domains) > 4) {
                                            $newlist = array_slice($domains, 0, 4);
                                          } else {
                                            $newlist = $domains;
                                          }

                                    ?>
                                    <div class="row mobile-products">

                                      <?php before_carousel_ads_domains();   ?>

                                      <div class="col-md-12 domainify">
                                          <!-- Mobile Top Domainify Products -->
                                          <?php  carousel_ads_new($newlist);   
                                          ?>
                                      </div>

                                      <div class="mobile-view-more">
                                         <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                      </div>


                                    </div>
                                    <?php endif;
                                       //Destop domains results end Row 1
                                    ?>

                                    <div class="row below-row">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>

                                        <?php if ($count == 19) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                       // For Mobile Top View
                                       banner_ad();
                                    ?>
                                    <div class="row below-row">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>

                                      <?php else : //Destop View results 
                                      ?>

                                        <?php if ($count == 23) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php if (!empty($domains)) :
                                        //Destop domains results Starts

                                        if (count($domains) > 4) {
                                          $newlist = array_slice($domains, 0, 4);
                                        } else {
                                          $newlist = $domains;
                                        }

                                     ?>
                                    <div class="row mobile-products">

                                      <?php before_carousel_ads_domains();   ?>

                                      <div class="col-md-12 domainify">
                                          <!-- Desktop Top Domainify Products -->
                                          <?php carousel_ads_new($newlist);   ?>
                                      </div>

                                      <div class="mobile-view-more">
                                         <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                      </div>

                                    </div>
                                    <?php endif;
                                       //Destop domains results end 
                                    ?>

                                    <div class="row below-row">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>

                                        <?php if ($count == 47) : ?>
                                        </ul>
                                      </div>
                                    </div>
                                    
                                    <?php
                                      // For Desktop Top View
                                      banner_ad();
                                    ?>
                                    
                                    <div class="row below-row">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                        <?php endif; ?>


                                        <?php if ($count == 71) : ?>
                                        </ul>
                                      </div>
                                    </div>

                                    <?php
                                        //Destop domains results Starts Row 2
                                        if (!empty($domains)) :
                                          if (count($domains) > 4) {
                                            $newlist = array_slice($domains, 4, 4);
                                          } else {
                                            $newlist = $domains;
                                          }

                                    ?>
                                      <div class="row mobile-products">

                                        <?php before_carousel_ads_domains();   ?>

                                        <div class="col-md-12 domainify">
                                            <!-- Desktop Bottom Domainify Products -->
                                            <?php carousel_ads_new($newlist);   ?>
                                        </div>

                                        <div class="mobile-view-more">
                                           <a href="https://domainify.com/names/" target="_blank">View more premium domains  <span><i class="fas fa-arrow-right"></i></span></a>
                                        </div>
                                        
                                      </div>
                                    <?php endif;
                                                  //Destop domains results end row 2
                                    ?>

                                    <div class="row below-row">
                                      <div class="col-md-12 pad_set">
                                        <ul class="list-star">
                                          <?php endif; ?>

                                           <?php endif; ?>

                                          <?php $count++;
                                                } endif;
                                              } else {
                                                $rlocation = home_url();
                                              }
                                          ?>
                                        </ul>

                                        <?php if (!$detect->isMobile() && !$detect->isTablet()) : ?>
                                          <?php if(!empty($mainstringnew) && ( is_array($mainstringnew) && is_array($mainstring2)) ) : ?>
                                          <?php if (count($mainstringnew) < 50 || count($mainstring2) < 50) : ?>
                                            <?php echo do_shortcode('[insertadvnew id="36848"]'); ?>
                                          <?php endif; ?>
                                          <?php endif; ?>
                                        <?php endif; ?>
                                      
                                        <div class="Pagination_nav">
                                         <?php  if ( !getNameideaAlgo() ) {
                                            echo get_paginationWithoutAJax($total, $limit);
                                          }
                                         ?>
                                        </div>
                                        

                                        <?php if (!$detect->isMobile() && !$detect->isTablet()) : ?>
                                         <?php
                                           // For Bottom Desktop Ads 
                                         ?>
                                        <?php endif; ?>

                                       

                                        <?php if ($detect->isMobile() && !$detect->isTablet()) : ?>
                                      </div>
                                    </div>
                                    <?php
                                         // For Bottom Mobile  
                                     ?>
                                  <?php endif; ?>
                                  </div>
                                </div>
                                    <?php if(isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true'   ) : ?> 
                                     <?php sticky_banner(); ?>
                                    <?php endif; ?>


                              </div>
                             
                          </div>
                      </div>
                          </div>
                      </div>
                    </div>
                    
               
                    
                    <!-- KeywordMatching Purpose -->
                    <?php click_event_purpose();   ?> 

                    <script>
                      jQuery(document).ready(function() {
                         
                         if( jQuery(window).innerWidth() <= 1024 ){
                           jQuery('#saved_id .save-ideas label').on('click', function(){
                               jQuery(this).prev().trigger('click');
                           });
                         }
                         
                         if( jQuery(window).innerWidth() <= 1024 ){
                            jQuery(".email-input:eq(0)").remove();
                            jQuery("#saved_id:eq(0)").remove();
                         }

                        jQuery(window).scroll(filtersFixScroll);
                        
                        <?php
                        $cookie = $_SESSION['invite_modal_value'];
                        $cookie_arr = [];
                        foreach ($cookie as $cookie_name => $cookie_id) {
                         array_push($cookie_arr, $cookie_name); 
                        } 
                        $json = json_encode($cookie_arr);
                        ?>
                        jQuery.removeCookie('savedideas');
                        var arr = <?php echo $json;  ?>;
                        var json_str = JSON.stringify(arr);
                        jQuery.cookie('savedideas', json_str, {
                          expires: 15
                        });
                        

                        var h = parseInt("1965");
                                  jQuery(window).scroll(function() {
                                    var scroll = jQuery(window).scrollTop()+201;
                                    if(scroll >= h){
                                      jQuery('.left-scroll').css('position','absolute');
                                      jQuery('.right-scroll').css('position','absolute');
                                      jQuery('.results-left-widget .textwidget.custom-html-widget').css('top','1732px');
                                      jQuery('.results-right-widget .textwidget.custom-html-widget').css('top','1732px');
                                    }else{
                                      jQuery('.left-scroll').css('position','fixed');
                                      jQuery('.right-scroll').css('position','fixed');
                                      jQuery('.results-left-widget .textwidget.custom-html-widget').css('top','0px');
                                      jQuery('.results-right-widget .textwidget.custom-html-widget').css('top','0px');
                                    }
                        });
                        jQuery('.cancel_btn').click(function() {
                          var resultarr = <?php echo json_encode($result_filter); ?>;
                          jQuery.each(resultarr, function(key, value) {

                            if (key == 'one_word' && value == 'on') {
                              jQuery('.oneword').prop('checked', true);
                            }
                            if (key == 'one_word' && value == '') {
                              jQuery('.oneword').prop('checked', false);
                            }


                            if (key == 'two_word' && value == 'on') {
                              jQuery('.twoword').prop('checked', true);
                            }
                            if (key == 'two_word' && value == '') {
                              jQuery('.twoword').prop('checked', false);
                            }

                            if (key == 'position' && value == 'on') {
                              jQuery('.can_position').prop('checked', true);
                            }
                            if (key == 'position' && value == '') {
                              jQuery('.can_position').prop('checked', false);
                            }

                            if (key == 'synonyms' && value == 'on') {
                              jQuery('.can_synonyms').prop('checked', true);
                            }
                            if (key == 'synonyms' && value == '') {
                              jQuery('.can_synonyms').prop('checked', false);
                            }

                            if (key == 'synonyms' && value == 'on') {
                              jQuery('.can_synonyms').prop('checked', true);
                            }
                            if (key == 'synonyms' && value == '') {
                              jQuery('.can_synonyms').prop('checked', false);
                            }


                            if (key == 'rhyming' && value == 'on') {
                              jQuery('.can_rhyming').prop('checked', true);
                              jQuery('.word-type').addClass('rhyming-on');
                            }

                            if (key == 'rhyming' && value == '') {
                              jQuery('.can_rhyming').prop('checked', false);
                              jQuery('.word-type').removeClass('rhyming-on');
                            }
                          });
                        });

                        jQuery('.int_cancel_btn').click(function() {
                          var int_filter = <?php echo json_encode($int_filter); ?>;
                          var int_grammer = <?php echo json_encode($int_grammer); ?>;
                          jQuery('.grammar0 , .grammar1 , .grammar2 , .grammar3 , .grammar4').prop('checked', false);
                         
                          jQuery.each( int_filter, function( key, value ) {

                             if(key == 'int_position' && value == 'both') { jQuery('.can_after,.can_before').prop('checked',true); }

                             if(key == 'int_position' && value == 'after'){ 
                              jQuery('.can_after').prop('checked',true); 
                              jQuery('.can_before').prop('checked',false); 
                             }

                             if(key == 'int_position' && value == 'before'){ 
                              jQuery('.can_before').prop('checked',true); 
                              jQuery('.can_after').prop('checked',false); 
                             }

                          });

                          jQuery.each(int_grammer, function(ke, val) {
                            jQuery("input[value='" + val + "']").prop('checked', true);
                          });
                        });
                   
                      });


                      jQuery(document).on('click','.social_share_link',function(e) {
                        e.preventDefault();
                        var url = '<?php echo 'https://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI']; ?>';
                        var surl = '';
                        if (jQuery(this).hasClass('social_facebook_link')) {
                          window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, '_blank');
                        } else if (jQuery(this).hasClass('social_twitter_link')) {
                          window.open('https://twitter.com/home?status=Hey,%20check%20out%20these%20amazing%20business%20name%20ideas%20' + url, '_blank');
                        } else if (jQuery(this).hasClass('social_linkedin_link')) {
                          window.open('https://www.linkedin.com/shareArticle?mini=true&title=Business%20Name%20Generator&url=' + url, '_blank');
                        } else {

                        }
                      });

                      /**
                       *
                       *Generator V2 code script starts
                       *
                       *
                       */


                      jQuery(window).resize(function() {

                        eleBgClass();

                        new_eng_filters();

                        jQuery('.dropdown-main .cstm-dropdown2').click(function() {
                          if (jQuery(window).width() > 767) {
                            if (jQuery('.busi-filter .savedideas-drop-data li').length == 0) {
                              jQuery('.empty-idea-msg').remove();
                              jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                            }
                            jQuery('.busi-filter .savedideas-drop-data').toggle();
                            jQuery(this).toggleClass('drop-opened');
                          } else {
                            if (jQuery('#myModalSaved .savedideas-drop-data li').length == 0) {
                              jQuery('.imgloadingshow').show();
                              jQuery('#myModalSaved .empty-idea-msg').remove();
                              jQuery('#myModalSaved .savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                              jQuery('.imgloadingshow').hide();
                            }
                            jQuery("#myModalSaved").modal('show');
                          }
                        });
                      });


                      // function handleOutboundLinkClicks(event, filter) {
                      //   __gaTracker('send', 'event', {
                      //     eventCategory: 'ResultsPage',
                      //     eventAction: 'IndustryFilters',
                      //     eventLabel: filter,
                      //     transport: 'beacon'
                      //   });
                      // }

                      // function handleOutboundAnchorLinkClicks(event, DomainName) {
                      //   __gaTracker('send', 'event', {
                      //     eventCategory: 'ResultsPage',
                      //     eventAction: 'Domainify Ad',
                      //     eventLabel: DomainName,
                      //     transport: 'beacon'
                      //   });
                      // }

                      jQuery(document).ready(function($) {
                        jQuery('#navbarTop').prepend( jQuery('.gen-search') );
                        jQuery('.gen-search').show();

                        eleBgClass();

                        new_eng_filters();

                        window.addEventListener("orientationchange", function() {
                          if (window.orientation == 0 || window.orientation == 90 || window.orientation == -90) {
                            eleBgClass();
                          }
                        }, false);

                        <?php if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE == 'en') : ?>
                          <?php
                          $page = get_url_var();
                          if ($page >= 2 || (isset($_GET['fltr']) && $_GET['fltr'] == 'result')) {
                            unset($_SESSION['filterd_columns']);
                            unset($_SESSION['keyMatching']);
                          }
                          if (isset($_SESSION['filterd_columns']) && !$page) :
                          ?>
                            // setTimeout(function() {
                            //   __gaTracker('send', 'event', 'ResultsPage', 'KeywordMatching', 'KeywordMatched', {
                            //     nonInteraction: true
                            //   });
                            // }, 1000);

                            setTimeout(function() {
                                jQuery('.keywordmatching').attr('data-label','KeywordMatched');
                                jQuery('.keywordmatching').click();
                            }, 2500); 

                          <?php endif; ?>

                          <?php if (isset($_SESSION['keyMatching']) && !$page) : ?>
                            // setTimeout(function() {
                            //   __gaTracker('send', 'event', 'ResultsPage', 'KeywordMatching', 'NoMatch', {
                            //     nonInteraction: true
                            //   });
                            // }, 1000);

                            setTimeout(function() {
                                jQuery('.keywordmatching').attr('data-label','NoMatch');
                                jQuery('.keywordmatching').click();
                            }, 2500); 

                          <?php endif; ?>

                        <?php endif; ?>

                        jQuery('.eng-ind-filter').find('.apply-btn').click(function(e) {
                          e.preventDefault();
                          var arr = [];
                          jQuery('.eng-ind-filter').find('.drop-down-data :checkbox').each(function() {
                            if (jQuery(this).is(":checked")) {
                              arr.push(jQuery(this).val());
                            }
                          });
                          var filter = arr.join(',');

                          //handleOutboundLinkClicks(jQuery(this), filter);

                          jQuery(this).parents('form').submit();

                        });

                        jQuery('.domain-buy .inner a').click(function(e) {
                          e.preventDefault();

                         // handleOutboundAnchorLinkClicks(jQuery(this), jQuery(this).data('domainname'));

                          window.open(jQuery(this).attr('href'), '_blank');

                        });

                        if (jQuery(window).width() > 767 && jQuery(window).width() < 992) {
                          jQuery('.fiter-drop.fltr-ipd button').click(function(e) {
                            e.preventDefault();
                            if (jQuery(this).hasClass('hidden-btn')) {
                              jQuery('.mobile-filter').removeClass('hide-me');
                              jQuery(this).removeClass('hidden-btn');
                              jQuery('.domain-filters').addClass('hide-me');
                              jQuery('.fiter-drop.tablet-new-filter button').addClass('hidden-btn');

                            } else {
                              jQuery(this).addClass('hidden-btn');
                              jQuery('.mobile-filter').addClass('hide-me');
                            }
                          });
                        }

                        if (jQuery(window).width() > 767 && jQuery(window).width() < 992) {
                          jQuery('.fiter-drop.tablet-new-filter button').click(function(e) {
                            e.preventDefault();

                            if (jQuery(this).hasClass('hidden-btn')) {
                              jQuery('.domain-filters').removeClass('hide-me');
                              jQuery(this).removeClass('hidden-btn');
                              jQuery('.mobile-filter').addClass('hide-me');
                              jQuery('.fiter-drop.fltr-ipd button').addClass('hidden-btn');

                              jQuery('.btn-flitr').click(function(e) {
                                e.preventDefault();
                                jQuery(this).parents('form').submit();
                              });
                            } else {
                              jQuery(this).addClass('hidden-btn');
                              jQuery('.domain-filters').addClass('hide-me');
                            }
                          });
                        }

                        jQuery('.selected_industry .icon_close').click(function() {
                          var form1 = jQuery(this).parents('form');
                          jQuery(this).parents('.applyed-fl').remove();
                          setTimeout(function() {
                            form1.submit();
                          }, 500);
                        });

                          jQuery('.eng-industry-filter button').click(function(e) {
                            e.preventDefault();
                            if (jQuery(this).hasClass('hidden-btn')) {
                            
                              jQuery('.eng-ind-filter').removeClass('hide-me');
                              jQuery(this).removeClass('hidden-btn');
                              jQuery('.domain-filters').addClass('hide-me');
                              jQuery('.eng-other-filter button').addClass('hidden-btn');
                              jQuery(this).find('.elegant-icon').addClass('arrow_carrot-up').removeClass('arrow_carrot-down'); 

                              jQuery('.applyed-fl').each(function() {
                                var checked = jQuery(this).text();
                                jQuery(this).find('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                              });
                            } else {
                             
                              jQuery(this).addClass('hidden-btn');
                              jQuery('.eng-ind-filter').addClass('hide-me');
                              jQuery(this).find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up'); 
                             
                              jQuery('.applyed-fl').each(function() {
                                var checked = jQuery(this).text();
                                jQuery('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                              });
                            }
                          });

                          jQuery('.eng-other-filter button').click(function(e) {
                            e.preventDefault();
                            
                            if (jQuery(this).hasClass('hidden-btn')) {
                              jQuery('.domain-filters').removeClass('hide-me');
                              jQuery(this).removeClass('hidden-btn');
                              jQuery('.eng-ind-filter').addClass('hide-me');
                              jQuery('.eng-industry-filter button').addClass('hidden-btn');
                              jQuery(this).find('.elegant-icon').addClass('arrow_carrot-up').removeClass('arrow_carrot-down'); 

                              jQuery('.applyed-fl').each(function() {
                                var checked = jQuery(this).text();
                                jQuery('.eng-ind-filter .live-search-list li input[value="' + checked + '"]').prop('checked', true);
                              });
                            } else {
                              jQuery(this).addClass('hidden-btn');
                              jQuery('.domain-filters').addClass('hide-me');
                              jQuery(this).find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up'); 
                              jQuery('.applyed-fl').each(function() {
                                var checked = jQuery(this).text();
                                jQuery('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                              });
                            }

                          });

                        var grammar = [<?php if (isset($_GET['grammar'])) : foreach ($_GET['grammar'] as $key => $value) {
                                            if (end($_GET['grammar']) != $value) {
                                              echo '"' . trim($value) . '",';
                                            } else {
                                              echo '"' . trim($value) . '"';
                                            }
                                          }
                                        endif; ?>];
                        if (grammar.length > 0) {
                          for (var i = 0; i < grammar.length; i++) {
                            jQuery('.grammar-drop-down-data .live-search-list, #myModalGrammer .live-search-list').find('input').each(function() {
                              if (jQuery(this).val() == grammar[i]) {
                                jQuery(this).attr('checked', 'checked');
                              }

                            });
                          }
                        }


                        jQuery('.domain-filters .ins-keyword input[type=checkbox]').click(function(e) {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.pos').val('before');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('Before', 'thegem-child'); ?>');
                          } else {
                            jQuery('.pos').val('after');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('After', 'thegem-child'); ?>');
                          }
                        });

                        jQuery('.busi-filter .mobile-filter input[type=checkbox]').click(function(e) {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.pos').val('before');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('Before', 'thegem-child'); ?>');
                          } else {
                            jQuery('.pos').val('after');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('After', 'thegem-child'); ?>');
                          }
                        });
                       
                        setTimeout(function() {

                          if (jQuery.cookie('savedideas')) {
                            jQuery('.savedideas-drop-data ul').html('');
                            var s = JSON.parse(jQuery.cookie('savedideas'));
                            var arr = jQuery.unique(s.sort()).sort();
                            if (arr.length > 0) {
                              jQuery.each(arr, function(key, value) {
                                var dt = value.replace(' ', '+');
                                dt = jQuery.trim(dt);
                                
                                savedideas_list(key,value,dt);
                            
                              });

                              jQuery('.industry-sub-right .send-or-email').attr('data-target', '#myModalShare');
                              jQuery('.cstm-dropdown2 .idea-count').text(Object.keys(s).length).show().prev('span').removeClass('icon_star_alt').addClass('icon_star');
                              jQuery('.industry-sub-right').addClass('added-saved-ideas');

                              for (var i = 0; i < arr.length; i++) {
                                jQuery('.list-star').find('[data-string="' + arr[i] + '"]').next().addClass('clicked-link');
                                jQuery('.list-star').find('[data-string="' + arr[i] + '"]').next().find('span').removeClass('icon_star_alt').addClass('icon_star').addClass('click');
                              }
                            } else {
                              jQuery('.industry-sub-right .send-or-email').removeAttr('data-target');
                            }
                          } else {
                            jQuery('.industry-sub-right .send-or-email').removeAttr('data-target');
                          }
                        
                        }, 3000);
                     
                        jQuery('.sliderRange').asRange({
                          min: 2,
                          max: 15,
                          value: 15,
                          range: false,
                          limit: false,
                          keyboard: true,
                          format: function(value) {
                            return (value == 15) ? value + "+" : value;
                          }
                        });



                        jQuery('.dropdown-main .cstm-dropdown2').click(function() {
                          if (jQuery(window).width() > 1024) {
                            if (jQuery('.busi-filter .savedideas-drop-data li').length == 0) {
                              jQuery('.empty-idea-msg').remove();
                              jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                            }
                            jQuery('.busi-filter .savedideas-drop-data').toggle();
                            
                            if(jQuery('.cstm-dropdown2 .count-down').find('.elegant-icon').hasClass('arrow_carrot-down') ){
                              jQuery('.cstm-dropdown2 .count-down').find('.elegant-icon').addClass('arrow_carrot-up').removeClass('arrow_carrot-down');
                            }else{
                              jQuery('.cstm-dropdown2 .count-down').find('.elegant-icon').addClass('arrow_carrot-down').removeClass('arrow_carrot-up');
                            } 
                            jQuery(this).toggleClass('drop-opened');
                          } else {
                            if (jQuery('#myModalSaved .savedideas-drop-data li').length == 0) {
                              jQuery('#myModalSaved .empty-idea-msg').remove();
                              jQuery('#myModalSaved .savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                            }
                            jQuery("#myModalSaved").modal('show');
                          }
                        });

                        jQuery('.sliderRange').on('asRange::change', function(e) {
                          jQuery('.character').val();
                        });

                        // jQuery('.busi-filter .sliderRange.deskslide').next().find(".asRange-pointer").on('asRange::moveEnd', function (e) {
                        //   jQuery(this).parents('form').submit();
                        // });

                        jQuery('.send-or-email').click(function() {
                          jQuery('body #myModalShare').find('.sendids').html('');
                          jQuery('.busi-filter .savedideas-drop-data li').each(function() {
                            jQuery('#myModalShare #saved_id .sendids').append(jQuery(this).find('.ids'));
                          });
                        });

                        // jQuery('.cstm-dropdown, .cstm-dropdown2').focusout(function(){
                        //   jQuery('.drop-down-data, .savedideas-drop-data').hide();
                        // });

                        jQuery('.dropdown-main .share-saved').click(function() {
                          var idea = jQuery(this).prev('input');
                          jQuery('.append-share-idea').html('').append(idea);
                        });

                        // jQuery('.live-search-list li').each(function() {
                        //   jQuery(this).attr('data-search-term', jQuery(this).find(':checkbox').val().toLowerCase());
                        // });

                    
                      });

                      jQuery('.drop-down-data .btn.clear').click(function() {
                          jQuery('.drop-down-data :checkbox').removeAttr('checked');
                          jQuery("input[type='hidden'][name='all-indus']").remove();
                      });

                  

                      jQuery('.mobile-filter .btn.clear').click(function() {
                        jQuery('.mobile-filter .live-search-list :checkbox').removeAttr('checked');
                      });

                      jQuery('#main-filter-modal').on("shown.bs.modal", function() {
                        new_eng_filters();
                      });

                
                        jQuery('.eng-ind-filter .btn.cancel').click(function() {

                          jQuery('.eng-industry-filter button').addClass('hidden-btn');
                          jQuery('.eng-ind-filter').addClass('hide-me');

                          jQuery('.eng-ind-filter').find('.live-search-list li input').removeAttr('checked');

                          jQuery('.applyed-fl').each(function() {
                            var checked = jQuery(this).text();
                            jQuery('.eng-ind-filter').find('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                          })

                          jQuery('.eng-ind-filter').find('.search_bar').find('input').val("");
                          jQuery('.eng-ind-filter').find('.live-search-list li').show();
                        });

                        jQuery('.domain-filters .btn.cancel').click(function() {
                          jQuery('.eng-other-filter button').addClass('hidden-btn');
                          jQuery('.domain-filters').addClass('hide-me');

                          jQuery('.applyed-fl').each(function() {
                            var checked = jQuery(this).text();
                            jQuery('.domain-filters').find('.live-search-list li input[value="' + checked + '"]').prop('checked', true);
                          })

                          jQuery('.domain-filters').find('.search_bar').find('input').val("");
                          jQuery('.domain-filters').find('.live-search-list li').show();
                        });

                        jQuery('.mobile-filter .btn.cancel').click(function() {
                          jQuery('.mobile-filter').toggle();
                        });

                    
                      jQuery('#myModal').on("shown.bs.modal", function() {
                        jQuery('#myModal .clear').click(function() {
                          jQuery('.domains-list :checkbox').removeAttr('checked');
                        });
                      });

                      jQuery('#myModalGrammer').on("shown.bs.modal", function() {
                        jQuery('#myModalGrammer .clear').click(function() {
                          jQuery('#myModalGrammer .live-search-list :checkbox').removeAttr('checked');
                        });
                      });

                      jQuery('#myModalDomains').on("shown.bs.modal", function() {
                        jQuery("body #myModalDomains").find('.imgloadingshow').show();

                        setTimeout(function() {
                          jQuery("#myModalDomains").find('.imgloadingshow').hide();
                        }, 1000);
                      });

                      
                        var url = '<?php echo 'https://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI']; ?>';
                        var surl = encodeURIComponent(url);
                        jQuery(document).on('click','.social_whatsapp_link',function(e) {
                          e.preventDefault();
                          if (window.innerWidth > 767) {
                            jQuery(this).parents('li').find('#whatsapp').attr('href', 'https://web.whatsapp.com/send?text=' + surl).text('https://web.whatsapp.com/send?text=' + surl);
                            jQuery(this).parents('li').find('#whatsapp')[0].click();

                          } else {
                            jQuery(this).parents('li').find('#whatsapp').attr('href', 'https://api.whatsapp.com/send?text=' + surl).text('whatsapp://send?text=' + surl);

                            jQuery(this).parents('li').find('#whatsapp')[0].click();
                          }

                          // setTimeout(function(){
                          //   jQuery('#whatsapp').click();
                          // },800);
                        });

                      

                      jQuery('.list-star li:nth-child(4n)').addClass('openafter');

                    
                      /*
                      On click result check domain 
                      */
                      jQuery.xhrPool = []; // array of uncompleted requests
                      jQuery.xhrPool.abortAll = function() { // our abort function
                        jQuery(this).each(function(idx, jqXHR) {
                          jqXHR.abort();
                        });
                        jQuery.xhrPool.length = 0
                      };
                      jQuery(document).on('click','.ntbs',function(e){


                      if( jQuery(e.target).closest('.fa-plus').length )
                      {   
                        var event_cat = jQuery(this).data('category'); 
                        var event_act = jQuery(this).data('action'); 
                        var event_lab = jQuery(this).data('label'); 
                        
                        jQuery('.add-event').attr( 'data-category',event_cat);
                        jQuery('.add-event').attr( 'data-action'  ,event_act);
                        jQuery('.add-event').attr( 'data-label'   ,event_lab);  
                        jQuery('.add-event').click(); 
                          
                      }
                        
                        jQuery('.list-star #results').remove();
                        var nameidea = jQuery(this).data('string');
                        nameidea = nameidea.replace(/\s/g, '');
                        nameidea = nameidea.toLowerCase();

                        jQuery.xhrPool.abortAll();
                        if (!jQuery(this).hasClass('checking')) {
                          jQuery('.stardomain.checked .ntbs.click-event.checking').trigger('click');
                        
                          jQuery(this).addClass('checking');
                          jQuery('.list-star li').removeClass('checked');
                          jQuery(this).parents('li').addClass('checked');

                          jQuery(this).find('.fa').addClass('fa-minus');  
                          jQuery(this).find('.fa').removeClass('fa-plus'); 

                          var domainslist = '';
                          var index = jQuery(this).parents('li').index();
                          var el = jQuery(this);
                          var cls = '';
                          switch (index) {
                            case 0:
                              cls = 'domain-click-0';
                              jQuery('.pad_set').removeClass('domain-click-1');
                              jQuery('.pad_set').removeClass('domain-click-2');
                              jQuery('.pad_set').removeClass('domain-click-3');
                              el.parents('.pad_set').addClass(cls);
                              break;
                            case 1:
                              cls = 'domain-click-1';
                              jQuery('.pad_set').removeClass('domain-click-0');
                              jQuery('.pad_set').removeClass('domain-click-2');
                              jQuery('.pad_set').removeClass('domain-click-3');
                              el.parents('.pad_set').addClass(cls);
                              break;
                            case 2:
                              cls = 'domain-click-2';
                              jQuery('.pad_set').removeClass('domain-click-0');
                              jQuery('.pad_set').removeClass('domain-click-1');
                              jQuery('.pad_set').removeClass('domain-click-3');
                              el.parents('.pad_set').addClass(cls);
                              break;

                            case 3:
                              cls = 'domain-click-3';
                              jQuery('.pad_set').removeClass('domain-click-0');
                              jQuery('.pad_set').removeClass('domain-click-1');
                              jQuery('.pad_set').removeClass('domain-click-2');
                              el.parents('.pad_set').addClass(cls);
                              break;
                          }
                          
                             
                        jQuery(window).resize(function() {
                        
                          if (window.innerWidth > 1024) {
                            var ind = jQuery(this).parents('li').index();
                            var diff = ind % 3;
                            var nextOpenIndex = ind - diff + 2;
                            var toOpen = nextOpenIndex;

                            if (nextOpenIndex >= jQuery(this).parents('.list-star').find('li').length) {
                              toOpen = jQuery(this).parents('.list-star').find('li').length - 1;
                            }

                            dropdown_section_nameideas(jQuery(this).parents('.list-star').find('li:eq(' + toOpen + ')'),nameidea);
                          }


                          if (window.innerWidth == 1024)  {
                            var ind = jQuery(this).parents('li').index();
                            var diff = ind % 3;
                            var nextOpenIndex = ind - diff + 2;
                            var toOpen = nextOpenIndex;

                            if (nextOpenIndex >= jQuery(this).parents('.list-star').find('li').length) {
                              toOpen = jQuery(this).parents('.list-star').find('li').length - 1;
                            }

                            dropdown_section_nameideas(jQuery(this).parents('.list-star').find('li:eq(' + toOpen + ')'),nameidea);
                          }

                          if (window.innerWidth > 767 && window.innerWidth < 992) {
                            var ind = jQuery(this).parents('li').index();
                            var diff = ind % 2;
                            var nextOpenIndex = ind - diff + 1;
                            var toOpen = nextOpenIndex;

                            if (nextOpenIndex >= jQuery(this).parents('.list-star').find('li').length) {
                              toOpen = jQuery(this).parents('.list-star').find('li').length - 1;
                            }
                             

                            dropdown_section_nameideas(jQuery(this).parents('.list-star').find('li:eq(' + toOpen + ')'),nameidea); 
                          }

                          if (window.innerWidth <= 767 && window.innerWidth >= 480) {
                            var ind = jQuery(this).parents('li').index();
                            var diff = ind % 2
                            var nextOpenIndex = ind - diff + 1;
                            var toOpen = nextOpenIndex;

                            if (nextOpenIndex >= jQuery(this).parents('.list-star').find('li').length) {
                              toOpen = jQuery(this).parents('.list-star').find('li').length - 1;
                            }
                            
                            dropdown_section_nameideas(jQuery(this).parents('.list-star').find('li:eq(' + toOpen + ')'),nameidea); 

                          }

                          if (window.innerWidth < 480) {
                            
                            dropdown_section_nameideas(jQuery(this).parents('li'),nameidea);

                          }
                          
                        
                        });

                          if (window.innerWidth > 1024) {
                            var ind = jQuery(this).parents('li').index();
                            var diff = ind % 3;
                            var nextOpenIndex = ind - diff + 2;
                            var toOpen = nextOpenIndex;

                            if (nextOpenIndex >= jQuery(this).parents('.list-star').find('li').length) {
                              toOpen = jQuery(this).parents('.list-star').find('li').length - 1;
                            }

                            dropdown_section_nameideas(jQuery(this).parents('.list-star').find('li:eq(' + toOpen + ')'),nameidea);
                          } 

                          if (window.innerWidth == 1024 ) {
                            var ind = jQuery(this).parents('li').index();
                            var diff = ind % 2;
                            var nextOpenIndex = ind - diff + 1;
                            var toOpen = nextOpenIndex;

                            if (nextOpenIndex >= jQuery(this).parents('.list-star').find('li').length) {
                              toOpen = jQuery(this).parents('.list-star').find('li').length - 1;
                            }
                            dropdown_section_nameideas(jQuery(this).parents('.list-star').find('li:eq(' + toOpen + ')'),nameidea);
                          }

                          if (window.innerWidth > 767 && window.innerWidth < 992) {
                            var ind = jQuery(this).parents('li').index();
                            var diff = ind % 2;
                            var nextOpenIndex = ind - diff + 1;
                            var toOpen = nextOpenIndex;

                            if (nextOpenIndex >= jQuery(this).parents('.list-star').find('li').length) {
                              toOpen = jQuery(this).parents('.list-star').find('li').length - 1;
                            }

                            dropdown_section_nameideas(jQuery(this).parents('.list-star').find('li:eq(' + toOpen + ')'),nameidea);
                          }

                          if (window.innerWidth <= 767 && window.innerWidth >= 480) {
                            var ind = jQuery(this).parents('li').index();
                            var diff = ind % 2
                            var nextOpenIndex = ind - diff + 1;
                            var toOpen = nextOpenIndex;

                            if (nextOpenIndex >= jQuery(this).parents('.list-star').find('li').length) {
                              toOpen = jQuery(this).parents('.list-star').find('li').length - 1;
                            }
                            
                            dropdown_section_nameideas( jQuery(this).parents('.list-star').find('li:eq(' + toOpen + ')'),nameidea); 
                          }

                          if (window.innerWidth < 480) {
                            
                            dropdown_section_nameideas(jQuery(this).parents('li'),nameidea); 
                         
                          }

                          jQuery('.list-star #results .imgloadingshow').show();


                          <?php
                           // For Dropdown banner
                           $advert = "";
                           $advert =  dropdown_banner();
                          ?>
                          jQuery(".list-star").find(".innerResult").prepend('<?php echo $advert; ?>');
                          //}


                          var item_id = 'https://www.bluehost.com/track/businessnamegenerator/bnwcheck';
                          var domain = jQuery(this).data('string');
                          domain = domain.replace(/\s/g, '');
                          domain = domain.toLowerCase();
                          
                          var page_id = "<?= !empty($_GET['home_id']) ? $_GET['home_id'] : '' ?>";
                          var tld = '';

                          var data = {
                            'action': 'dc_display_dropdown_domain',
                            'domain': domain,
                            'location':"resultsdropdown",
                            'item_id': item_id,
                            'tld': tld,
                            'template': 'nameideas',
                            'status': 'WHOIS',
                            'page_id' : page_id,
                            'device' : Device_Detect(),
                            'lang': '<?php echo ICL_LANGUAGE_CODE; ?>',
                            'security': domain_checker_ajax.domain_checker_nonce
                          };

                          jQuery.ajaxSetup({
                            beforeSend: function(jqXHR) { // before jQuery send the request we will push it to our array
                              jQuery.xhrPool.push(jqXHR);
                            },
                            complete: function(jqXHR) { // when some of the requests completed it will splice from the array
                              var index = jQuery.xhrPool.indexOf(jqXHR);
                              if (index > -1) {
                                jQuery.xhrPool.splice(index, 1);
                              }
                            }
                          });

                          jQuery.ajax({
                            type: 'post',
                            url: domain_checker_ajax.ajaxurl,
                            data: data,
                            success: function(response) {

                              response = jQuery.parseJSON(response);

                              jQuery('.list-star #results .imgloadingshow').hide();

                              if (response) {
                                var d = 100;

                                jQuery.each(response, function(i, obj) {

                                  setTimeout(function() {

                                    jQuery(".list-star").find(".innerResult .colm1").append(unescape(response[i]));

                                  }, d);

                                  d += 300;
                                });
                              }
                            }
                          });

                          if(!jQuery(this).parents('.inner').find('.clickable-arrow').hasClass('clicked-link')){
                            jQuery(this).parents('.inner').find('.clickable-arrow').trigger('click');
                          }
                        } else {
                          
                          jQuery(this).removeClass('checking');
                          jQuery(this).parents('li').removeClass('checked');

                          jQuery(this).find('.fa').addClass('fa-plus');  
                          jQuery(this).find('.fa').removeClass('fa-minus');
                        }
                      });

                      /*
                       * Generator v2 Save  ideas on star
                       * click
                       * 
                       **/

                      var counter = <?php echo (isset($_SESSION['invite_modal_value'])) ? count($_SESSION['invite_modal_value']) : '0'; ?>;
                      
                      
                      // jQuery(document).on('click','.tracking_event',function(e){
                                             
                      //   var data-label = jQuery(this).attr('data-label');
                      //  __gaTracker('send', 'event', 'ResultsPage', 'DomainAvailabilityCheck', ''+data-label+'');


                      // });


                      function setSelectedTestPlan(name, number, obj) {

                        //__gaTracker('send', 'event', 'ResultsPage', 'SavedIdeas', 'SaveStar');
                        

                        if (counter == 0) {
                          jQuery('.savedideas-drop-data ul').removeClass('ideas-added');
                        }

                        if (jQuery(obj).parents('.stardomain').find("span.elegant-icon").hasClass("click")) {
                           
                          counter = counter - 1;
                          var count = jQuery('.dropdown-main .idea-count').length;
                          (counter == 0) ? jQuery('.dropdown-main .idea-count').hide(): '';
                          var liden = 'li#' + number;
                          var neiurl = '<?php bloginfo('template_url'); ?>';
                          //jQuery("#imgloadingshow").show();
                          jQuery(obj).parents('.stardomain').find("span.elegant-icon").removeClass("icon_star").addClass("icon_star_alt").removeClass('click');

                          jQuery(obj).parents('.stardomain').find("a.result_" + number).removeClass("clicked-link");
                         
                          jQuery.ajax({
                            type: "POST",
                            url: '<?php echo admin_url('admin-ajax.php'); ?>',
                            data: {
                              action: 'ajax_con',
                              id: number,
                              name: name,
                              active: '0'
                            },
                            cache: false,
                            success: function(result) {
                              jQuery(obj).parents('.stardomain').find(".result_" + number).removeAttr("disabled", false);
                              //jQuery("#imgloadingshow").hide();
                              jQuery(obj).parents('.stardomain').find(".result_" + number).removeClass("click");
                              jQuery("#" + number).remove();
                              var arr = [];

                              //jQuery('#savedideas').find(liden).remove();
                              jQuery('.savedideas-drop-data ul').html('');
                              jQuery.each(jQuery.parseJSON(result), function(key, value) {
                             
                                arr.push(key);
                              });

                              if (jQuery.cookie('savedideas')) {
                                var old_json_str = jQuery.cookie('savedideas');
                                var newarr = JSON.parse(old_json_str);

                                jQuery('.savedideas-drop-data ul').html('');
                                savearr = jQuery.unique(newarr.sort()).sort();

                                var filtered = savearr.filter(function(value, index, arr) {

                                  return value !== name;

                                }, name);

                                if (filtered.length > 0) {
                                  jQuery.removeCookie('savedideas');
                                  var json_str = JSON.stringify(filtered);
                                  jQuery.cookie('savedideas', json_str, {
                                    expires: 15
                                  });
                                  jQuery('.empty-idea-msg').remove();
                                  jQuery('.industry-sub-right').addClass('added-saved-ideas');
                                  jQuery.each(filtered, function(key, value) {
                                    var dt = value.replace(' ', '+');
                                    dt = jQuery.trim(dt);

                                     savedideas_list(key,value,dt);
                                  });

                                  jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(filtered.length);
                                  jQuery('.industry-sub-right .send-or-email').attr('data-target', '#myModalShare');
                                } else {
                                  jQuery.removeCookie('savedideas');
                                  jQuery('.industry-sub-right .send-or-email').removeAttr('data-target');
                                  // jQuery('.cstm-dropdown2 .idea-count').hide();
                                  // jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                  // jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?php // echo __('click the <span class="icon_star_alt elegant-icon"></span> to save ideas', 'thegem-child'); ?>');
                                  // jQuery('.cstm-dropdown2 span.eicon_star').removeClass('icon_star').addClass('icon_star_alt');
                                }
                              } else {
                                var json_str = JSON.stringify(arr);
                                jQuery.cookie('savedideas', json_str, {
                                  expires: 15
                                });

                                jQuery('.savedideas-drop-data ul').html('');
                                savearr = jQuery.unique(arr.sort()).sort();

                                jQuery.each(arr, function(key, value) {
                                  var dt = value.replace(' ', '+');
                                  dt = jQuery.trim(dt);

                                   savedideas_list(key,value,dt);
                                });
                                jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(savearr.length);
                              }

                              if (jQuery('.savedideas-drop-data li').length == 0) {
                                jQuery('.cstm-dropdown2 .idea-count').hide();
                                jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');

                                jQuery('.cstm-dropdown2 span.icon_star').removeClass('icon_star').addClass('icon_star_alt');
                              } else {
                                jQuery('.empty-idea-msg').remove();
                                jQuery('.industry-sub-right').addClass('added-saved-ideas');
                                //jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(Object.keys(jQuery.parseJSON(result)).length);
                              }

                              jQuery('.savedideas-drop-data .icon_close').click(function() {
                                var value = jQuery(this).data('remove-item');

                                if (arr.length == 0) {
                                  //jQuery('.savedideas-drop-data').hide();
                                  jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                                }
                              });
                            }
                          });

                        } else {
                          if (counter < 100) {
                            var neiurl = '<?php bloginfo('template_url'); ?>';
                            jQuery(obj).parents('.stardomain').find(".elegant-icon.result_" + number).attr('disabled', 'disabled').removeClass("icon_star_alt").addClass("icon_star").addClass("click");

                            jQuery(obj).parents('.stardomain').find("a.result_" + number).addClass("clicked-link");
                           
                            //jQuery("#imgloadingshow").show();
                            jQuery.ajax({
                              type: "POST",
                              url: '<?php echo admin_url('admin-ajax.php'); ?>',
                              data: {
                                action: 'ajax_con',
                                id: number,
                                name: name,
                                active: '1'
                              },
                              cache: false,
                              success: function(result) {
                                counter += 1;
                                //jQuery("#imgloadingshow").hide();
                                jQuery('.savedideas-drop-data ul').html('');
                                var arr = [];
                                jQuery.each(jQuery.parseJSON(result), function(key, value) {
                                  arr.push(key);
                                });

                                  var json_str = JSON.stringify(arr);
                                  jQuery.cookie('savedideas', json_str, {
                                    expires: 15
                                  });

                                  jQuery('.savedideas-drop-data ul').html('');
                                  savearr = jQuery.unique(arr.sort()).sort();

                                  jQuery.each(arr, function(key, value) {
                                    var dt = value.replace(' ', '+');
                                    dt = jQuery.trim(dt);

                                     savedideas_list(key,value,dt);
                                  });
                                  jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(savearr.length);
                                  jQuery('.industry-sub-right .send-or-email').attr('data-target', '#myModalShare');

                              


                                if (jQuery('.savedideas-drop-data li').length == 0) {
                                  //jQuery('.cstm-dropdown2 .idea-count').hide();
                                  jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                  jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                                } else {
                                  jQuery('.empty-idea-msg').remove();
                                  jQuery('.industry-sub-right').addClass('added-saved-ideas');
                                  // jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(Object.keys(jQuery.parseJSON(result)).length);
                                  jQuery('.cstm-dropdown2 span.icon_star_alt').removeClass('icon_star_alt').addClass('icon_star');
                                }

                                jQuery('.savedideas-drop-data .icon_close').click(function() {
                                  var value = jQuery(this).data('remove-item');

                                  if (Object.keys(jQuery.parseJSON(result)).length == 0) {
                                    //jQuery('.savedideas-drop-data').hide();
                                    jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                                  }
                                });
                                // jQuery('#savedideas').append("<li id='"+number+"'><span class='"+name+" savespan elegant-icon icon_close' onClick=newremove('"+number+"')></span><input type='checkbox' class='ids' name='list[]' hidden='' value='"+name+"' checked='checked'>"+name+"</li>");

                                // if(window.innerWidth<768){
                                //     if(jQuery('#savedideas li').length==1){
                                //       alert('Find all your saved ideas at the bottom of the page');   
                                //     }
                                // }
                                
                              }
                            });
                          }
                        }
                      }

                      /*
                       * Generator v2 removeSavedResult 
                       * 
                       **/

                     function removeSavedResult(obj)
                      {
                        //jQuery(obj).parents('li').remove();
                        var neiurl = '<?php bloginfo('template_url'); ?>';
                        if (jQuery('.savedideas-drop-data li').length == 0) {
                          jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                          jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                        }

                        var value = jQuery(obj).parents('li').data('kname');
                       

                        if (value) {
                          var newvalue = value.replace('+', ' ');
                        }
                        jQuery.ajax({
                          type: "POST",
                          url: '<?php echo admin_url('admin-ajax.php'); ?>',
                          data: {
                            action: 'ajax_con',
                            id: "",
                            name: newvalue,
                            active: '0'
                          },
                          cache: false,
                          success: function(result) {
                            if (result.length > 0) {
                              jQuery("[data-string='" + newvalue + "']").parents('.stardomain').find('.click').removeClass("click");
                              jQuery("[data-string='" + newvalue + "']").parents('.stardomain').find('span.elegant-icon').removeClass('icon_star').addClass('icon_star_alt');
                              
                              jQuery("[data-string='" + newvalue + "']").parents('.stardomain').find(".clickable-arrow").removeClass("clicked-link");
                              
                              jQuery('.savedideas-drop-data ul').html('');




                              if (jQuery.cookie('savedideas')) {
                                var old_json_str = jQuery.cookie('savedideas');
                                var newarr = JSON.parse(old_json_str);

                                jQuery('.savedideas-drop-data ul').html('');
                                savearr = jQuery.unique(newarr.sort()).sort();

                                var filtered = savearr.filter(function(value, index, arr) {
                                  return value !== newvalue;
                                }, newvalue);

                                if (filtered.length > 0) {
                                  jQuery.removeCookie('savedideas');
                                  var json_str = JSON.stringify(filtered);
                                  jQuery.cookie('savedideas', json_str, {
                                    expires: 15
                                  });

                                  jQuery('.empty-idea-msg').remove();
                                  jQuery('.industry-sub-right').addClass('added-saved-ideas');
                                  jQuery.each(filtered, function(key, value) {
                                    var dt = value.replace(' ', '+');
                                    dt = jQuery.trim(dt);

                                     savedideas_list(key,value,dt);
                                  });

                                  jQuery('.cstm-dropdown2 .idea-count').removeAttr('style').text(filtered.length);
                                  jQuery('.industry-sub-right .send-or-email').attr('data-target', '#myModalShare');

                                } else {

                                  jQuery.removeCookie('savedideas');
                                  jQuery('.industry-sub-right .send-or-email').removeAttr('data-target');

                                  jQuery('.cstm-dropdown2 .idea-count').hide();
                                  jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                  jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                                  
                                }
                              } else {
                                jQuery('.cstm-dropdown2 .idea-count').hide();
                                jQuery('.industry-sub-right').removeClass('added-saved-ideas');
                                jQuery('.savedideas-drop-data ul').parent('div').append('<p class="empty-idea-msg"><?= ''.$translations->saveideaserrortext.' <span class="icon_star_alt elegant-icon"></span> '.$translations->saveideaserrorsubtext.'' ?></p>');
                              }

                            }
                          }
                        });

                      }

                      jQuery(document).on('submit, click','.sharemail',function(e){
                        e.preventDefault();

                        
                        var email = jQuery(".email-input").val();
                     
                        if (jQuery('#saved_id .savedideas-drop-data ul li').length < 1 ) {
                          jQuery(".responsebar").html('').append('<div class="alert alert-danger">' +
                            '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
                            "<p><?php echo addslashes($translations->emailerror1 ); ?></p>" +
                            '</div>');
                          return false;
                        
                        } else if (email == '') {
                          
                          <?php $share_id_error = str_replace("\\",'',$translations->emailerror2); ?>
                         
                          jQuery(".responsebar").html('').append('<div class="alert alert-danger">' +
                            '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
                            '<p><?php echo addslashes($share_id_error); ?></p> ' +
                            '</div>');
                          return false;
                        } else if (!isValidEmailAddress(jQuery(".email-input").val())) {
                          jQuery(".responsebar").html('').append('<div class="alert alert-danger">' +
                            '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
                            "<p><?php echo addslashes($translations->emailerror3 ); ?></p>" +
                            '</div>');
                          return false;
                        } else {
                          var hurl = '<?php echo get_stylesheet_directory_uri(); ?>';
                          jQuery('#idearmailbtn').attr("disabled", true);
                          jQuery("#mailsendgif").show();
                          var copy = "<?php echo WixAffiliateLinks()['url']; ?>";
                          var lang_code = "<?php echo ICL_LANGUAGE_CODE; ?>";
                          var country_code = "<?php echo WixAffiliateLinks()['country_code']; ?>";
                          jQuery('#saved_id').append('<input type="hidden" name="aff_url" value="'+copy+'">');
                          jQuery('#saved_id').append('<input type="hidden" name="lang_code" value="'+lang_code+'">');
                          jQuery('#saved_id').append('<input type="hidden" name="country_code" value="'+country_code+'">');
                         
                          var list = [];
                          jQuery('#saved_id  input[name*="list[]"]').each(function() { 
                            list.push(jQuery(this).val());
                          }); 

                          jQuery.ajax({
                            type: "POST",
                            url : '<?php echo admin_url('admin-ajax.php'); ?>',
                            data: {action: 'send_saved_ideas',
                                  list: list,
                                  email:jQuery('#saved_id .email-input').val(),
                            },
                            success: function(response) {
                                jQuery('#idearmailbtn1').removeAttr("disabled", false);
                                jQuery("#mailsendgif").hide();
                                
                                if(response == "success")
                                {
                                 
                                 
                                 jQuery(".responsebar").html('').append('<div class="alert alert-success">' +
                                   '<a href="#" class="close" data-bs-dismiss="alert" aria-label="close">&times;</a>' +
                                   "<p><?php echo isset($translations->sucessmessage) ? $translations->sucessmessage : 'Your ideas have been sent to your inbox...'; ?></p>" +
                                   '</div>');

                                }else{
                                 
                                   jQuery(".responsebar").html(response).show();
                                }
                            }
                          });
                        }
                      });
                      jQuery('#semail').keypress(function(e) {
                        if (e.which == 13) { //Enter key pressed
                          e.preventDefault();
                          e.stopPropagation();
                          jQuery('#idearmailbtn').click(); //Trigger search button click event
                        }
                      });

                      function isValidEmailAddress(emailAddress) {
                        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
                        return pattern.test(emailAddress);
                      }

                      jQuery('.domain-filters .btn-flitr').click(function(e) {

                        if (!jQuery('.oneword, .twoword, .rhyming input[type=checkbox], .synonyms input[type=checkbox]').is(':checked')) {

                          jQuery('.oneword, .twoword').prop("checked", true);
                          jQuery('.one-word, .two-word').val('on');
                        }
                        jQuery('this').parents('form').submit();
                      });

                      function new_eng_filters() {
                        jQuery('.oneword').click(function() {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.oneword').prop("checked", false);
                            jQuery('.one-word').val('off');
                          } else {
                            jQuery('.one-word').val('on');
                          }
                        });

                        jQuery('.twoword').click(function() {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.twoword').prop("checked", false);
                            jQuery('.two-word').val('off')
                          } else {
                            jQuery('.two-word').val('on');
                          }

                        });

                        jQuery('#main-filter-modal').find('.ins-keyword input[type=checkbox]').click(function(e) {
                          if (!jQuery(this).is(':checked')) {
                            jQuery('.pos').val('before');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('Before', 'thegem-child'); ?>');
                          } else {
                            jQuery('.pos').val('after');
                            jQuery(this).parents('.switch-toggle').find('p').text('<?php echo __('After', 'thegem-child'); ?>');
                          }
                        });

                        jQuery('.ins-keyword input[type=checkbox]').click(function() {
                          if (!jQuery(this).is(':checked')) {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('Before', 'thegem-child'); ?>');
                            jQuery('.pos').val('before');
                            setTimeout(function() {
                              jQuery('.filter.desk').find('form').submit();
                            }, 500);

                          } else {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('After', 'thegem-child'); ?>');
                            jQuery('.pos').val('after');
                            setTimeout(function() {
                              jQuery('.filter.desk').find('form').submit();
                            }, 500);

                          }
                        });

                        jQuery('.busi-filter .filter.desk .synonyms input[type=checkbox] , #main-filter-modal .synonyms input[type=checkbox]').click(function() {
                          if (!jQuery(this).is(':checked')) {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('Off', 'thegem-child'); ?>');
                            jQuery('.synonyms-val').val('off');

                          } else {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('On', 'thegem-child'); ?>');
                            jQuery('.synonyms-val').val('on');
                            jQuery('.rhyming-val').val('off');
                            if (jQuery('.one-word').val() === "on" && jQuery('.two-word').val() === "off") {
                              jQuery('.one-word').val('off');
                              jQuery('.two-word').val('off');
                            }
                          }
                        });

                        jQuery('.rhyming input[type=checkbox]').click(function(e) {
                          if (!jQuery(this).is(':checked')) {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('Off', 'thegem-child'); ?>');
                            jQuery('.rhyming-val').val('off');
                            jQuery('.oneword, .twoword').removeAttr('disabled');
                            jQuery('.oneword, .twoword').prop("checked", true);
                            jQuery('.one-word, .two-word').val('on');
                            jQuery('.word-type').removeClass('rhyming-on');
                            jQuery('.synonyms-val').val('on');
                            jQuery('.rhyming-val').val('off');
                          } else {
                            jQuery(this).parents('.switch-toggle').find('.condition').find('p').text('<?php echo __('On', 'thegem-child'); ?>');
                            jQuery('.rhyming-val').val('on');
                            jQuery('.oneword').prop("checked", false).attr('disabled', 'disabled');
                            jQuery('.twoword').prop("checked", true).attr('disabled', 'disabled');
                            jQuery('.one-word, .two-word').val('off');
                            jQuery('.synonyms-val').val('off');
                            jQuery('.word-type').addClass('rhyming-on');
                            jQuery('.synonyms-val').val('off');
                            jQuery('.rhyming-val').val('on');
                          }
                        });

                      }

                      function eleBgClass() {
                        jQuery('.list-star').children('li').removeClass('bg');
                        if (window.innerWidth >= 992) {
                          jQuery('.list-star').children('li:first-child').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(2)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(3)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(4)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(8n+9)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(8n+10)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(8n+11)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(8n+12)').addClass('bg');
                        } else if (window.innerWidth < 992 && window.innerWidth >= 767) {
                          jQuery('.list-star').children('li:first-child').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(2)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(3)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(6n+7)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(6n+8)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(6n+9)').addClass('bg');
                        } else if (window.innerWidth < 768 && window.innerWidth >= 480) {
                          jQuery('.list-star').children('li:first-child').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(2)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(4n+5)').addClass('bg');
                          jQuery('.list-star').children('li:nth-child(4n+6)').addClass('bg');
                        } else {
                          jQuery('.list-star').children('li:nth-child(2n+1)').addClass('bg');
                        }
                      }


                      jQuery.xhrPool = []; // array of uncompleted requests
                      jQuery.xhrPool.abortAll = function() { // our abort function
                        jQuery(this).each(function(idx, jqXHR) {
                          jqXHR.abort();
                        });
                        jQuery.xhrPool.length = 0
                      };

                     

                      jQuery('#Search').keypress(function(e) {
                        if (e.keyCode === 8) {
                          return true;
                        }
                        var regex = new RegExp("^[\x7f-\xffa-zA-Z0-9\-\.]+$");
                        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                        if (regex.test(str)) {
                          return true;
                        }
                        if (e.which == 13) {
                          jQuery(this).submit();
                          return false;
                        }

                        e.preventDefault();
                        return false;
                      });
                      
                      jQuery(document).ready(function(){
                       
                       jQuery('#domain-checker-form').addClass("new-result-page");

                      }); 

                      
                      jQuery(document).on('click','.navbar .navbar-toggler', function(){
                          
                        if( jQuery(window).width() < 768 ){  
                          if( jQuery(this).hasClass('opened') ){
                              var ele = jQuery(this);
                              jQuery(this).removeClass('opened').addClass('collapsed');
                                setTimeout(function(){
                                  jQuery(ele).next('div').removeClass('show');
                                  jQuery(ele).parent().parent('.navbar').removeClass('navbar-onscroll'); 
                              }, 400);
                          }else{
                            jQuery(this).addClass('opened').removeClass('collapsed');
                          }
                        }
                      }); 
                     
                      /**
                       *
                       *Generator V2 code script ends
                       *
                       *
                       */
                    </script>
                    <?php //echo do_shortcode('[user_active_window]'); ?>
                    <?php get_footer(); 
                    include_once(dirname(__FILE__) . '/footer-modals.php'); 
                    include_once(dirname(__FILE__) . '/inc/Js-functions.php'); 
               