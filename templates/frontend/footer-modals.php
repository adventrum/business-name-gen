
<!----------------------------------------------------------------------------
* Exit Content Popup
* ---------------------------------------------------------------------------->
<div class="exit-popup">
    <div class="snp-builder snp-bld-showme">
      <div class="snp-bld-step-cont snp-bld-step-cont-1 snp-bld-center">
          <div data-width="900" data-height="499" class="snp-bld-step animated fadeInUp" style="display: block;">
            <div class="bld-el-cont bld-el-img bld-step-1-el-1 "> <img loading="lazy" src="https://1qelgi1nkh1arphqg277c3i1-wpengine.netdna-ssl.com/wp-content/uploads/2020/12/wixwhite.png" class="bld-el sp-no-webp"> </div>
            <div class="bld-el-cont">
                <div class="bld-el">
                  <p style="text-align: left;"> <span> <b>Create a logo to match your new name with the Wix Logo Maker</b> </span> </p>
                </div>
            </div>
            <div class="bld-el-button"> <a href="https://wixstats.com/?a=54991&amp;c=2767&amp;s1=" class="bld-el snp-cursor-pointer" target="_blank" rel="noopener">Start Now</a></div>
          </div>
      </div>
    </div>
</div> 
<div class="snp-overlay"></div> 