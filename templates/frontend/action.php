<?php
include_once( dirname(__FILE__).'/saved-ideas-translation.php' );
$string="";
$nwstring="";


 $email_setting = json_decode(get_option('bnwm_email_settings'));
 
 $subject = isset($email_setting->email_subject) && !empty($email_setting->email_subject) ? $email_setting->email_subject : 'Your Business Name Generator Ideas';

 $header_logo = isset($email_setting->header_logo) && !empty($email_setting->header_logo) ? wp_get_attachment_url($email_setting->header_logo) : WORD_MANAGER_PLUGIN_URL.'assets/images/header-logo.png';

 $header_right_txt = isset($email_setting->header_right_text) && !empty($email_setting->header_right_text) ? $email_setting->header_right_text : 'Visit';

 $header_right_url = isset($email_setting->header_right_url) && !empty($email_setting->header_right_url) ? $email_setting->header_right_url : 'https://businessnamegenerator.com';


 $saved_ideas_title = isset($email_setting->saved_ideas_title) && !empty($email_setting->saved_ideas_title) ? $email_setting->saved_ideas_title : 'Your Saved Ideas';


 $register_button_text = isset($email_setting->register_button_text) && !empty($email_setting->register_button_text) ? $email_setting->register_button_text : 'Register Free';


 $footer_title_text = isset($email_setting->footer_title_text) && !empty($email_setting->footer_title_text) ? $email_setting->footer_title_text : 'Create a website for Free';



  $footer_button_text = isset($email_setting->footer_button_text) && !empty($email_setting->footer_button_text) ? $email_setting->footer_button_text : 'Learn More';
  

  $footer_description = '<p style="color:#fff;font-size:16px;font-family: arial;">Get Started with easy affordable website hosting with GoDaddy.</p><ul style="list-style:none;font-size:16px;color:#fff;padding:0;font-family: arial;">
                                               <li style="color:#fff;">- Free Site Builder</li>
                                               <li style="color:#fff;">- One-click Wordpress Install</li>
                                               <li style="color:#fff;">- 24/7 Live Chat &amp; Phone Support</li>
                                            <li style="color:#fff;">- 30-Day Money-Back Guarantee</li>
                                            </ul>'; 

  $footer_description = isset($email_setting->footer_description) && !empty($email_setting->footer_description) ? $email_setting->footer_description : $footer_description;


  $footer_image = isset($email_setting->footer_image) && !empty($email_setting->footer_image) ? wp_get_attachment_url($email_setting->footer_image) : WORD_MANAGER_PLUGIN_URL.'assets/images/footer-logo.png';


   $aff_link   = !empty($_POST['aff_url']) ? $_POST['aff_url'] : 'https://www.dpbolvw.net/click-5701193-12648943';
   $learn_more = !empty($_POST['aff_url']) ? $_POST['aff_url'] : 'https://www.dpbolvw.net/click-5701193-12648943';
   $title_text = 'Get Started with easy affordable website hosting with GoDaddy.';
   $translated_content = savedIdeasTranslatedContent($_POST['country_code'], $_POST['lang_code']);
   
   $copy = $translated_content['copy'];
   $learn_more_label = $translated_content['learn_more'];
   $register_label = $translated_content['register'];


    $email = $_POST['email'];
    if(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL) === false){
        // MailChimp API credentials
        $api_key = '';
        $apiKey  = '07e969f5d138b0766808c596903844c0-us5';
        $listID  = 'c0badb5359';
        // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
        // member information
        $data = array(
        'apikey'        => $api_key,
        'email_address' => $email,
        'status'        => 'subscribed',
        'tags' => [ 'BNG' ] 
        );

        $json = json_encode($data);
        
        if(!empty($_POST['marketing_email']))
        { 
            // This condition for send request to mailchimp if condition is true
            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
            curl_close($ch);
        }
        
        $to = "$email";
        $subject = $subject;
        //$headers = 'From: Your Business Name Generator Ideas <mail@businessnamegenerator.com>'. "\r\n";
        $headers = "From: mail@businessnamegenerator.com\r\n";
        $headers .= "Reply-To: mail@businessnamegenerator.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email</title>
    <style>
        body {
            padding: 0;
            margin: 0;
        }
        ul{
            list-style:none;
            padding:0px;
        }
        @media only screen and (max-width: 480px) {
            .logo img {
                width: 100px;
            }

            .img-area img {
                width: 120px !important;
            }

            p,
            a {
                font-size: 12px !important;
            }
        }
    </style>
</head>

<body>
    <table width="100%" align="center">
        <tr valign="top" align="center">
            <td align="center" valign="top">
                <table width="600" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr class="head" valign="top">
                            <td background="'.WORD_MANAGER_PLUGIN_URL.'assets/images/header-bg.png'.'"
                                width="100%" valign="top" style="text-align:left; background-size:100% 100%;background-repeat: no-repeat;">
                                <!--[if gte mso 9]>
                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false">
                                    <v:fill type="tile" src="https://4fi2fy3hz0lp1pco501bgin0-wpengine.netdna-ssl.com/wp-content/uploads/2017/12/header-bg.png" color="#7bceeb" />
                                    <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                <table class"email_info" width="100%" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td style="padding: 15px 0px 15px 15px"><a class="logo" href="https://businessnamegenerator.com"><img
                                                        src="'.$header_logo.'"
                                                        alt=""></a></td>
                                            <td style="padding-right: 15px;text-align: right;padding-top: 5px;">
                                                <p style="font-size:14px;color:#fff;font-family: arial;">'.$header_right_txt.' <a style="text-decoration: none;color:#fff;" href="'.$header_right_url.'">Businessnamegenerator.com</a>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--[if gte mso 9]>
                                    </v:textbox>
                                    </v:rect>
                                <![endif]-->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3 style="text-align:center;color:#000;font-size:22px;font-family: arial;">'.$saved_ideas_title.'</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width=100%; style="padding-bottom:15px;">
                                    <tbody>';
            /*foreach($_POST['list'] as $list)
            {
            //$string=$list." , ".$string;
            $nwstring='<a href="https://businessnamegenerator.com/wp-content/themes/thegem-child/godaddycheck.php?search='.$list.'" target="_blank">'.$list."</a>  , ".$nwstring;
            }
            $name= trim($string);
            //$fname = substr($name, 0, -1);
            //$nname= trim($nwstring);
            $ffname = substr($string, 0, -1); 
            $fnameidea = explode(" , ",$ffname);*/
            /*$fnameidea = trim($fnameidea, ",");*/
            $list = array_unique($_POST['list']);
            foreach($list as $allideas)
            { 
                $tld = '';
                if(count(explode('.', $allideas)) > 1){
                 
                 $explode  = explode('.', $allideas,2);
                 $allideas = strtoupper($explode[0]);
                 $tld = '<span>.'.strtoupper($explode[1]).'</span>';
                }
               
            $message .='<tr>
                                            <td>
                                                <h5 style="font-size:16px;color:#000;font-family:arial;text-align: right;padding-right: 15px;font-weight: 400;margin: 12px 0;">'.$allideas.''.$tld.'</h5>
                                            </td>
                                            <td>
                                                <div class="link" style="padding-left:15px;">
                                                    <a style="font-size:14px;color:#fff;font-family:arial;background: #FFA100;padding: 8px 15px;text-decoration:none;border-radius: 5px;"
                                                        href="'.$aff_link.'">'.$register_button_text.'</a>
                                                </div>
                                            </td>
                                        </tr>';
            }
        $message .= '</tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="content-area">
                            <td background= "'.WORD_MANAGER_PLUGIN_URL.'assets/images/header-bg.png'.'"
                                width="100%" valign="top" style="text-align:left; background-size:100% 100%;background-repeat: no-repeat;">
                                <!--[if gte mso 9]>
                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false">
                                    <v:fill type="tile" src="https://4fi2fy3hz0lp1pco501bgin0-wpengine.netdna-ssl.com/wp-content/uploads/2017/12/header-bg.png" color="#7bceeb" />
                                    <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                <table width="100%" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
                                    <tbody>
                                        <tr>
                                            <td class="img-area">
                                                <img style="width:220px;" src="'.$footer_image.'"
                                                    alt="">
                                            </td>
                                            <td class="content" style="padding:15px 10px 20px 0">
                                            <h2 style="color:#fff;font-size:22px;font-family: arial;">'.$footer_title_text.'</h2>
                                            <div class="info" style="color:#fff;font-size:16px;font-family: arial; list-style:none;">'.$footer_description.'</div>
                                            <div class="link" style="padding-top:15px;text-align: left;">
                                            <a style="text-align:center;text-decoration: none; color:#fff;background:#FFA100;border-radius: 5px;font-size:16px;display: inline-block;font-family: arial;border-style: solid; border-width: 8px 30px; border-color: #FFA100;" href="' . $aff_link . '">'.$footer_button_text.'</a>
                                            </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!--[if gte mso 9]>
                                    </v:textbox>
                                    </v:rect>
                                <![endif]-->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>';

        @mail( $to, $subject, $message,$headers);
        if(!empty($_POST['marketing_email']))
        { 
            // This condition for check mailchimp request send successfully or not
            if ($httpCode == 200) {
                echo 'success'; die;
            } else {
                switch ($httpCode) {
                    case 214:
                        echo 'Given email address is already subscribed, thank you!';
                        break;
                    default:
                        echo 'Oops. Something went wrong. Please try again later.';
                        break;
                }
            }
        }else{
            echo 'success'; die;
        }
    }else{
        echo 'Please provide a valid email address.';die;
    }