<?php


if ( !defined('ICL_LANGUAGE_CODE') ){
    define('ICL_LANGUAGE_CODE', 'en');
}
if ( !defined('CURRENT_MULTISITE_CODE') ){
    $server_uri = $_SERVER['REQUEST_URI'];
    $multisites = ['pt-br', 'pt-pt','it','fr','de','es','ar','da','nl','el','id','ms','no','pl','ru','sv','th','tr'];
    $current_site = explode('/', $server_uri)[1];
    if( in_array($current_site, $multisites) ){
        $site_key = array_search($current_site, $multisites);
        $current_site = $multisites[$site_key];
    } else {
        $current_site = '';
    }
    define('CURRENT_MULTISITE_CODE', $current_site);
}


function Geobasedbanner($slug)
{
  global $post;

  $advert_id = '';

  $args = array(
    'numberposts' => 1,
    'offset' => 0,
    'order' => 'DESC',
    'post_type' => 'advertisement',
    "suppress_filters" => false,
    "name" => '' . $slug . ''
  );

  if (get_posts($args)[0]->ID) {
    $advert_id = get_posts($args)[0]->ID;
  }

  return $advert_id;
}

function my_wp_is_mobile()
{
  static $is_mobile;

  if (isset($is_mobile))
    return $is_mobile;

  if (empty($_SERVER['HTTP_USER_AGENT'])) {
    $is_mobile = false;
  } elseif (
    strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
    || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false
  ) {
    $is_mobile = true;
  } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
    $is_mobile = true;
  } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
    $is_mobile = false;
  } else {
    $is_mobile = false;
  }

  return $is_mobile;
}


function carousel_ads($newlist)
{

  foreach ($newlist as $domain) : ?>

    <div class="domain-buy">
      <div class="inner">
        <a class="outer-link click-event outbound-link" data-category="ResultsPage" data-action="Domainify Ad" data-label="<?php echo $domain['title']; ?>" href="<?php echo $domain['permalink']; ?>" target="_blank" rel="nofollow noopener">
          <div class="img-area">
            <img src="<?php echo $domain['featured_src']; ?>" class="img-responsive" alt="<?= $domain['alt'] ?>">
          </div>
          <div class="content">
            <h3 style="text-align: left;"><?php echo $domain['title']; ?></h3>
            <?php
            $price = !empty($domain['sale_price']) ? $domain['sale_price'] : $domain['price'];
            ?>
            <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?php echo number_format($price); ?></span></ins>
            <div class="link">
              <p>View</p>
            </div>
          </div>
        </a>
      </div>
    </div>
  <?php endforeach; ?>

  <?php
}


function carousel_ads_new($newlist, $currency = '', $category = 'ResultsPage')
{

  foreach ($newlist as $domain) : ?>

    <div class="domain-buy">
      <div class="inner">
        <a class="outer-link click-event outbound-link" data-category="<?= $category; ?>" data-action="Domainify Ad" data-label="<?php echo $domain['title']; ?>" href="<?php echo $domain['permalink']; ?>" target="_blank" rel="nofollow noopener">
          <div class="img-area">
            <img src="<?php echo $domain['featured_src']; ?>" class="img-responsive" alt="<?= $domain['alt'] ?>" height="100" width="100">
          </div>
          <div class="content">
            <div class="buy-text">
              <h3 style="text-align: left;"><?php echo $domain['title']; ?></h3>
              <?php
              $price = !empty($domain['sale_price']) ? $domain['sale_price'] : $domain['price'];
              ?>
              <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?php echo number_format($price) . ' ' . $currency; ?></span></ins>
            </div>
            <div class="link">
              <p>View <span><i class="fas fa-arrow-right"></i></span></p>
            </div>
          </div>
        </a>
      </div>
    </div>
  <?php endforeach; ?>

<?php
}

function  before_carousel_ads_domains()
{
?>

  <div class="domainify-text">
    <div class="col-md-12">
      <p>Kickstart your business with a Premium Domain and Branding Package!</p>
      <a href="https://domainify.com/names/" target="_blank">View more premium domains <span><i class="fas fa-arrow-right"></i></span></a>
    </div>
  </div>

<?php
}

function add_more_filter_inputs()
{
?>

  <?php if (isset($_GET['home_id']) && !empty($_GET['home_id'])) : ?>
    <input type="hidden" name="home_id" value="<?= trim($_GET['home_id']);  ?>">
  <?php endif; ?>


  <?php if (isset($_GET['stickyads']) &&  !empty($_GET['stickyads'])  && $_GET['stickyads'] == 'true') : ?>
    <input type="hidden" name="stickyads" value="true">
  <?php endif;  ?>

  <?php if (isset($_GET['sidebarads-category']) && !empty($_GET['sidebarads-category'])) : ?>
    <input type="hidden" name="sidebarads-category" value="<?= $_GET['sidebarads-category']; ?>">
  <?php endif;  ?>

  <?php if (isset($_GET['stickyads-category']) && !empty($_GET['stickyads-category'])) : ?>
    <input type="hidden" name="stickyads-category" value="<?= $_GET['stickyads-category']; ?>">
  <?php endif;  ?>

  <?php if (isset($_GET['device']) && !empty($_GET['device'])) : ?>
    <input type="hidden" name="device" value="<?= trim($_GET['device']);  ?>">
  <?php endif; ?>

  <?php if (isset($_GET['all-indus']) && !empty($_GET['all-indus'])) : ?>
    <input type="hidden" name="all-indus" value="all">
  <?php endif; ?>

  <?php if (isset($_GET['redirect']) && !empty($_GET['redirect'])) : ?>
    <input type="hidden" name="redirect" value="<?= trim($_GET['redirect']);  ?>">
  <?php endif; ?>

  <?php if (isset($_GET['shortcode_id']) && !empty($_GET['shortcode_id'])) : ?>
    <input type="hidden" name="shortcode_id" value="<?= trim($_GET['shortcode_id']);  ?>">
  <?php endif; ?>

<?php
}
?>


<?php
function click_event_purpose($category = 'ResultsPage', $action = 'KeywordMatching')
{
?>

  <div>
    <input type="hidden" class="keywordmatching click-event" data-category="<?= $category; ?>" data-action="<?= $action; ?>">
    <input type="hidden" class="add-event click-event">
  </div>

<?php
}

function geo_targeting_ads($advert_id)
{

?>
  <?php

  $country_codes = [];

  if (have_rows('geo_targeting_banner', 'option')) {
    while (have_rows('geo_targeting_banner', 'option')) {
      the_row();
      if (!in_array(get_sub_field('country_code'), $country_codes)) {
        array_push($country_codes, get_sub_field('country_code'));
      }
    }
  }

  if (in_array(getenv('HTTP_GEOIP_COUNTRY_CODE'), $country_codes)) {
    echo do_shortcode('[insertadvnew id="' . $advert_id . '"]');
  } else {
    if (have_rows('geo_targeting_default', 'option')) {

      while (have_rows('geo_targeting_default', 'option')) {
        the_row();
        if (my_wp_is_mobile()) {

          $advert_id = get_sub_field('mob_ads_default');
          echo do_shortcode('[insertadvnew id="' . $advert_id . '"]');
        } else {
          $advert_id = get_sub_field('desk_ads_default');
          echo do_shortcode('[insertadvnew id="' . $advert_id . '"]');
        }
      }
    }
  }
}

function geo_targeting_rslt_dropdown($advert_id)
{
  $country_codes = [];

  if (have_rows('geo_targeting_banner', 'option')) {
    while (have_rows('geo_targeting_banner', 'option')) {
      the_row();
      if (!in_array(get_sub_field('country_code'), $country_codes)) {
        array_push($country_codes, get_sub_field('country_code'));
      }
    }
  }

  if (in_array(getenv('HTTP_GEOIP_COUNTRY_CODE'), $country_codes)) {
    return $advert = preg_replace(
      '/\n/',
      '',
      trim(do_shortcode('[insertadvnew id="' . $advert_id . '"]'))
    );
  } else {

    global $post;

    $advert_id = '44811';

    $args = array(
      'numberposts' => 1,
      'offset' => 0,
      'order' => 'DESC',
      'post_type' => 'advertisement',
      "suppress_filters" => false,
      "name" => 'looka-banner-dropdown'
    );

    if (get_posts($args)[0]->ID) {
      $advert_id = get_posts($args)[0]->ID;
    }

    return $advert = preg_replace(
      '/\n/',
      '',
      trim(do_shortcode('[insertadvnew id="' . $advert_id . '"]'))
    );
  }
}

function top_sticky_banner_desk()
  {
    $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/godaddy-logo.png';
  ?>
  
  <div class="leader-right outbound-link" data-category="stickybanners" data-action="desktop-mrec-right" data-label="https://www.tkqlhce.com/click-3797283-10730504">
     <div class="right-cad" link="https://www.tkqlhce.com/click-3797283-10730504">
        <div class="head">
           <div class="cad-img">
              <picture class="sp-no-webp">
                 <source srcset="<?= $src; ?>" type="image/webp">
                 <source srcset="<?= $src; ?>" type="image/png">
                 <img src="<?= $src; ?>" class="sp-no-webp" srcset="<?= $src; ?>">
              </picture>
           </div>
           <div class="link">
              <a href="https://www.tkqlhce.com/click-3797283-10730504" target="_blank">
              View Offer                    </a>
           </div>
        </div>
        <div class="content">
           <div class="text">
              <h3>Get 70% off your Domain Purchase</h3>
              <p>Purchase your domain with Godaddy today and save. World’s Largest Registrar. Award-Winning Support.</p>
           </div>
        </div>
     </div>
     <div class="hide-sec hide-btn">
        <p>HIDE <i class="fa fa-angle-down"></i></p>
     </div>
  </div>

<?php
}

function bottom_sticky_banner_desk()
{
    $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/godaddy-logo.png';
 ?>
   <div class="leader-bottom outbound-link" data-category="stickybanners" data-action="desktop-leaderboard-bottom" data-label="https://www.tkqlhce.com/click-3797283-10730504">
      <div class="bottom-cad" link="https://www.tkqlhce.com/click-3797283-10730504">
         <div class="col-left">
            <div class="img-area">
               <picture class="sp-no-webp">
                  <source srcset="<?= $src; ?>" type="image/webp">
                  <source srcset="<?= $src; ?>" type="image/png">
                  <img src="<?= $src; ?>" class="sp-no-webp" srcset="<?= $src; ?>">
               </picture>
               <div class="link-btn mob">
                  <a class="https://www.tkqlhce.com/click-3797283-10730504" href="https://www.tkqlhce.com/click-3797283-10730504" target="_blank">
                  View Offer                    </a>
               </div>
            </div>
            <div class="content-area">
               <h2>Get 70% off your Domain Purchase!</h2>
               <p>Purchase your domain with Godaddy today and save. World’s Largest Registrar. Award-Winning Support.</p>
            </div>
         </div>
         <div class="col-right">
            <div class="link-btn">
               <a href="https://www.tkqlhce.com/click-3797283-10730504" target="_blank">View Offer</a>
            </div>
         </div>
      </div>
      <div class="hide-sec hide-btn">
         <p>HIDE <i class="fa fa-angle-down"></i></p>
      </div>
   </div>

 <?php
  
}

function bottom_sticky_banner_mob()
{
    $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/godaddy-logo.png';
 ?>
 
 <div class="leader-mobile outbound-link" style="display: none;" data-category="stickybanners" data-action="mobile-leaderboard-bottom" data-label="https://www.tkqlhce.com/click-3797283-10730504">
    <div class="right-cad" link="https://www.tkqlhce.com/click-3797283-10730504">
       <div class="head">
          <div class="cad-img">
             <picture class="sp-no-webp">
                <source srcset="<?= $src; ?>" type="image/webp">
                <source srcset="<?= $src; ?>" type="image/png">
                <img src="<?= $src; ?>" class="sp-no-webp" srcset="<?= $src; ?>">
             </picture>
          </div>
          <div class="link">
             <a href="https://www.tkqlhce.com/click-3797283-10730504" target="_blank">
             View Offer                  </a>
          </div>
       </div>
       <div class="content">
          <div class="text">
             <h3>test3</h3>
             <p>Purchase your domain with Godaddy today and save. World’s Largest Registrar. Award-Winning Support.</p>
          </div>
       </div>
    </div>
    <div class="hide-sec hide-btn">
       <p>HIDE <i class="fa fa-angle-down"></i></p>
    </div>
 </div>

 <?php
  
}

function sidebar_ads_category($ad_category, $ad_type)
{

  if ($ad_type == 'custom') {

    if (have_rows('' . $ad_category . '', 'option')) {
      the_row();

      if (have_rows('sidebar_title')) {
        while (have_rows('sidebar_title')) {
          the_row();

          echo '<h2>' . get_sub_field('heading_title') . '</h2>';
        }
      }

      if (have_rows('custom_ads')) {
        echo "<div class='leader-main'>";
        while (have_rows('custom_ads')) {
          the_row();

          $btn_color   =  get_sub_field('button_color') ? "style='color: " . get_sub_field('button_color') . "'" : '';
          $border_color =  get_sub_field('border_color') ? "style='border-color: " . get_sub_field('border_color') . "'" : '';

        ?>
          <?php if (get_sub_field('logo') || get_sub_field('title') || get_sub_field('description')) {  ?>

            <?php $affiliate = get_sub_field('affiliate_link') ? get_sub_field('affiliate_link') : "#";    ?>

            <div class="leader-right outbound-link" data-category="username-resultspage" data-action="custom-sidebar-ad" data-label="<?= $affiliate; ?>">

              <div class="right-cad" <?= $border_color; ?> link="<?= $affiliate; ?>">
                <div class="head">
                  <div class="cad-img">
                    <img src="<?= get_sub_field('logo'); ?>" width="138" height="35">
                  </div>
                </div>
                <div class="content">
                  <div class="text">
                    <h3><?= get_sub_field('title'); ?></h3>
                    <p><?= get_sub_field('description'); ?></p>
                    <div class="link">
                      <a href="<?= $affiliate; ?>" target="_blank">View Offer</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <?php } ?>

        <?php
        }
        echo "</div>";
      }
    }
  } else {

    if (have_rows('' . $ad_category . '', 'option')) {
      the_row();

      if (have_rows('iframe_ads')) {
        echo "<div class='leader-main'>";
        while (have_rows('iframe_ads', 'option')) {
          the_row();

          echo get_sub_field('iframe_code');
        }
        echo "</div>";
      }
    }
  }
}





function bottom_sticky_banner_username_desk($ad_category)
{

  if (have_rows('' . $ad_category . '', 'option')) {
    the_row();

    if (have_rows('leader-bottom')) {
      while (have_rows('leader-bottom')) {
        the_row();

        $btn_color   =  get_sub_field('button_color') ? "style='color: " . get_sub_field('button_color') . "'" : '';
        $border_color =  get_sub_field('border_color') ? "style='border-color: " . get_sub_field('border_color') . "'" : '';

        ?>

        <?php if (get_sub_field('logo') || get_sub_field('title') || get_sub_field('description')) {  ?>

          <?php $affiliate = get_sub_field('affiliate_link') ? get_sub_field('affiliate_link') : "#";    ?>

          <div class="leader-bottom outbound-link" data-category="stickybanners" data-action="desktop-leaderboard-bottom" data-label="<?= $affiliate; ?>">
            <div class="bottom-cad" <?= $border_color; ?> link="<?= $affiliate; ?>">

              <div class="col-left">

                <div class="img-area">
                  <img loading="lazy" src="<?= get_sub_field('logo'); ?>">
                  <div class="link-btn mob">
                    <a <?= $btn_color;  ?> href="<?= $affiliate; ?>" target="_blank">
                      <?= get_sub_field('button_text'); ?>
                    </a>
                  </div>
                </div>
                <div class="content-area">
                  <h2><?= get_sub_field('title'); ?></h2>
                  <p><?= get_sub_field('description'); ?></p>
                </div>
              </div>

              <div class="col-right">
                <div class="link-btn">
                  <a href="<?= $affiliate; ?>" <?= $btn_color;  ?> target="_blank"><?= get_sub_field('button_text'); ?></a>
                </div>
              </div>

            </div>
            <div class="hide-sec hide-btn">
              <p>HIDE</p>
            </div>
          </div>

        <?php } ?>


      <?php
      }
    }
  }
}


function bottom_sticky_banner_username_mob($ad_category)
{

  if (have_rows('mrec-mobile')) {

    while (have_rows('mrec-mobile')) {
      the_row();

      $btn_color   =  get_sub_field('button_color') ? "style='color: " . get_sub_field('button_color') . "'" : '';
      $border_color =  get_sub_field('border_color') ? "style='border-color: " . get_sub_field('border_color') . "'" : '';

      ?>
      <?php if (get_sub_field('logo') || get_sub_field('title') || get_sub_field('description')) {  ?>

        <?php $affiliate = get_sub_field('affiliate_link') ? get_sub_field('affiliate_link') : "#";    ?>

        <div class="leader-mobile outbound-link" style="display: none;" data-category="stickybanners" data-action="mobile-leaderboard-bottom" data-label="<?= $affiliate; ?>">

          <div class="right-cad" <?= $border_color; ?> link="<?= $affiliate; ?>">
            <div class="head">
              <div class="cad-img">
                <img src="<?= get_sub_field('logo'); ?>">
              </div>

              <div class="link">
                <a href="<?= $affiliate; ?>" <?= $btn_color; ?> target="_blank">
                  <?= get_sub_field('button_text'); ?>
                </a>
              </div>
            </div>
            <div class="content">
              <div class="text">
                <h3><?= get_sub_field('title'); ?></h3>
                <p><?= get_sub_field('description'); ?></p>
              </div>
            </div>
          </div>
          <div class="hide-sec hide-btn">
            <p>HIDE <i class="fa fa-angle-down"></i></p>
          </div>
        </div>

      <?php } ?>

<?php
    }
  }
}

function mobile_iframe_ads($ad_category)
{


  if (have_rows('' . $ad_category . '', 'option')) {
    the_row();
    echo get_sub_field('iframe_code_mob');
  }
}

function IpBasedAffiliatesBusiness($sid_name = "", $home_id = "", $device = "")
{

  $lang = '';

  if (defined('ICL_LANGUAGE_CODE')) {
    $lang = ICL_LANGUAGE_CODE;
  }

  $current_country  =  getenv('HTTP_GEOIP_COUNTRY_CODE');
  $current_language =  strtoupper(getCurrentSite());

  $extra_parameter  =  '';
  if (!empty($sid_name) && !empty($home_id) && !empty($device)) {
    $extra_parameter  = '&source=' . $sid_name . '-' . $home_id . '-' . $device . '-' . $current_country . '';
  }

  $response = [];

  if ($current_country == 'DE' &&  $current_language == 'DE') {
    $response['aff_link'] = 'https://acn.ionos.de/aff_c?offer_id=2&aff_id=1039&url_id=14' . $extra_parameter . '';
    $response['aff_text'] = 'Registrieren Sie Ihre perfekte Domain bei IONOS. 0,70 € / Monat im 1. Jahr';
  } else if ($current_country == 'IT' &&  $current_language == 'IT') {
    $response['aff_link'] = 'https://acn.ionos.it/aff_c?offer_id=8&aff_id=1039&url_id=32' . $extra_parameter . '';
    $response['aff_text'] = 'Registra il tuo dominio perfetto con IONOS. A partire da 1 € / anno';
  } else if ($current_country == 'ES' &&  $current_language == 'ES') {
    $response['aff_link'] = 'https://acn.ionos.es/aff_c?offer_id=4&aff_id=1039&url_id=46' . $extra_parameter . '';
    $response['aff_text'] = 'Registra tu dominio perfecto con IONOS. Desde 1 € / año';
  } else if ($current_country !== 'ES' && $current_country !== 'MX' && $current_language == 'ES') {
    $response['aff_link'] = 'https://acn.ionos.com/aff_c?offer_id=1&aff_id=1039&url_id=7' . $extra_parameter . '';
    $response['aff_text'] = 'Registra tu dominio perfecto con IONOS. Desde $1 / año';
  } else if ($current_country == 'FR' &&  $current_language == 'FR') {
    $response['aff_link'] = 'https://acn.ionos.fr/aff_c?offer_id=5&aff_id=1039&url_id=39' . $extra_parameter . '';
    $response['aff_text'] = 'Enregistrez votre domaine parfait avec IONOS. A partir de 1 € / an';
  } else if ($current_country == 'MX' &&  $current_language == 'MX') {
    $response['aff_link'] = 'https://acn.ionos.mx/aff_c?offer_id=7&aff_id=1039&url_id=19' . $extra_parameter . '';
    $response['aff_text'] = 'Registre su dominio perfecto con IONOS GRATIS. Solo primer año.';
  } else {
    $response = [];
  }
  return $response;
}

function AltIpBasedAffiliatesBusiness($sid_name = "", $home_id = "", $device = "")
{

 

  $current_country  =  getenv('HTTP_GEOIP_COUNTRY_CODE');
  $current_language =  strtoupper(getCurrentSite());

  $extra_parameter  =  '';
  if (!empty($sid_name) && !empty($home_id) && !empty($device)) {
    $extra_parameter  = '&s2=' . $sid_name . '-' . $home_id . '-' . $device . '-' . $current_country . '';
  }

  $response = [];

  if ($current_country == 'FR') {
    $response['aff_link'] = 'https://wixstats.com/?a=54991&oc=793&c=2913&s1=' . $extra_parameter . '';
    $response['btn_text'] = 'Connectez-vous avec le bon domaine';
  } else if ($current_country == 'DE') {
    $response['aff_link'] = 'https://wixstats.com/?a=54991&oc=772&c=2894&s1=' . $extra_parameter . '';
    $response['btn_text'] = 'Mit der richtigen Domain online gehen';
  } else if ($current_country == 'IT') {
    $response['aff_link'] = 'https://wixstats.com/?a=54991&oc=791&c=2911&s1=' . $extra_parameter . '';
    $response['btn_text'] = 'Collegati online con il dominio giusto';
  } else if ($current_country == 'ES') {
    $response['aff_link'] = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=' . $extra_parameter . '';
    $response['btn_text'] = 'Conéctese con el dominio correcto';
  } else if ($current_country == 'MX') {
    $response['aff_link'] = 'https://wixstats.com/?a=54991&oc=792&c=2912&s1=' . $extra_parameter . '';
    $response['btn_text'] = 'Conéctese con el dominio correcto';
  } else if ($current_country == 'BR') {
    $response['aff_link'] = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=' . $extra_parameter . '';
    $response['btn_text'] = 'Fique online com o domínio certo';
  } else if ($current_country == 'PT') {
    $response['aff_link'] = 'https://wixstats.com/?a=54991&oc=794&c=2914&s1=' . $extra_parameter . '';
    $response['btn_text'] = 'Fique online com o domínio certo';
  } else {
    $response = [];
  }
  return $response;
}




function WixDropDownAffiliate(){
  return WixAffiliateLinks()['dropdownlink'];
}
add_shortcode('WixDropDownAffiliate', 'WixDropDownAffiliate');


function placement_banner(){

    
    $current_country  =  getenv('HTTP_GEOIP_COUNTRY_CODE');
    $current_language =  strtoupper(getCurrentSite());

 
    $response = '';
    if ( $current_language == 'EN') {
      $response = 'true';
    } else if ($current_country == 'FR' &&  $current_language == 'FR') {
      $response = 'true';
    } else if ($current_country == 'DE' &&  $current_language == 'DE') {
      $response = 'true';
    } else if ($current_country == 'IT' &&  $current_language == 'IT') {
      $response = 'true';
    } else if ($current_country == 'PT' &&  $current_country  == 'PT-PT' ) {
      $response = 'true';
    } else if ($current_country == 'BR' &&  $current_language == 'PT-BR') {
      $response = 'true';
    } else if ($current_country == 'ES' &&  $current_language == 'ES') {
      $response = 'true';
    } else if ($current_country == 'MX' &&  $current_language == 'ES') {
      $response = 'true';
    } else if ($current_country == 'NL' &&  $current_language == 'NL') {
      $response = 'true';
    } else if ($current_country == 'SE' &&  $current_language == 'SV') {
      $response = 'true';
    } else {
      $response = '';
    }
    return $response;
}




if (!function_exists('serach_word_senitize')) :
  function serach_word_senitize($query_word)
  {
      $patterns = array(
          '#^https?\:\/\/#',
          '/www./',
          '/^\s\s+/',
          '/\s\s+$/',
          '/\s\s+/u',
          '/.co.uk/',
          '/.net/',
      );

      $replace = array('', '', '', '', ' ', '', '', '', '', '');
      $query_word = preg_replace($patterns, $replace, $query_word);
      if (stripos($query_word, ',')) {
          $query_word = str_replace(',', ' ', $query_word);
          $query_word = str_replace(', ', ' ', $query_word);
      }
      if (stripos($query_word, '.com')) {
          $query_word = str_replace('.com', '', $query_word);
      }
      if (stripos($query_word, '.org')) {
          $query_word = str_replace('.org', '', $query_word);
      }
      if (stripos($query_word, '.co')) {
          $query_word = str_replace('.co', '', $query_word);
      }
      $query_word = preg_replace('/[^a-zA-Z0-9-\s]/', '', $query_word);

      return $query_word;
  }
endif;


function default_ad(){
   $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
  ?>

   <div class="desktopads-banners">
    <a class="secound-gd-link" href="https://wixstats.com/?a=54991&amp;c=2767&amp;s1=" target="_blank" rel="noopener noreferrer">
   <div class="ad-result">
   <div class="secound-gd-link"><img src="<?= $src; ?>" height="50px"></div>
   <div class="text">
   <h2>Create your business logo with<span> Wix Logo Maker</span></h2>
   </div>
   <div class="col-right">
   <div class="get first-gd-link">Start Now</div>
   </div></div>
   </a><p><a class="secound-gd-link" href="https://wixstats.com/?a=54991&amp;c=2767&amp;s1=" target="_blank" rel="noopener noreferrer"></a>
   </p></div>

  <?php
}



function banner_ad(){
 
 
  $current_country  = getenv( 'HTTP_GEOIP_COUNTRY_CODE' );
  $current_language = strtoupper(getCurrentSite());


  if( $current_language == 'FR' && $current_country == 'FR' ){
 
      $affiliate = 'https://wixstats.com/?a=54991&oc=795&c=2915&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  }
  elseif( $current_language == 'DE' && $current_country == 'DE' ){
 
      $affiliate = 'https://wixstats.com/?a=54991&oc=795&c=2915&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  } 
  elseif( $current_language == 'IT' && $current_country == 'IT' ){
  
      $affiliate = 'https://wixstats.com/?a=54991&oc=795&c=2915&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  } 
  elseif( $current_language == 'ES' && $current_country == 'ES' || $current_language == 'ES' && $current_country == 'MX' ){
  
      $affiliate = 'https://wixstats.com/?a=54991&oc=795&c=2915&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  } 
  elseif( $current_language == 'PT-BR' && $current_country == 'BR' ){
  
      $affiliate = 'https://wixstats.com/?a=54991&oc=795&c=2915&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  }
  elseif( $current_language == 'PT-PT' && $current_country == 'PT' ){
  
      $affiliate = 'https://wixstats.com/?a=54991&oc=795&c=2915&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  } 
  elseif( in_array($current_country, getGeoCountriesList()) && $current_language == 'EN' ){
  
      $affiliate = 'https://wixstats.com/?a=54991&oc=795&c=2915&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  } 
  elseif( $current_language == 'NL' && $current_country == 'NL' ){
  
      $affiliate = 'https://wixstats.com/?a=54991&oc=790&c=2910&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  }
  elseif( $current_language == 'SV' && $current_country == 'SE' ){
  
      $affiliate = 'https://wixstats.com/?a=54991&oc=831&c=2951&s1=&s2=BNG-resultsbanner';
      mainbannerGeobased($affiliate);
  } 
  elseif( $current_language == 'EN' && $current_country == 'IN' ){
      $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/godaddy-logo.png';
    ?>
    <div class="desktopads-geo-banners">
    <div class="ad-result">
      <a class="secound-gd-link" href="https://www.tkqlhce.com/click-3797283-10730504" target="_blank" rel="noopener noreferrer"><img src="<?= $src; ?>" height="50px"></a>
     <div class="text">
     <h2> Get web hosting as low as <span> ₹ 199.00/mo.</span></h2>
     <p>Fast and secure. Includes free domain + email. </p></div>
     <div class="col-right"><a class="get first-gd-link" href="https://www.tkqlhce.com/click-3797283-10730504" target="_blank" rel="nofollow noopener noreferrer">Learn More</a></div></div>
     </div> 
    <?php
  } 
  else
  {
      $ad_id = isset($_GET['shortcode_id']) && !empty($_GET['shortcode_id']) ? $_GET['shortcode_id'] : 1;
      $advs = getAdvertisements($ad_id);

      if( !empty($advs) && is_array($advs) ){
       
         $data = $advs[0]['advertisement']; 
         $template_id = $advs[0]['template_id'];
         $check_exist = renderTemplate($template_id);
         if( $check_exist ) {
           echo renderTemplate( $template_id,
              ["title"=>$data->title,
              "subtitle"=>$data->subtitle, 
              "button_link"=>$data->button_link,
              "button_caption"=>$data->button_caption,
              "img"=>wp_get_attachment_url($data->img) ] 
           );
         }
      }else{
       default_ad();
      }
  }
}


function sticky_banner(){
 
  $ad_id = isset($_GET['shortcode_id']) && !empty($_GET['shortcode_id']) ? $_GET['shortcode_id'] : 1;
  $advs = getStickyAdvertisements($ad_id);

  if( !empty($advs) && is_array($advs) ){
      
      foreach( $advs as $ads ){
         
         $data = $ads['advertisement']; 
         $template_id = $ads['template_id'];

         $position = json_decode($data->positions);
         $positions = 'style="top:'.$position->top_val.';bottom:'.$position->bottom_val.';right:'.$position->right_val.';left:'.$position->left_val.';"';
         
         echo renderTemplate( $template_id,
              ["title"=>$data->title,
              "content"=>$data->content, 
              "button_link"=>$data->button_link,
              "button_caption"=>$data->button_caption,
              "img"=>wp_get_attachment_url($data->img),
              "positions"=>$positions ] 
         );

      }

  }
}


function mainbannerGeobased($affiliate = ""){ 

   $desktop = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/desktop.png';
   $tablet = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/tab.png';
   $mobile = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/mobile.png';
  ?>

   <div class="desktopads-banners wix-banner">
     <a href="<?= $affiliate; ?>" target="_blank" rel="noopener noreferrer" class="secound-gd-link">
       <img src="<?= $desktop; ?>" class="desktop-wix">
       <img src="<?= $tablet; ?>" class="tab-wix">
       <img src="<?= $mobile; ?>" class="mobile-wix">
     </a>
   </div>

<?php

}

function dropdown_banner(){
  
  
  $current_language = getCurrentSite();
  $current_country  = getenv( 'HTTP_GEOIP_COUNTRY_CODE' );
  
  $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/godaddy-logo.png';
  $dropdownlink = WixAffiliateLinks()['dropdownlink'];
 
  $title = 'Get 70% OFF your domain purchase with GoDaddy';
  $sub_title = 'Offer applied when using ‘Register’ below';
  
  
  $languages_code = ['fr','de','it','es','pt-br','pt-pt'];

  if( $current_language == 'fr' ){
     
     $title = 'Bénéficiez de 70% de réduction lorsque vous achetez votre domaine avec GoDaddy';
     $sub_title = 'L’offre s’applique lorsque vous cliquez sur ‘S’inscrire’ ci-dessous';
   
     if( $current_country == 'FR' ){
  
      $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
      $title = 'Profitez d’un nom de domaine gratuitement en souscrivant à un abonnement Wix Premium. En savoir plus';
      $sub_title = 'L’offre s’applique lorsque vous cliquez sur ‘S’inscrire’ ci-dessous';  

     }
  }
  elseif( $current_language == 'de' ){
      
      $title = 'Erhalten Sie 70 % Rabatt auf Ihren Domain-Kauf mit GoDaddy';
      $sub_title = 'Angebot wird angewandt, wenn Sie unten auf “Registrieren“ klicken';
      
      if( $current_country == 'DE' ){
      
       $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
       $title = 'Hol dir deine Domain ohne zusätzliche Kosten mit einem Premiumpaket von Wix. Mehr Informationen';
       $sub_title = 'Angebot wird angewandt, wenn Sie unten auf “Registrieren“ klicken';  

      } 
  }
  elseif( $current_language == 'es' ){
      
      $title = 'Obtenga 70% de descuento en la compra de su dominio con GoDaddy';
      $sub_title = 'La oferta se aplica cuando hace clic en ‘Registrarse’ a continuación';
      
      if( $current_country == 'ES' || $current_country == 'MX' ){
      
       $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
       $title = 'Obtén tu dominio sin costo adicional con un plan Wix Premium. Más información';
       $sub_title = 'La oferta se aplica cuando hace clic en ‘Registrarse’ a continuación';  

      } 
  }
  elseif( $current_language == 'it' ){
      
      $title = 'Ottieni uno sconto del 70% sull’acquisto del tuo dominio con GoDaddy';
      $sub_title = 'L’offerta viene applicata cliccando su ‘Registrati’ qui sotto';
      
      if( $current_country == 'IT' ){
      
       $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
       $title = 'Ottieni il tuo dominio senza costi aggiuntivi con un pacchetto Premium Wix. Scopri di più';
       $sub_title = 'L’offerta viene applicata cliccando su ‘Registrati’ qui sotto';  

      } 
  }
  elseif( $current_language == 'pt-br' ){
      
      $title = 'Obtenha um DESCONTO DE 70% nas suas compras de domínio com GoDaddy';
      $sub_title = 'Oferta aplicada ao usar ‘Registro’ abaixo';
      
      if( $current_country == 'BR' ){
      
       $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
       $title = 'Obtenha um domínio gratuito ao adquirir um plano Premium do Wix. Saiba mais';
       $sub_title = 'Oferta aplicada ao usar ‘Registro’ abaixo';  

      } 
  }
  elseif( $current_language == 'pt-pt' ){
      
      $title = 'Obtenha um DESCONTO DE 70% nas suas compras de domínio com GoDaddy';
      $sub_title = 'Oferta aplicada ao usar ‘Registro’ abaixo';
      
      if( $current_country == 'PT' ){
      
       $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
       $title = 'Obtenha um domínio gratuito ao adquirir um plano Premium do Wix. Saiba mais';
       $sub_title = 'Oferta aplicada ao usar ‘Registre-se’ abaixo';  

      } 
  }
  elseif( $current_language == 'nl' ){
      
      $title = 'Obtenha um DESCONTO DE 70% nas suas compras de domínio com GoDaddy';
      $sub_title = 'Oferta aplicada ao usar ‘Registro’ abaixo';
      
      if( $current_country == 'NL' ){
      
       $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
       $title = 'Obtenha um domínio gratuito ao adquirir um plano Premium do Wix. Saiba mais';
       $sub_title = 'Oferta aplicada ao usar ‘Registre-se’ abaixo';  

      } 
  }
  elseif( $current_language == 'sv' ){
      
      $title = 'Obtenha um DESCONTO DE 70% nas suas compras de domínio com GoDaddy';
      $sub_title = 'Oferta aplicada ao usar ‘Registro’ abaixo';
      
      if( $current_country == 'SE' ){
      
       $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
       $title = 'Obtenha um domínio gratuito ao adquirir um plano Premium do Wix. Saiba mais';
       $sub_title = 'Oferta aplicada ao usar ‘Registre-se’ abaixo';  

      } 
  }
  else
  {
  
    if( in_array($current_country, getGeoCountriesList()) && $current_language == 'en' ){
   
     $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/wix_logo_black.png';
     $title = 'Get your domain at no extra cost with a Wix Premium plan. Find out more';
     $sub_title = 'Offer applied when using ‘Register’ below';
    
    }

  }

  $html = '<div class="desktopadv dflex"><div class="ad-result"><a class="secound-gd-link" href="'.$dropdownlink.'" target="_blank" rel="noopener noreferrer"><img src="'.$src.'" height="50px" /></a></p><div class="text"><h3>'.$title.'</h3><p>'.$sub_title.'</p></div></div>';
  
  return $html;


}

function sidebar_banner(){
    $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/sidebar-banner.png';
  ?>
 
    <p><a href="https://wixstats.com/?a=54991&amp;c=2767&amp;s1=&amp;s2=bng-sidebarbanner" target="_blank" rel="noopener"><img src="<?= $src; ?>"></a></p>
 
 <?php
}

function headerbannerGeoBased(){
    $src = WORD_MANAGER_PLUGIN_URL.'assets/frontend/Advertisements/header-banner.png';
  ?>
 
    <div class="resultpage-top-banner"> <p><a href="https://wixstats.com/?a=54991&amp;oc=795&amp;c=2915&amp;s1=&amp;s2=BNG-headerbanner" target="_blank" rel="noopener"><img src="<?= $src; ?>"></a></p></div>
 
 <?php
}

//alert message
if (!function_exists('alert_message')) :
  function alert_message($type, $message)
  {
      $messages = array('type' => $type, 'message' => $message);
      $_SESSION['messages'] = array('alert' => $messages);
  }
endif;


function get_url_var(){
    $strURL = $_SERVER['REQUEST_URI'];
    $arrVals = explode("?",$strURL);
    $arrVals2 = explode("/",$arrVals[0]);
      
    $numkey=str_word_count(stripslashes($_GET['bname']));

    if($numkey<='1'){
      $bname=stripslashes($_GET['bname']);
    }
    else{
      $fword=explode(' ',stripslashes($_GET['bname']));
      $bname=$fword[0].' '.$fword[1];
    }
    if(isset($_GET['related'])){
          $element='?bname='.$bname."&related=".$_GET['related'];
    }
    else{
        $element='?bname='.$bname;
    }
    $found=0;
    if($arrVals2==''){
      $found=$found+1;
    }
    else{

      if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE != 'en' ) {
          $found=$arrVals2[3];
      }
      if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
          $found=$arrVals2[2];
      }

    }
    return $found;
}
