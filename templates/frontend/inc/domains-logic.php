<?php 
 // $domains = array();
  $total = ''; 
  if (!isset($_SESSION['invite_modal_value'])){
     $_SESSION['invite_modal_value']=array();
  }
  if(isset($_GET['industry'])){
    unset($_SESSION['filterd_columns']);
  }
  if(isset($_GET['bname']) && preg_match('#^[a-z0-9\s]+$#i', $_GET['bname']) ){
      
      $search_word = serach_word_senitize($_GET['bname']);

      if(!preg_split('/\s+/', stripslashes( $search_word ) )){
        $message = "Please enter atleast 1 word to generate results.";
        alert_message('success', $message);
      }
       // $search_word = preg_replace(,'', $search_word);

    $numkey = count(preg_split('/\s+/', stripslashes( $search_word ) ));

    if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ) {
      include_once( dirname( __FILE__ ).'/domains-word-generator.php' );
    }else{
      include_once( dirname( __FILE__ ).'/international-lang-word-generator.php' );
    }
  }
  
  //shuffle( $domains );

  $_SESSION['synonyms_set'] = ( !empty($synonyms_list) ) ? 'yes' : 'no';

  if( !$detect->isMobile() && !$detect->isTablet() ):
    $limit = 96;
  endif;

  if( $detect->isTablet() ):
    $limit = 72;
  endif;

  if( $detect->isMobile() && !$detect->isTablet() ):
    $limit = 30;
  endif;

  function get_url_var(){
      $strURL = $_SERVER['REQUEST_URI'];
      $arrVals = explode("?",$strURL);
      $arrVals2 = explode("/",$arrVals[0]);
        
      $numkey=str_word_count(stripslashes($_GET['bname']));

      if($numkey<='1'){
        $bname=stripslashes($_GET['bname']);
      }
      else{
        $fword=explode(' ',stripslashes($_GET['bname']));
        $bname=$fword[0].' '.$fword[1];
      }
      if(isset($_GET['related'])){
            $element='?bname='.$bname."&related=".$_GET['related'];
      }
      else{
          $element='?bname='.$bname;
      }
      $found=0;
      if($arrVals2==''){
        $found=$found+1;
      }
      else{

        if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE != 'en' ) {
            $found=$arrVals2[3];
        }
        if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ){
            $found=$arrVals2[2];
        }

      }
      return $found;
  }

  if(!empty($mainstring) && ($mainstring || $general_liststring) ){
    $filter_columns = '';
    $page = get_url_var();

    $ofset =(!empty($page) && $page !=1) ? ( $page * $limit-$limit) :0;
    if ( defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE == 'en' ) {

    /** code for new Results Filters **/

      if(isset($_GET['one_word']) && $_GET['one_word'] == 'on' && $_GET['two_word'] == 'off' && $_GET['rhyming'] == 'off' && $_GET['synonyms'] == 'on' ){
        
        $mainstringnew = tlds_filter($suffix_list);
        $mainstring2 = array_slice( $mainstringnew, $ofset, $limit, true );
        $total= count( $mainstringnew );
      }

      if(isset($_GET['one_word']) && $_GET['one_word'] == 'on' && $_GET['two_word'] == 'off' && $_GET['rhyming'] == 'off' && $_GET['synonyms'] == 'off' ){
        
        $mainstringnew = tlds_filter($suffix_list);
        $mainstring2 = array_slice( $mainstringnew, $ofset, $limit, true );
        $total= count( $mainstringnew );
      }

      if(isset($_GET['two_word']) && $_GET['two_word'] == 'on' && $_GET['one_word'] == 'off' && $_GET['rhyming'] == 'off' && $_GET['synonyms'] == 'off'){
        
        $mainstringnew = array_merge($mainstring, $general_liststring);
        $mainstringnew = tlds_filter($mainstringnew);
        $mainstring2   = array_slice( $mainstringnew, $ofset, $limit, true );
        $total= count( $mainstringnew );
      }

      if(isset($_GET['one_word']) && $_GET['one_word'] == 'on' && $_GET['two_word'] == 'on' && $_GET['synonyms'] == 'on' && $_GET['rhyming'] == 'off'){

        $mainstringnew = array_merge( $mainstring, $general_liststring );
        $mainstringnew = tlds_filter($mainstringnew);

        if( !empty($synonyms_list) ){

          $synonyms_list = tlds_filter($synonyms_list); 
          $suffix_list  = tlds_filter($suffix_list);
        
          if( !$detect->isMobile() && !$detect->isTablet() ){ 
            $limit = ($page >= 4) ? 72: 56;

            $synlimit = 20;
            $synofset = ( !empty($page) && $page !=1 ) ? ($page * $synlimit-$synlimit) :0;

            $suflimit = 20;
            $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;

          }

          if( $detect->isMobile() && !$detect->isTablet()){
            $limit = 10;

            $synlimit = 10;
            $synofset = ( !empty($page) && $page !=1 ) ? ( $page * $synlimit-$synlimit ) :0;

            $suflimit = 10;
            $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
          }

          $arr = array_slice( $synonyms_list, $synofset, $synlimit, true );

          $arr2 = array_slice( $suffix_list, $sufofset, $suflimit, true );

          if( count($arr2) == 0 ){

            $arr = array_slice( $synonyms_list, $sufofset, 24, true );
          }

          if( empty($arr) && empty($arr2)) { $limit = 96; }

          $arr3 = array_slice( $mainstringnew, $ofset, $limit, true );

          $mix = array_merge( $arr, $arr2 );

          $mainstring2 = array_merge( $arr3, $mix );

          shuffle( $mainstring2 );

          $total= count( $mainstringnew ) + count( $synonyms_list ) + count($suffix_list);
       
        }else{
          $mainstring2 = array_slice( $mainstringnew, $ofset, $limit, true );
          $total= count( $mainstringnew ) + count($suffix_list);
        }
        
      }

      if(isset($_GET['one_word']) && $_GET['one_word'] == 'on' && $_GET['two_word'] == 'on' && $_GET['rhyming'] == 'off' && !isset($_GET['synonyms']) ){

          $mainstringnew = array_merge( $mainstring, $general_liststring );
          $mainstringnew = tlds_filter($mainstringnew);
          
          $limit = 96;

          $suflimit = 20;
          $sufofset = ( !empty($page) && $page !=1 ) ? ($page * $suflimit-$suflimit) :0;
          $suffix_listn = array_slice( $suffix_list, $sufofset, 20, true );
          $suffix_listn = tlds_filter($suffix_listn);
          
          $mainstrin = array_slice( $mainstringnew, $ofset, $limit, true );

          $mainstring2 = array_merge( $mainstrin, $suffix_listn );
          
          shuffle( $mainstring2 );

          $total= count( $mainstringnew ) + count( $suffix_list );

      }

      if(isset($_GET['one_word']) && $_GET['one_word'] == 'on' && $_GET['two_word'] == 'on' && $_GET['synonyms'] == 'off' && $_GET['rhyming'] == 'off' ){
        

        $mainstringnew = array_merge($mainstring, $suffix_list);
        shuffle($mainstringnew);
      
        $mainstringnew = array_merge($mainstringnew, $general_liststring);
      
        $mainstringnew = tlds_filter($mainstringnew);
      
        $mainstring2   = array_slice( $mainstringnew, $ofset, $limit, true );

        $total= count( $mainstringnew );
        
      }

      if(isset($_GET['one_word']) &&$_GET['one_word'] == 'off' && $_GET['two_word'] == 'on' && $_GET['synonyms'] == 'on' && $_GET['rhyming'] == 'off'){
        
      
        $mainstringnew = array_merge($mainstring, $synonyms_list);
        $mainstringnew = array_merge($mainstringnew, $general_liststring);
       
        $mainstringnew = tlds_filter($mainstringnew);
        $mainstring2 = array_slice( $mainstringnew, $ofset, $limit, true );

        $total= count( $mainstringnew );

      }

      if(isset($_GET['one_word']) && $_GET['one_word'] == 'off' && $_GET['two_word'] == 'off' && $_GET['synonyms'] == 'on' && $_GET['rhyming'] == 'off' ){

         
        $mainstringnew = $synonyms_list;
        $mainstringnew = tlds_filter($mainstringnew);
        $mainstring2   = array_slice( $mainstringnew, $ofset, $limit, true );

        $total= count( $mainstringnew );
        
      }

      if(isset($_GET['rhyming']) && $_GET['rhyming'] == 'on' && $_GET['one_word'] == 'off' && $_GET['two_word'] == 'off' && $_GET['synonyms'] == 'off' ){
          

          $mainstringnew = $rhyming_list;
          $mainstringnew = tlds_filter($mainstringnew);
          $mainstring2   = array_slice( $mainstringnew, $ofset, $limit, true );

          $total= count( $mainstringnew );

      }

      if(isset($_GET['rhyming']) && $_GET['rhyming'] == 'on' && $_GET['one_word'] == 'on' && $_GET['two_word'] == 'off' && $_GET['synonyms'] == 'off' ){
          
         
          $mainstringnew = array_merge($suffix_list, $rhyming_list);
          $mainstringnew = array_merge($mainstringnew, $general_liststring);
         
          $mainstringnew = tlds_filter($mainstringnew);
          $mainstring2   = array_slice( $mainstringnew, $ofset, $limit, true );

          $total= count( $mainstringnew );

      }

      if(isset($_GET['rhyming']) && $_GET['rhyming'] == 'on' && $_GET['one_word'] == 'on' && $_GET['two_word'] == 'on' && $_GET['synonyms'] == 'off' ){
         
         
          $mainstringnew = array_merge($suffix_list, $rhyming_list);
          $mainstringnew = array_merge($mainstringnew, $mainstring);
          $mainstringnew = array_merge($mainstringnew, $general_liststring);
          
          $mainstringnew = tlds_filter($mainstringnew);
          $mainstring2   = array_slice( $mainstringnew, $ofset, $limit, true );

          $total= count( $mainstringnew );
      }

      if(isset($_GET['rhyming']) && $_GET['rhyming'] == 'on' && $_GET['one_word'] == 'off' && $_GET['two_word'] == 'on' && $_GET['synonyms'] == 'off' ){

          $mainstringnew = array_merge($mainstring, $rhyming_list);
          $mainstringnew = array_merge($mainstringnew, $general_liststring);
        
          $mainstringnew = tlds_filter($mainstringnew);
          $mainstring2   = array_slice( $mainstringnew, $ofset, $limit, true );

          $total= count( $mainstringnew );

      }

      if( isset($_GET['bname']) && (!isset( $_GET['one_word'] ) && !isset( $_GET['two_word']) && !isset($_GET['synonyms']) && !isset($_GET['rhyming']) ) ){

        $mainstringnew = array_merge( $mainstring, $general_liststring );
        $mainstringnew = tlds_filter($mainstringnew);
 
        if( !empty($synonyms_list) ){
         
          $synonyms_list = tlds_filter($synonyms_list); 
          $suffix_list   = tlds_filter($suffix_list); 
         

          if( !wp_is_mobile() ){
            $limit = ($page >= 4) ? 72: 56;

            $synlimit = 20;
            $synofset = ( !empty($page) && $page !=1 ) ? ($page * $synlimit-$synlimit) :0;

            $suflimit = 20;
            $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
          }

          if( $detect->isTablet()){
            $limit = 52;

            $synlimit = 10;
            $synofset = ( !empty($page) && $page !=1 ) ? ( $page * $synlimit-$synlimit ) :0;

            $suflimit = 10;
            $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
          }

          if( $detect->isMobile() && !$detect->isTablet()){
            $limit = 10;

            $synlimit = 10;
            $synofset = ( !empty($page) && $page !=1 ) ? ( $page * $synlimit-$synlimit ) :0;

            $suflimit = 10;
            $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
          }

          $arr = array_slice( $synonyms_list, $synofset, $synlimit, true );
 
          $arr2 = array_slice( $suffix_list, $sufofset, $suflimit, true );

          if( count($arr2) == 0 ){
            $arr = array_slice( $synonyms_list, $sufofset, 24, true );
          }

          if( empty($arr) && empty($arr2)) { $limit = 96; }

          $arr3 = array_slice( $mainstringnew, $ofset, $limit, true );

          $mix = array_merge( $arr, $arr2 );

          $mainstring2 = array_merge( $arr3, $mix );

          shuffle( $mainstring2 );

          $total= count( $mainstringnew ) + count( $synonyms_list ) + count($suffix_list);
        
        }else{

          $suflimit = 20;
          $sufofset = ( !empty($page) && $page !=1 ) ? ( $page * $suflimit-$suflimit ) :0;
         
         
          $suffix_list  = tlds_filter($suffix_list);
          $suffix_listn = array_slice( $suffix_list, $sufofset, 20, true );

         
          $mix = array_merge( $suffix_listn, $mainstringnew );

          $mainstrin = array_slice( $mix, $ofset, $limit, true );

          $mainstring2 = array_slice( array_merge($mix, $mainstrin), $ofset, $limit, true );
          shuffle( $mainstring2 );

          $total= count( $mainstringnew ) + count($suffix_list);

        }

      }

     // $total= count( $mainstringnew );

      /*** code for new Ends ***/

    }else{
      $mainstringnew = tlds_filter($mainstringnew);
      $mainstring2 = array_slice( $mainstring, $ofset, $limit );
      $total = count( $mainstring );
    }

  }

  $totalFormat = ($total)? number_format($total):'0';


 
 function tlds_filter($array){

   if( isset($_GET['tlds']) && !empty($_GET['tlds']) ) {
     $tlds = $_GET['tlds'];
   }else{
     $tlds = 'com'; 
   }

  $domains_list = array_map(function($val) use($tlds) { return  strtolower($val).".".$tlds; ; }, $array);
  shuffle($domains_list);
  return $domains_list;
 
}