<?php

/**
*   @param array $industry
*   @param string $position
*   @param string $length
*   @param string $messages
*   @param array $general_liststring
**/

$industry = (isset($_GET['industry']) && !empty($_GET['industry'])) ? $_GET['industry']: '';
$position = (isset($_GET['position']) && !empty($_GET['position'])) ? $_GET['position']: '';
$length   = (isset($_GET['character']) && !empty($_GET['character'])) ? $_GET['character']: '';
$synonyms = (isset($_GET['synonyms']) && !empty($_GET['synonyms'])) ? $_GET['synonyms']: 'off';
$one_word = (isset($_GET['one_word']) && !empty($_GET['one_word'])) ? $_GET['one_word']: 'off';
$two_word = (isset($_GET['two_word']) && !empty($_GET['two_word'])) ? $_GET['two_word']: 'off';
$messages = '';
$general_liststring = array();
$suffix_list = array();
$synonyms_list = array();
$rhyming_list = array();

if( !function_exists('rhyming_words_list') ){
	function rhyming_words_list($search_word, $position, $length){
		global $wpdb;

		$list = array();
		$myrows='';
		$table = 'wp_eng_generator';

		$myrows = $wpdb->get_results("select * from $table", ARRAY_A);

		if( count(preg_split('/\s+/', $search_word))>=1 ){
			
			$word = explode(' ', $search_word);


			foreach ($word as $key => $value) {
			$char = mb_substr($value, 0, 1, "UTF-8");
				if( $myrows != 0 || $myrows != false) {
					foreach ($myrows as $word) {
						foreach($word as $key => $row ){
							if(strtolower($char) == strtolower(mb_substr($row, 0, 1, "UTF-8")) ){

								if( $length < 15 && ( strlen($row) == $length || strlen($row) < $length ) ){
									
									$result = ($position === 'after') ? ucwords($row.' '.$value) : ucwords($value.' '.$row);
							        
							        if(!in_array($result, $list)){
                                 	  array_push($list, $result);

							        } 

									// $list[] = ($position === 'after') ? ucwords($row.' '.$value) : ucwords($value.' '.$row);
								}
								if( $length == 15 && ( strlen($row) < $length || strlen($row) > $length ) ){
									
									$result = ($position === 'after') ? ucwords($row.' '.$value) : ucwords($value.' '.$row);
							        
							        if(!in_array($result, $list)){
                                 	  array_push($list, $result);

							        } 

									// $list[] = ($position === 'after') ? ucwords($row.' '.$value) : ucwords($value.' '.$row);
								}
								if( $length == ''){
									
                                    $result = ($position === 'after') ? ucwords($row.' '.$value) : ucwords($value.' '.$row);
							        
							        if(!in_array($result, $list)){
                                 	  array_push($list, $result);

							        } 
                                     
									// $list[] = ($position === 'after') ? ucwords($row.' '.$value) : ucwords($value.' '.$row);
								}
							}
						}
					}
				}
			}
		}

		shuffle($list);

		return $list;
	}
}

$rhyming_list = rhyming_words_list($search_word, $position, $length);

if( !function_exists('synonyms_words_list') ){
	function synonyms_words_list($search_word, $position, $length){
		global $wpdb;

		$list = array();
		$temp = array();
		$temp_word = array();
		$word_for_synonyms = array();
		$mainstringN = array();
		$myrows = '';
		$table = 'wp_synonym';

		$listcolumns = $wpdb->get_results("SELECT COLUMN_NAME 
			FROM INFORMATION_SCHEMA.COLUMNS 
			WHERE TABLE_NAME = '$table'", ARRAY_A);
		
		foreach ($listcolumns as $key => $value) {
			array_push($temp, $value['COLUMN_NAME']);
		}

		if( count(preg_split('/\s+/', $search_word))>=1 ){
			$word = explode(' ', $search_word);
			foreach ($word as $key => $value) {
				$k = array_search(strtolower($value), $temp);
				if( $temp[$k] && $k !=0 ){
					array_push($temp_word, $temp[$k]);
				}
			}
		}

		if( !empty($temp_word) ){
			if(count($temp_word)>1){
				
				$wordcol = implode(',', $temp_word);
				$myrows = $wpdb->get_results("select $wordcol from $table", ARRAY_A);
			}else{
				$wordcol = $temp_word[0];
				$myrows = $wpdb->get_results("select $wordcol from $table", ARRAY_A);
			}
		}

		if( $myrows != 0 || $myrows != false) {
			foreach ($myrows as $word) {
				foreach($word as $key => $row ){
					array_push($list, $row);
				}
			}
		}

		$alldata = $wpdb->get_results("select * from `wp_eng_generator`", ARRAY_A);

  		foreach ($list as $keword) {
  			if($keword){
		        foreach( $alldata as $key => $row ){
		        	foreach( $row as $in => $value ){
		        		if($in == 'id'){ continue;}
	        			if( !empty($value) ){

	        				if($length < 15 && (strlen($value) == $length || strlen($value) < $length) ){
	        					
	        					$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
	        				}

	        				if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
	        					
	        					$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
	        				}
	        				
	        				if(!$length){
	        					$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);

	        				}
	        			}
	        		}
	        	}
        	}
  		}

      	shuffle($mainstringN);

		return $mainstringN;
	}
}

$synonyms_list = synonyms_words_list($search_word, $position, $length);

function remove_word($keword){

	if( substr($keword, -2) === 'sh' ){
		return substr_replace($keword, "",-2);
	}else if( substr($keword, -2) === 'ic' ){
		return substr_replace($keword, "",-2);
	}else if( substr($keword, -2) === 'es' ){
		return substr_replace($keword, "",-2);
	}else if( substr($keword, -2) === 'al' ){
		return substr_replace($keword, "",-2);
	}
	// else if( substr($keword, -3) == 'ing' ){
	// 	return substr_replace($keword, "",-3);
	// }
	else if( substr($keword, -3) == 'ion' ){
		return substr_replace($keword, "",-3);
	}else if( substr($keword, -4) == 'ment' ){
		return substr_replace($keword, "",-4);
	}else if( 'beauty' == strtolower($keword) ){
		return 'beaut';
	}else if( 'technology' == strtolower($keword)){
		return 'tech';
	}else if( 'fitness' == strtolower($keword) ){
		return 'fit';
	}else if( 'luxury' == strtolower($keword) ){
		return 'lux';
	}else{
		return $keword;
	}

}

function check_for_suffix($keword){
	if( substr($keword, -2) === 'sh' ){
		return true;
	}else if( substr($keword, -2) === 'ic' ){
		return true;
	}else if( substr($keword, -2) === 'es' ){
		return true;
	}else if( substr($keword, -2) === 'al' ){
		return true;
	}
	// else if( substr($keword, -3) == 'ing' ){
	// 	return true;
	// }
	else if( substr($keword, -3) == 'ion' ){
		return true;
	}else if( substr($keword, -4) == 'ment' ){
		return true;
	}else if( 'beauty' == strtolower($keword) ){
		return true;
	}else if( 'technology' == strtolower($keword) ){
		return true;
	}else if( 'fitness' == strtolower($keword)  ){
		return true;
	}else if( 'luxury' == strtolower($keword)  ){
		return true;
	}else{
		return false;
	}
}

if( !function_exists('suffix_words_list')){
	function suffix_words_list( $search_word, $length ) {
		global $wpdb;

		$list = array();
		$myrows = '';

		if( ( !empty($_GET['one_word']) && $_GET['one_word'] == 'on') && $_GET['synonyms'] == 'on' && $_GET['two_word'] == 'off' && $_GET['rhyming'] == 'off'){

			$lists = array();
			$temp = array();
			$temp_word = array();
			$word_for_synonyms = array();
			$myrows = '';
			$table = 'wp_synonym';

			$listcolumns = $wpdb->get_results("SELECT COLUMN_NAME 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_NAME = '$table'", ARRAY_A);
			
			foreach ($listcolumns as $key => $value) {
				array_push($temp, $value['COLUMN_NAME']);
			}

			if( count(preg_split('/\s+/', $search_word))>=1 ){
				$word = explode(' ', $search_word);
				foreach ($word as $key => $value) {
					$k = array_search(strtolower($value), $temp);
					if( $temp[$k] && $k !=0 ){
						array_push($temp_word, $temp[$k]);
					}
				}
			}

			if( !empty($temp_word) ){

				if( count($temp_word)>1 ){
					$wordcol = implode(',', $temp_word);
					$res = $wpdb->get_results("select $wordcol from $table", ARRAY_A);
				}else{
					$wordcol = $temp_word[0];
					$res = $wpdb->get_results("select $wordcol from $table", ARRAY_A);
				}
			

				if( $res != 0 || $res != false ) {
					foreach ($res as $word) {
						foreach($word as $key => $row ){
							array_push($lists, $row);
						}
					}
				}
			}
		}

		$myrows = $wpdb->get_results("select suffix from `wp_eng_generator`", ARRAY_A);

		if( count(preg_split('/\s+/', $search_word)) >= 1 ){

			$word = explode(' ', $search_word);

			$words = ( !empty($lists) ) ? array_merge( $lists, $word ) : $word;

			foreach ($words as $key => $value) {
				if($value){
					if( ($myrows != 0 || $myrows != false) && check_for_suffix($value) ) {
						$keword = remove_word($value);

						foreach ($myrows as $word) {
							if( $word['suffix'] == '') continue;

							$list[] = ucwords($keword.''.strtolower($word['suffix']));
						}

					}else{
						foreach ($myrows as $word) {
							foreach($word as $key => $row ){
								if(!$row) continue;
								
								$list[] = ucwords($value.''.strtolower($row));
							}
						}
					}
				}
			}
		}

		shuffle($list);

		$temp = array();

		foreach ($list as $key => $value) {
			if( $length < 15 && ( strlen($value) == $length || strlen($value) < $length ) ){
				array_push($temp, $value);
			}
			if( $length == 15 && ( strlen($value) < $length || strlen($value) > $length ) ){
				array_push($temp, $value);
			}
			if( $length == ''){
				array_push($temp, $value);
			}
		}

		$list = $temp;
		return $list;
	}
}

$suffix_list = suffix_words_list( $search_word, $length );

/**
*	function domains_list
* 	show available related domains
*
*   @param string $search_word industry filter
*   @return array domains list
**/

if( !function_exists('domains_list') ):

	function domains_list( $search_word ){

		global $wpdb;
        $exist_ids = [];    
        $indusrows = [];
        $firstletterows = [];
        // New Code 4 September 2020

		if( isset($_GET['bname']) && preg_match('#^[a-z0-9\s]+$#i', $_GET['bname']) )
		{

			
				$search_key = $_GET['bname'];
				$search_key = mb_convert_encoding($search_key, 'UTF-8', 'Windows-1252');
                
                // For Get Entryies By Title Start //
				$condition = '';
				
				$condition = ' price < 4000) AND ( title LIKE "%'.$search_key.'%") AND status = "1"';
               
				$condition .= ' ORDER BY price DESC ';

				$titlerows = $wpdb->get_results("SELECT DISTINCT `id`, `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A);

                if($titlerows){
                	foreach ($titlerows as $row) {
                		  array_push($exist_ids, $row['id']);
                	}
                }
                // End //
                
        		// For Get Entryies By Industries Start //
        		if( is_array($search_word) ){
                 
                  if( count($search_word)>1 ){
                        
        				$words = $search_word;
        				$condition = '';

        				foreach ($words as $value) {
        					if ( end($words) != $value ) {
        						$condition .= ' price < 4000) AND (title LIKE "%'.$value.'%" OR tags LIKE "%'.$value.'%" OR categories LIKE "%'.$value.'%" OR';
        					}else{
        					    $condition .= ' title LIKE "%'.$value.'%" OR tags LIKE "%'.$value.'%" OR categories LIKE "%'.$value.'%") AND status = "1"';

        					    if($exist_ids)
        					    	$condition .= ' AND id NOT IN('.implode(',', $exist_ids).') '; 

        					    $condition .= ' ORDER BY price DESC ';
        					}
        				}

        				$indusrows = $wpdb->get_results("SELECT DISTINCT `id`,`title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE ($condition", ARRAY_A);
        				
        			} else {

        				$condition = '';
        				
        				$condition .= 'price < 4000) AND (title LIKE "%'.$search_word[0].'%" OR tags LIKE "%'.$search_word[0].'%" OR categories LIKE "%'.$search_word[0].'%") AND status = "1"';
                       
                        if($exist_ids)
                        	$condition .= ' AND id NOT IN('.implode(',', $exist_ids).') '; 

                        $condition .= ' ORDER BY price DESC ';

        				$indusrows = $wpdb->get_results("SELECT DISTINCT `id`,`title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A);

        			}
                     
                   if($indusrows){
                   	foreach ($indusrows as $row) {
                   		array_push($exist_ids, $row['id']);
                   	}
                   } 
              	}
                // End //

                  

                // For Get Entryies By Categories Start //
                $condition = ''; 
                
                $condition .= ' price < 4000) AND ( categories LIKE "%'.$search_key.'%") AND status = "1"'; 
			     
			    if($exist_ids)
			    	$condition .= ' AND id NOT IN('.implode(',', $exist_ids).') '; 

			    $condition .= ' ORDER BY price DESC ';

			    $catrows = $wpdb->get_results("SELECT DISTINCT `id`, `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A);

                if($catrows){
                	foreach ($catrows as $row) {
                		array_push($exist_ids, $row['id']);
                	}
                }
                // End // 

                
                // For Get Entryies By Tags Start //
				$condition = ''; 
				
				$condition .= ' price < 4000) AND ( tags LIKE "%'.$search_key.'%") AND status = "1"'; 
                
                if($exist_ids)
                	$condition .= ' AND id NOT IN('.implode(',', $exist_ids).') '; 

                $condition .= ' ORDER BY price DESC ';

				$tagrows = $wpdb->get_results("SELECT DISTINCT `id`, `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A); 

				if($tagrows){
					foreach ($tagrows as $row) {
						array_push($exist_ids, $row['id']);
					}
				}
                // End //
			    
			    
                

                 
                // For Get Entryies By Static As a dynamically Start // 
				$condition = '';
                
                $condition .= ' price < 4000) AND categories LIKE "%Brandable%" AND status = "1" ';
               
                if($exist_ids)
                	$condition .= ' AND id NOT IN('.implode(',', $exist_ids).') ';
                 
               
               	$condition .= ' ORDER BY price DESC ';
               
                $myrows = $wpdb->get_results(" SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A );
                 
                // End //  
                
                
             

				$domainslist  = array_merge( $titlerows,$indusrows,$catrows,$tagrows,$myrows);
             
				
				$paged        = explode('/', $_SERVER['REDIRECT_URL']);
				if($paged[2] > 0){
					shuffle( $domainslist );
				}
			  	return $domainslist;
				
		} 

		// $domainslist = array();
		// $myrows='';

		// if( is_array($search_word) ){
		// 	if( count($search_word)>1 ){

		// 		$words = $search_word;
		// 		$condition = '';

		// 		foreach ($words as $value) {
		// 			if ( end($words) != $value ) {
		// 				$condition .= ' price < 4000) AND (title LIKE "%'.$value.'%" OR short_description LIKE "%'.$value.'%" OR tags LIKE "%'.$value.'%" OR categories LIKE "%'.$value.'%" OR';
		// 			}else{
		// 			$condition .= ' title LIKE "%'.$value.'%" OR short_description LIKE "%'.$value.'%" OR tags LIKE "%'.$value.'%" OR categories LIKE "%'.$value.'%") AND status = "1" ORDER BY price DESC';
		// 			}
		// 		}

		// 		$myrows = $wpdb->get_results("SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE ($condition", ARRAY_A);
				
		// 	} else {

		// 		$condition = '';
				
		// 		$condition .= 'price < 4000) AND (title LIKE "%'.$search_word[0].'%" OR short_description LIKE "%'.$search_word[0].'%" OR tags LIKE "%'.$search_word[0].'%" OR categories LIKE "%'.$search_word[0].'%") AND status = "1" ORDER BY price DESC';

		// 		$myrows = $wpdb->get_results("SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A);

		// 	}
		// }
		// else{

		// 	$condition = '';
			
		// 	$condition .= 'price < 4000) AND ( title LIKE "%'.$search_word.'%" OR short_description LIKE "%'.$search_word.'%" OR tags LIKE "%'.$search_word.'%" OR categories LIKE "%'.$search_word.'%") AND status = "1" ORDER BY price DESC';


		// 	$myrows = $wpdb->get_results("SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A);

		// }


		// if( $myrows == 0 || $myrows == false ) {

		// 	$condition = '';

		// 	$condition .= ' price < 4000) AND categories LIKE "%Brandable%" AND status = "1" ORDER BY price DESC';

		//     $myrowsres = $wpdb->get_results(" SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A );
		// }

		// if( $myrows != 0 || $myrows != false || count($myrows)< 8 ) {

		// 	$condition = '';

		// 	$condition .= ' price < 4000) AND categories LIKE "%Brandable%" AND status = "1" ORDER BY price DESC';

		//     $myrowsres = $wpdb->get_results(" SELECT DISTINCT `title`, `price`, `featured_src`, `permalink`, `price_html`, `alt` FROM `domains` WHERE (".$condition."", ARRAY_A );

		// }

		// if( !empty($myrowsres) && count($myrows)< 8 && !empty($myrows) ){
		// 	return $domainslist = array_merge( $myrows, $myrowsres );
		// }else if( !empty($myrowsres) && empty($myrows) ){
		// 	return $domainslist = $myrowsres;
		// }else{
		// 	return $domainslist = $myrows;
		// }
	}

endif;

/**
*	function general_list
* 	generates the general list keywords
*
*   @param string $search_word
*   @param string $position
*   @return array general list
**/
if(!function_exists('general_list')):

		function general_list($search_word, $position, $length)
		{
			global $wpdb;
	        $mainstringN = array();
	        $start = 0;
	        $myrows = $wpdb->get_results("select general_list from `wp_eng_generator` ", ARRAY_A);

	        if( count(preg_split('/\s+/', $search_word) ) > 1 ){
		        	$kewords = explode(' ', $search_word);

		        	foreach ($kewords as $keword) {
		        		$keword = serach_word_senitize( $keword );

    			        foreach( $myrows as $key => $row ){
    			        	foreach( $row as $in => $value ){
    		        			if( !empty($value) ){

    		        				if($length < 15 && (strlen($row[$in]) == $length || strlen($row[$in]) < $length ) ){
										$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    		        				}

    		        				if($length == 15 && (strlen($row[$in]) == $length || strlen($row[$in]) > $length || strlen($row[$in]) < $length)){
										$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    		        				}

    		        				if(!$length){
    		        					$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    		        				}
    		        			}
    		        		}
    			        }
		        	}
		        }else{
		        	$keword = serach_word_senitize( $search_word );
		        	
    		        foreach( $myrows as $key => $row ){
    		        	foreach( $row as $in => $value ){
    	        			if( !empty($value) ){
    	        				if($length < 15 && (strlen($row[$in]) == $length || strlen($row[$in]) < $length ) ){
    	        					$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    	        				}

    	        				if($length == 15 && (strlen($row[$in]) == $length || strlen($row[$in]) > $length || strlen($row[$in]) < $length)){
    	        					$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    	        				}

    	        				if(!$length){
    	        					$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    	        				}

    	        			}
    	        		}
    		        }
		        }

	        return $mainstringN;
		}

	endif;

if($numkey >= '1'){
	if( !empty($search_word) && is_array($industry) && count($industry) == 1 ){
		$mainstring=array();

		/**
  		*	Instance 1) The [insertgenerator] shortcode has one Industry filters defined. 
    	*	Skiped keyword match function and display results page with the industry filter outlined in shortcode.
    	*
		*   @param string $search_word
		*   @param array $industry
		*   @param string $position
		*   @param string $length
		**/

		if(!function_exists('single_filter_wordgen')):
			function single_filter_wordgen($search_word, $industry, $position, $length)
			{
				global $wpdb;
		        $mainstringN=array();
		        $col = '';
		        $start = 0;
                $industry[count($industry) - 1] = ( end($industry) == "Real Estate" ) ? "realestate" : $industry[count($industry) - 1];
		        foreach ($industry as $value) {
		        	
		        	$value = ($value == "Real Estate") ? "realestate" : $value;
		        	$col = trim( strtolower($value) );
		        }
                
		        $myrows = $wpdb->get_results("select $col from `wp_eng_generator`", ARRAY_A);

		        if($myrows !== 0 || $myrows !== false){
		        	$message = __("We've applied industry filters based on your keywords. Add or Remove filters to get more relevant results.", 'thegem-child');

        			/**
        	  		*	Display Alert/ERROR message
        	    	*
        			*   @param string $type eg. 'danger' for error, 'info' for change or action,
        			*   'success' for successful, 'warning' for attention
        			*   
        			*   @param string $message
        			**/
        			if(!isset($_GET['fltr'])):
						alert_message('success', $message);
					endif;
		        }

		      

		        /**
		        *	Instance 1) Joining the Search keywords with filters
		        *	
		        **/

		        /**
		        * @param $search_word more than one
		        **/
		        if( count( preg_split('/\s+/', $search_word) )> 1 ){
		        	$kewords = explode(' ', $search_word);

    	        	if($col == 'suffix'){
    		        	if( substr($keword, -2) === 'es' ){
    		        		$keword = substr_replace($keword, "",-2);
    		        	}
    		        	else if( substr($keword, -3) == 'ing' ){
    		        		//$keword = substr_replace($keword ,"",-3);
    		        		$keword = $keword;
    		        	}else{
    						$keword = $search_word;
    					}
    	        	}

		        	foreach ($kewords as $keword) {
		        		foreach( $myrows as $row ){
		        			if( !empty($row[$col]) ){

		        				/**
		        				* charaters count filters length less or equal to 15
		        				**/
		        				if($length < 15 && (strlen($row[$col]) == $length || strlen($row[$col]) < $length ) ){
		        					if($col == 'suffix'){
		        						$mainstringN[] = ucwords($keword.''.strtolower($row[$col]));
		        					}else{
		        						$mainstringN[] = ($position === 'after') ? ucwords( $row[$col].' '.$keword ): ucwords( $keword.' '.$row[$col] );
		        					}
		        				}

		        				/**
		        				* charaters count filters lenght more than 15
		        				**/
		        				if($length == 15 && (strlen($row[$col]) == $length || strlen($row[$col]) > $length || strlen($row[$col]) < $length)){
		        					if($col == 'suffix'){
		        						$mainstringN[] = ucwords($keword.''.strtolower($row[$col]));
		        					}else{
		        						$mainstringN[] = ($position === 'after') ? ucwords( $row[$col].' '.$keword ): ucwords( $keword.' '.$row[$col] );
		        					}
		        				}

		        				/**
		        				* if charaters count filters not applied default
		        				**/
		        				if(!$length){
		        					if($col == 'suffix'){
		        						$mainstringN[] = ucwords($keword.''.strtolower($row[$col]));
		        					}else{
		        						$mainstringN[] = ($position === 'after') ? ucwords($row[$col].' '.$keword): ucwords($keword.' '.$row[$col]);
		        					}
		        				}
		        			}
		        		}
		        	}
		        }else{

		        /**
		        * @param $search_word equals to one
		        **/
		        	$keword = $search_word;
		        	if($col == 'suffix'){
			        	if( substr($keword, -2) === 'es' ){
			        		$keword = substr_replace($keword, "",-2);
			        	}
			        	else if( substr($keword, -3) == 'ing' ){
			        		//$keword = substr_replace($keword ,"",-3);
			        		$keword = $keword;
			        	}else{
							$keword = $search_word;
						}
		        	}

		        	foreach( $myrows as $row ){
		        		if( !empty($row[$col]) ){

		        			if($length < 15 && (strlen($row[$col]) == $length || strlen($row[$col]) < $length ) ){
		        				if($col == 'suffix'){
		        					$mainstringN[] = ucwords($keword.''.strtolower($row[$col]));
		        				}else{
		        					$mainstringN[] = ($position === 'after') ? ucwords( $row[$col].' '.$keword ): ucwords( $keword.' '.$row[$col] );
		        				}
		        			}

		        			if($length == 15 && (strlen($row[$col]) == $length || strlen($row[$col]) > $length || strlen($row[$col]) < $length)){
		        				if($col == 'suffix'){
		        					$mainstringN[] = ucwords($keword.''.strtolower($row[$col]));
		        				}else{
		        					$mainstringN[] = ($position === 'after') ? ucwords( $row[$col].' '.$keword ): ucwords( $keword.' '.$row[$col] );
		        				}
		        			}

		        			if(!$length){

	        					if($col == 'suffix'){
	        						$mainstringN[] = ucwords($keword.''.strtolower($row[$col]));
	        					}else{
	        						$mainstringN[] = ($position === 'after') ? ucwords( $row[$col].' '.$keword ): ucwords( $keword.' '.$row[$col] );
	        					}

		        			}
		        		}
		        	}
		        }
		        shuffle($mainstringN);
		       	
		        return $mainstringN;
			}
		endif;
		$mainstring = single_filter_wordgen($search_word, $industry, $position, $length);

		$general_liststring = general_list($search_word, $position, $length);

		$domains = domains_list( $industry );

		
		
	}


	/**
	*	Instance 2) The [insertgenerator] shortcode has more than one Industry filters defined. 
	*	Skiped keyword match function and display results page with the industry filter outlined in shortcode.
	*
	*   @param string $search_word
	*   @param string $industry
	*   @param string $position
	*   @param string $length
	**/

	if( !empty($search_word) && is_array($industry) && count($industry) > 1 ){

		$mainstring=array();
		if(!function_exists('multi_filter_wordgen')):
			function multi_filter_wordgen($search_word, $industry, $position, $length)
			{
				global $wpdb;
		        $mainstringN=array();
		        $col = '';
		        $start = 0;

		        $industry[count($industry) - 1] = ( end($industry) == "Real Estate" ) ? "realestate" : $industry[count($industry) - 1];
		        foreach ($industry as $value) {
		        	
		        	$value = ($value == "Real Estate") ? "realestate" : $value;

		        	if ( end($industry) != $value) {
		        		$col .= trim( strtolower($value) ).', ';
		        	}else{
		        		$col .= trim( strtolower($value) );
		        	}
		        }

		        $myrows = $wpdb->get_results("select $col from `wp_eng_generator`", ARRAY_A);

		        if($myrows !== 0 || $myrows !== false){
		        	$message = __("We've applied industry filters based on your keywords. Add or Remove filters to get more relevant results.", 'thegem-child');

		        	/**
        	  		*	Display Alert/ERROR message
        	    	*
        			*   @param string $type eg. 'danger' for error, 'info' for change or action,
        			*   'success' for successful, 'warning' for attention
        			*   
        			*   @param string $message
        			**/
		        	if(!isset($_GET['fltr'])):
						alert_message('success', $message);
					endif;
		        }


		        /**
		        *	Instance 2) Joining the Search keywords with filters
		        *	
		        **/
		        if( count( preg_split('/\s+/', $search_word) )> 1 ){
		        	$kewords = explode(' ', $search_word);
		        	foreach ($kewords as $keword) {
    			        foreach( $myrows as $key => $row ){
    			        	foreach( $row as $column => $value ){
    		        			if( !empty($value) ){

    		        				if( $length < 15 && (strlen($value) == $length || strlen($value) < $length) ){

    		        					if($column == 'suffix'){
    		        						if( substr($keword, -2) === 'es' ){
    		        							$newkeword = substr_replace($keword, "",-2);
    		        						}
											else if( substr($keword, -3) == 'ing' ){
    		        							//$newkeword = substr_replace($keword ,"",-3);
    		        							$newkeword = $keword;
    		        						}else{
    		        							$newkeword = $keword;
    		        						}
    		        						
    		        						$mainstringN[] = ucwords($newkeword.''.strtolower($value));

    		        					}else{
    		        						
    		        						$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
    		        						
    		        					}

    		        				}
    		        				if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
    		        					if($column == 'suffix'){
    		        						if( substr($keword, -2) === 'es' ){
    		        							$newkeword = substr_replace($keword, "",-2);
    		        						}
											else if( substr($keword, -3) == 'ing' ){
    		        							//$newkeword = substr_replace($keword ,"",-3);
    		        							$newkeword = $keword;
    		        						}else{
    		        							$newkeword = $keword;
    		        						}
    		        						
    		        						$mainstringN[] = ucwords($newkeword.''.strtolower($value));

    		        						
    		        					}else{
    		        						
    		        						$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
    		        						
    		        					}
    		        				}

    		        				if(!$length){

    		        					if($column == 'suffix'){
    		        						if( substr($keword, -2) === 'es' ){
    		        							$nkeword = substr_replace($keword, "",-2);
    		        						}
    		        						else if( substr($keword, -3) == 'ing' ){
    		        							//$nkeword = substr_replace($keword ,"",-3);
    		        							$nkeword = $keword;
    		        						}else{
    		        							$nkeword = $keword;
    		        						}
    		        						
    		        						$mainstringN[] = ucwords($nkeword.''.strtolower($value));
    		        					}else{
    		        						
    		        						$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    		        					}
    		        					
    		        				}
    		        			}
    		        		}
    			        }
		        	}
		        }else{
		        	$keword = $search_word;
    		        foreach( $myrows as $key => $row ){
    		        	foreach( $row as $column => $value ){
    	        			if( !empty($value) ){

    	        				if( $length < 15 && (strlen($value) == $length || strlen($value) < $length) ){
    	        					if($column == 'suffix'){
    	        						if( substr($keword, -2) === 'es' ){
    	        							$keword = substr_replace($keword, "",-2);
    	        						}

    	        						if( substr($keword, -3) == 'ing' ){
    	        							//$keword = substr_replace($keword ,"",-3);
    	        							$keword = $keword;
    	        						}
    	        						
    	        						$mainstringN[] = ucwords($keword.''.strtolower($value));
    	        					}else{

    	        						$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
    	        					}

    	        				}

    	        				if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
    	        					if($column == 'suffix'){
    	        						if( substr($keword, -2) === 'es' ){
    	        							$keword = substr_replace($keword, "",-2);
    	        						}

    	        						if( substr($keword, -3) == 'ing' ){
    	        							//$keword = substr_replace($keword ,"",-3);
    	        							$keword = $keword;
    	        						}
    	        						
    	        						$mainstringN[] = ucwords($keword.''.strtolower($value));
    	        					}else{
    	        							
    	        						$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
    	        					}

    	        				}

    	        				if(!$length){
    	        					if($column == 'suffix'){
    	        						if( substr($keword, -2) === 'es' ){
    	        							$keword = substr_replace($keword, "",-2);
    	        						}

    	        						if( substr($keword, -3) == 'ing' ){
    	        							//$keword = substr_replace($keword ,"",-3);
    	        							$keword = $keword;
    	        						}
    	        						
    	        						$mainstringN[] = ucwords($keword.''.strtolower($value));
    	        					}else{
    	        						$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    	        					}
    	        				}
    	        			}
    	        		}
    		        }
		        }

		        shuffle($mainstringN);
		       	
		        return $mainstringN;
			}
		endif;
		$mainstring = multi_filter_wordgen($search_word, $industry, $position, $length);

		$general_liststring = general_list($search_word, $position, $length);
		$domains = domains_list( $industry );

		
	}

	/**
	*	Instance 3) There is no industry filter applied in [insertgenerator] shortcode
	*	Keyword match found and display results page with all industry filters.
	*   @param string $search_word
	*   @param string $industry
	*   @param string $position
	*   @param string $length
	**/

	if( !empty($search_word) && empty($industry) &&  preg_match('#^[a-z0-9\s]+$#i', $search_word)  ){

		$mainstring=array();
		if(!function_exists('all_wordgen')):
			function all_wordgen($search_word, $industry, $position, $length)
			{
				global $wpdb;
		        $mainstringN=array();
		        $start = 0;
		        $columns = array();

		      if(!isset($_GET['fltr'])):

		        $columsname = $wpdb->get_results("SELECT column_name FROM information_schema.columns WHERE table_name = 'wp_eng_generator'", ARRAY_A);

		        /**
		        *	Instance 3) If there are multiple serach keywords
		        **/
		        $nsearch_word = '';
		        if( strpos($search_word, ',') ){
		        	$nsearch_word = str_replace(',', ' ', $search_word);
		        }
		        
		        if( count( preg_split('/\s+/', $search_word) )> 1 ){
		        	$query_word='';
		        	$kewords = array_filter(explode(' ', $search_word));
		        	$kewords = array_unique($kewords);
		        	foreach ($kewords as $value) {
		        		if ( end($kewords) != $value) {
		        			$query_word .= "keyword ='".ucfirst($value)."' OR ";
		        		}else{
		        			$query_word .= "keyword ='".ucfirst($value)."'";
		        		}
		        	}

					$mycols = $wpdb->get_results("select industry from `wp_matching_list` where ".$query_word."", ARRAY_A);
					// echo "select industry from `wp_matching_list` where ".$query_word;
		    	}else{
		        	$mycols = $wpdb->get_results("select industry from `wp_matching_list` where keyword ='".$search_word."' AND industry !=''", ARRAY_A);
		    	}
		        if($mycols !== 0 || $mycols !== false){

		        	foreach( $mycols as $key => $col ){
		        		if(!empty($col)){
		        			$arr = explode('/', strtolower($col['industry']) );
		        		}

			        	foreach( $columsname as $key => $column ){
			        		if( in_array($column['column_name'], $arr) ){
			        			array_push($columns, $column['column_name']);
			        		}
			        	}
		        	}
		        	unset($_SESSION['filterd_columns']);

		        	// unset($columns[array_search('luxury', $columns)]);
		        	// unset($columns[array_search('photography', $columns)]);

		        	reset($columns);

		        	$_SESSION['filterd_columns'] = array_unique($columns);
		        	
					$columns = (count($columns)>1 AND is_array($columns))? implode(',', $columns) : current($columns);
		        }
		        if( strpos($columns, ',') || $columns !='' || is_string($columns)){
		        	$myrows = $wpdb->get_results("select ".$columns." from `wp_eng_generator` ", ARRAY_A);

		        	if($myrows !== 0 || $myrows !== false){

		        		
		        		$message = __("We've applied industry filters based on your keywords. Add or Remove filters to get more relevant results.", 'thegem-child');

			        	/**
	        	  		*	Display Alert/ERROR message
	        	    	*
	        			*   @param string $type eg. 'danger' for error, 'info' for change or action,
	        			*   'success' for successful, 'warning' for attention
	        			*   
	        			*   @param string $message
	        			**/
	        			unset($_SESSION['messages']);
	        			if(!isset($_GET['fltr'])): //checking result page
							alert_message('success', $message);
						endif;
		        	}

		        }

		        else{
		        	/**
		        	*	Instance 3) There is no industry filter applied in [insertgenerator] shortcode
		        	*	Apply all filters to the keywords and displaying
		        	**/
		        	unset($_SESSION['filterd_columns']);
		        	$myrows = $wpdb->get_results("select * from `wp_eng_generator` ", ARRAY_A);

		        	if($myrows !== 0 || $myrows !== false){
		        		$message = __('Select relevant industry filters to get better name ideas!', 'thegem-child');

	        			if(!isset($_GET['fltr'])):
							alert_message('success', $message);
						endif;
		        	}
		        }

		      else:
		      	unset($_SESSION['filterd_columns']);
		        	$myrows = $wpdb->get_results("select * from `wp_eng_generator` ", ARRAY_A);

		        	if($myrows !== 0 || $myrows !== false){
		        		$message = __('Select relevant industry filters to get better name ideas!', 'thegem-child');

	        			if(!isset($_GET['fltr'])):
							alert_message('success', $message);
						endif;
		        	}
		      endif;

		      	
		        /**
		        *	Instance 3) Joining the Search keywords with filters
		        **/
                

		        if( count( preg_split('/\s+/', $search_word) )> 1 ){
		        	$kewords = explode(' ', $search_word);
		        	foreach ($kewords as $keword) {

		        		if( $keword ){

			        		$keword = serach_word_senitize( $keword );

	    			        foreach( $myrows as $key => $row ){

	    			        	foreach( $row as $in => $value ){

	    			        		if($in == 'id'){ continue; }

	    		        			if( $value !== '' ){

	    		        				if($length < 15 && (strlen($value) == $length || strlen($value) < $length) ){
	    		        					if($in == 'suffix'){
	    		        						
	    		        						if( substr($keword, -2) == 'es' ){
	    		        							$keword = substr_replace($keword ,"",-2);
	    		        							$mainstringN[] = ucwords( $keword.''.$value );
	    		        						}

	    		        						if( substr($keword, -3) == 'ing' ){
	    		        							//$keword = substr_replace($keword ,"",-3);
	    		        							$keword = $keword;
	    		        							$mainstringN[] = ucwords( $keword.''.$value );
	    		        						}
	    		        					}else{
	    		        						$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
	    		        					}
	    		        				}

	    		        				if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
	    		        					if($in == 'suffix'){
	    		        						
	    		        						if( substr($keword, -2) == 'es' ){
	    		        							$keword = substr_replace($keword ,"",-2);
	    		        							$mainstringN[] = ucwords( $keword.''.$value );
	    		        						}

	    		        						if( substr($keword, -3) == 'ing' ){
	    		        							//$keword = substr_replace($keword ,"",-3);
	    		        							$keword = $keword; 
	    		        							$mainstringN[] = ucwords( $keword.''.$value );
	    		        						}
	    		        					}else{
	    		        						$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
	    		        					}
	    		        				}
	    		        				
	    		        				if(!$length){
	    		        					$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);

	    		        				}
	    		        			}
	    		        		}
	    			        }
		        		}
		        	}
		        }else{
                  	$keword = serach_word_senitize( $search_word );
		        	
    		        foreach( $myrows as $key => $row ) {
    		        	foreach( $row as $in => $value ) {
    		        		if( $in == 'id' ){ continue; }

    	        			if( !empty($value) ){

    	        				if($length < 15 && (strlen($value) == $length || strlen($value) < $length) ){
    	        					if($in == 'suffix'){
    	        						
    	        						if( substr($keword, -2) == 'es' ){
    	        							$keword = substr_replace($keword ,"",-2);
    	        							$mainstringN[] = ucwords( $keword.''.$value );
    	        						}

    	        						if( substr($keword, -3) == 'ing' ){
    	        							//$keword = substr_replace($keword ,"",-3);
    	        							$keword = $keword;
    	        							$mainstringN[] = ucwords( $keword.''.$value );
    	        						}
    	        					}else{

    	        						$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
    	        					}
    	        					
    	        				}

    	        				if($length == 15 && (strlen($value) == $length || strlen($value) > $length || strlen($value) < $length)){
    	        					
    	        					if($in == 'suffix'){
    	        						
    	        						if( substr($keword, -2) == 'es' ){
    	        							$keword = substr_replace($keword ,"",-2);
    	        							$mainstringN[] = ucwords( $keword.''.$value );
    	        						}

    	        						if( substr($keword, -3) == 'ing' ){
    	        							//$keword = substr_replace($keword ,"",-3);
    	        							$keword = $keword;
    	        							$mainstringN[] = ucwords( $keword.''.$value );
    	        						}
    	        					}else{

    	        						$mainstringN[] = ($position === 'after') ? ucwords( $value.' '.$keword ): ucwords( $keword.' '.$value );
    	        					}
    	        					
    	        				}
    	        				
    	        				if(!$length){

    	        					$mainstringN[] = ($position === 'after') ? ucwords($value.' '.$keword): ucwords($keword.' '.$value);
    	        				}
    	        			}
    	        		}
    		        }
		        }

		        shuffle($mainstringN);
		       	
		        return $mainstringN;
			}
		endif;

		$mainstring = all_wordgen($search_word, $industry, $position, $length);

		if( isset($_SESSION['filterd_columns']) ){
			$general_liststring = general_list( $search_word, $position, $length );

			$domains = domains_list( $_SESSION['filterd_columns'] );
		}

		if( !isset($_SESSION['filterd_columns']) && !isset($_GET['industry'])){
			$domains = domains_list( $search_word );
		}

	}

}