<div class="wrap word-manager-container">
    <h1 class="wp-heading-inline">Word Manager</h1>
    <a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_word_requests&add_new_request=true" class="page-title-action">Add New</a>
    <form>
        <h2 class="wp-heading-inline">All Requests</h2>
        <div class="tablenav top">
            <div class="tablenav-pages"><span class="displaying-num"><?php echo $viewData['count']; ?> items</span>
                        <?php echo $viewData['pagination']; ?>
            </div>
            <br class="clear">
        </div>
        <table class="wp-list-table widefat fixed striped table-view-list pages">
            <thead>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>Title</span></th>
                    <th scope="col" class="manage-column column-title column-primary"><span>Request's Language</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>Total Words</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Processed Words</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Status</span></th>
                </tr>
            </thead>

            <tbody id="requests-list">
                <?php foreach( $viewData['data'] as $key => $rq ): ?>
                <tr id="request-<?php echo $key; ?>" class="iedit author-self level-0 request-<?php echo $key; ?> type-page status-publish">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                        <div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
                        <strong><a class="row-title" href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words&word_request_id=<?php echo $key; ?>" aria-label="“<?php echo $rq['name']; ?>” (Edit)"><?php echo $rq['name']; ?></a></strong>
                        <div class="row-actions"><span class="edit"><a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_words&word_request_id=<?php echo $key; ?>" aria-label="Edit words">Edit words</a></span></div><button type="button" class="toggle-row"><span class="screen-reader-text">Show more details</span></button>
                    </td>
                    <td><?php echo $languages[$rq['language_code']]; ?>
                    </td>
                    <td><?php echo $rq['total_results']; ?></td>
                    <td><?php echo $rq['processed']; ?></td>
                    <td><?php echo $rq['status']; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>

            <tfoot>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>Title</span></th>
                    <th scope="col" class="manage-column column-title column-primary"><span>Request's Language</span></th>
                    <th scope="col" class="manage-column column-total-words column-primary"><span>Total Words</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Processed Words</span></th>
                    <th scope="col" class="manage-column column-processed-words column-primary"><span>Status</span></th>
                </tr>
            </tfoot>

        </table>
        <div class="tablenav bottom">
            <div class="tablenav-pages"><span class="displaying-num"><?php echo $viewData['count']; ?> items</span>
                <?php echo $viewData['pagination']; ?>
            </div>
        </div>
    </form>
</div>