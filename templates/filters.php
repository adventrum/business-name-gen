<div class="wrap word-manager-settings-container">
    <a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_generator_filters&add_generator_filter=true" class="page-title-action">Add New</a>
    <form>
        <h1 class="wp-heading-inline">Generator Filters</h1>
        <table class="wp-list-table widefat fixed striped table-view-list pages">
            <thead>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>ID</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Name</span></th>
                </tr>
            </thead>

            <tbody id="generator-filters-list">
                <?php foreach( $viewData as $vd ): ?>
                <tr id="cat" class="iedit author-self level-0 request type-page status-publish">
                    <td><?php echo $vd->id; ?></td>
                    <td>
                    <a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_generator_filters&edit_generator_filter=<?php echo $vd->id; ?>">
                        <strong><?php echo $vd->name; ?></strong>
                    </a>
                    <div class="row-actions"><span class="edit"><a href="<?php echo home_url(); ?>/wp-admin/admin.php?page=bnwm_generator_filters&edit_generator_filter=<?php echo $vd->id; ?>" aria-label="Edit words">Edit Options</a></span></div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>

            <tfoot>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>ID</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Name</span></th>
                </tr>
            </tfoot>

        </table>
    </form>
</div>