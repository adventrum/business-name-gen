<div class="wrap word-manager-settings-container">
    <a href="/wp-admin/admin.php?page=bnwm_generator_templates&add_template=true" class="page-title-action">Add New</a>
    <form>
        <h1 class="wp-heading-inline">Templates</h1>
        <table class="wp-list-table widefat fixed striped table-view-list pages">
            <thead>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>ID</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Name</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Content</span></th>
                </tr>
            </thead>

            <tbody id="generator-filters-list">
                <?php foreach( $templates as $template ): ?>
                <tr id="cat" class="iedit author-self level-0 request type-page status-publish">
                    <td><?php echo $template->id; ?></td>
                    <td>
                    <a href="/wp-admin/admin.php?page=bnwm_generator_templates&edit_template=<?php echo $template->id; ?>">
                        <strong><?php echo $template->name; ?></strong>
                    </a>
                    <div class="row-actions"><span class="edit"><a href="/wp-admin/admin.php?page=bnwm_generator_templates&edit_template=<?php echo $template->id; ?>" aria-label="Edit words">Edit Template</a></span></div>
                    </td>
                    <td>
                        <pre><code><?php echo $template->content; ?></code></pre>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>

            <tfoot>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>ID</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Name</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Content</span></th>
                </tr>
            </tfoot>

        </table>
    </form>
</div>