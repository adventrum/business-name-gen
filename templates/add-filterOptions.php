<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<form method="post" class="add-filter-shortcode">
    <?php if( isset($_GET['edit_generator_filter']) ): ?>
        <input type="hidden" name="edit_generator_filter" value="<?php echo $_GET['edit_generator_filter']; ?>">
    <?php endif; ?>
        <legend>
            <h2>Create Filter Options</h2>
        </legend>
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Options Name</label>
                    </th>
                    <td>
                        <input type="text" name="filter_name" value="<?php echo $filter_name ? $filter_name : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Filter Title</label>
                    </th>
                    <td>
                        <input type="text" name="filter_title" value="<?php echo $filter_options->filter_title ? $filter_options->filter_title : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Results Count Text</label>
                    </th>
                    <td>
                        <input type="text" name="results_count_text" value="<?php echo $filter_options->results_count_text ? $filter_options->results_count_text : ''; ?>">
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Enable Saved Ideas</label>
                    </th>
                    <td>
                        <input type="checkbox" class="filter-has-sub-fields" data-fields="filters-saved-ideas" name="enable_saved_ideas" value="true" <?php echo $filter_options->enable_saved_ideas->value == 'true' ? ' checked' : ''; ?>>
                    </td>
                </tr>
                <tr class="form-field form-required filter-sub-fields" id="filters-saved-ideas">
                    <th scope="row"></th>
                    <td>
                        <fieldset>
                            <table class="form-table">
                                <tbody>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Saved Ideas Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_title" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_title ? $filter_options->enable_saved_ideas->saved_ideas_title : ''; ?>">
                                        </td>
                                    </tr>

                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Saved Ideas Title Mobile</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_title_mobile" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_title_mobile ? $filter_options->enable_saved_ideas->saved_ideas_title_mobile : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Sub Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_subtitle" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_subtitle ? $filter_options->enable_saved_ideas->saved_ideas_subtitle : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Email placeholder</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_email_placeholder" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_email_placeholder ? $filter_options->enable_saved_ideas->saved_ideas_email_placeholder : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Email Send Button Text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_email_send_text" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_email_send_text ? $filter_options->enable_saved_ideas->saved_ideas_email_send_text : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Email Opt in Notice</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_email_optin_notice" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_email_optin_notice ? $filter_options->enable_saved_ideas->saved_ideas_email_optin_notice : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Privacy Policy Text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_policy_text" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_policy_text ? $filter_options->enable_saved_ideas->saved_ideas_policy_text : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Privacy Policy URL</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_policy_url" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_policy_url ? $filter_options->enable_saved_ideas->saved_ideas_policy_url : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Share Email Text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_share_email_text" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_share_email_text ? $filter_options->enable_saved_ideas->saved_ideas_share_email_text : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Mobile Close button text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_mobile_close_button" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_mobile_close_button ? $filter_options->enable_saved_ideas->saved_ideas_mobile_close_button : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Saved Ideas Modal Title Mobile</label>
                                        </th>
                                        <td>
                                            <input type="text" name="saved_ideas_modal_mobiletitle" value="<?php echo $filter_options->enable_saved_ideas->saved_ideas_modal_mobiletitle ? $filter_options->enable_saved_ideas->saved_ideas_modal_mobiletitle : ''; ?>">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Enable Industry Filters</label>
                    </th>
                    <td>
                        <input type="checkbox" class="filter-has-sub-fields" data-fields="filters-industry" name="enable_industry_filters" value="true" <?php echo $filter_options->enable_industry_filters->value == 'true' ? ' checked' : ''; ?>>
                    </td>
                </tr>
                <tr class="form-field form-required filter-sub-fields" id="filters-industry">
                    <th scope="row"></th>
                    <td>
                        <fieldset>
                            <table class="form-table">
                                <tbody>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Industry Filters Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="industry_title" value="<?php echo $filter_options->enable_industry_filters->industry_title ? $filter_options->enable_industry_filters->industry_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Mobile Modal Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="industry_modal_title" value="<?php echo $filter_options->enable_industry_filters->industry_modal_title ? $filter_options->enable_industry_filters->industry_modal_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Search Placeholer</label>
                                        </th>
                                        <td>
                                            <input type="text" name="industry_search_placeholder" value="<?php echo $filter_options->enable_industry_filters->industry_search_placeholder ? $filter_options->enable_industry_filters->industry_search_placeholder : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Clear Button Text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="industry_clear_button_text" value="<?php echo $filter_options->enable_industry_filters->industry_clear_button_text ? $filter_options->enable_industry_filters->industry_clear_button_text : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Apply Button Text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="industry_apply_button_text" value="<?php echo $filter_options->enable_industry_filters->industry_apply_button_text ? $filter_options->enable_industry_filters->industry_apply_button_text : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Mobile Modal Close Button Text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="industry_close_button_text" value="<?php echo $filter_options->enable_industry_filters->industry_close_button_text ? $filter_options->enable_industry_filters->industry_close_button_text : ''; ?>">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Enable Names Filters</label>
                    </th>
                    <td>
                        <input type="checkbox" class="filter-has-sub-fields" data-fields="filters-names" name="enable_names_filter" value="true" <?php echo $filter_options->enable_names_filter->value == 'true' ? ' checked' : ''; ?>>
                    </td>
                </tr>
                <tr class="form-field form-required filter-sub-fields" id="filters-names">
                    <th scope="row"></th>
                    <td>
                        <fieldset>
                            <table class="form-table">
                                <tbody>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="names_title" value="<?php echo $filter_options->enable_names_filter->names_title ? $filter_options->enable_names_filter->names_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Names Mobile Modal title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="names_mobile_modal_title" value="<?php echo $filter_options->enable_names_filter->names_mobile_modal_title ? $filter_options->enable_names_filter->names_mobile_modal_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Names Apply Button</label>
                                        </th>
                                        <td>
                                            <input type="text" name="names_apply_button" value="<?php echo $filter_options->enable_names_filter->names_apply_button ? $filter_options->enable_names_filter->names_apply_button : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Names Cancel Button</label>
                                        </th>
                                        <td>
                                            <input type="text" name="names_cancel_button" value="<?php echo $filter_options->enable_names_filter->names_cancel_button ? $filter_options->enable_names_filter->names_cancel_button : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Names Mobile Close Button Text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="names_close_button_text" value="<?php echo $filter_options->enable_names_filter->names_close_button_text ? $filter_options->enable_names_filter->names_close_button_text : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Enable Character Count Filter</label>
                                        </th>
                                        <td>
                                            <input type="checkbox" class="filter-has-sub-fields" name="enable_character_count" data-fields="filters-character-count" value="true" <?php echo $filter_options->enable_names_filter->enable_character_count->value == 'true' ? ' checked' : ''; ?>>
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required filter-sub-fields" id="filters-character-count">
                                        <th scope="row"></th>
                                        <td>
                                            <fieldset>
                                                <table class="form-table">
                                                    <tbody>
                                                    <tr class="form-field form-required">
                                                        <th scope="row">
                                                            <label>Character count Title</label>
                                                        </th>
                                                        <td>
                                                            <input type="text" name="character_count_title" value="<?php echo $filter_options->enable_names_filter->enable_character_count->character_count_title ? $filter_options->enable_names_filter->enable_character_count->character_count_title : ''; ?>">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Enable Words Filter</label>
                                        </th>
                                        <td>
                                            <input type="checkbox" class="filter-has-sub-fields" data-fields="filters-words" name="enable_words_filter" value="true" <?php echo $filter_options->enable_names_filter->enable_words_filter->value == 'true' ? ' checked' : ''; ?>>
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required filter-sub-fields" id="filters-words">
                                        <th scope="row"></th>
                                        <td>
                                            <fieldset>
                                                <table class="form-table">
                                                    <tbody>
                                                        <tr class="form-field form-required">
                                                            <th scope="row">
                                                                <label>Words Title</label>
                                                            </th>
                                                            <td>
                                                                <input type="text" name="words_title" value="<?php echo $filter_options->enable_names_filter->enable_words_filter->words_title ? $filter_options->enable_names_filter->enable_words_filter->words_title : ''; ?>">
                                                            </td>
                                                        </tr>
                                                        <tr class="form-field form-required">
                                                            <th scope="row">
                                                                <label>One word Title</label>
                                                            </th>
                                                            <td>
                                                                <input type="text" name="one_word_title" value="<?php echo $filter_options->enable_names_filter->enable_words_filter->one_word_title ? $filter_options->enable_names_filter->enable_words_filter->one_word_title : ''; ?>">
                                                            </td>
                                                        </tr>
                                                        <tr class="form-field form-required">
                                                            <th scope="row">
                                                                <label>Two word Title</label>
                                                            </th>
                                                            <td>
                                                                <input type="text" name="two_word_title" value="<?php echo $filter_options->enable_names_filter->enable_words_filter->two_word_title ? $filter_options->enable_names_filter->enable_words_filter->two_word_title : ''; ?>">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Enable keyword Filter</label>
                                        </th>
                                        <td>
                                            <input type="checkbox" class="filter-has-sub-fields" data-fields="filters-keyword" name="enable_keyword_filter" value="true" <?php echo $filter_options->enable_names_filter->enable_keyword_filter->value == 'true' ? ' checked' : ''; ?>>
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required filter-sub-fields" id="filters-keyword">
                                        <th scope="row"></th>
                                        <td>
                                            <fieldset>
                                                <table class="form-table">
                                                    <tbody>
                                                        <tr class="form-field form-required">
                                                            <th scope="row">
                                                                <label>Keyword Filter Title</label>
                                                            </th>
                                                            <td>
                                                                <input type="text" name="keyword_title" value="<?php echo $filter_options->enable_names_filter->enable_keyword_filter->keyword_title ? $filter_options->enable_names_filter->enable_keyword_filter->keyword_title : ''; ?>">
                                                            </td>
                                                        </tr>
                                                        <tr class="form-field form-required">
                                                            <th scope="row">
                                                                <label>Keyword Filter Title Mobile</label>
                                                            </th>
                                                            <td>
                                                                <input type="text" name="keyword_title_mobile" value="<?php echo $filter_options->enable_names_filter->enable_keyword_filter->keyword_title_mobile ? $filter_options->enable_names_filter->enable_keyword_filter->keyword_title_mobile : ''; ?>">
                                                            </td>
                                                        </tr>
                                                        <tr class="form-field form-required">
                                                            <th scope="row">
                                                                <label>Before Title</label>
                                                            </th>
                                                            <td>
                                                                <input type="text" name="before_title" value="<?php echo $filter_options->enable_names_filter->enable_keyword_filter->before_title ? $filter_options->enable_names_filter->enable_keyword_filter->before_title : ''; ?>">
                                                            </td>
                                                        </tr>
                                                        <tr class="form-field form-required">
                                                            <th scope="row">
                                                                <label>After Title</label>
                                                            </th>
                                                            <td>
                                                                <input type="text" name="after_title" value="<?php echo $filter_options->enable_names_filter->enable_keyword_filter->after_title ? $filter_options->enable_names_filter->enable_keyword_filter->after_title : ''; ?>">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Enable Rhyming Filter</label>
                                        </th>
                                        <td>
                                            <input type="checkbox" class="filter-has-sub-fields" name="enable_rhyming_filter" value="true" data-fields="filters-rhyming" <?php echo $filter_options->enable_names_filter->enable_rhyming_filter->value == 'true' ? ' checked' : ''; ?>>
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required filter-sub-fields" id="filters-rhyming">
                                        <th scope="row"></th>
                                        <td>
                                            <fieldset>
                                                <table class="form-table">
                                                    <tbody>
                                                        <tr class="form-field form-required">
                                                            <th scope="row">
                                                                <label>Rhyming Title</label>
                                                            </th>
                                                            <td>
                                                                <input type="text" name="rhyming_title" value="<?php echo $filter_options->enable_names_filter->enable_rhyming_filter->rhyming_title ? $filter_options->enable_names_filter->enable_rhyming_filter->rhyming_title : ''; ?>">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Enable Grammar Filter</label>
                    </th>
                    <td>
                        <input type="checkbox" class="filter-has-sub-fields" data-fields="filters-grammar" name="enable_grammar_filters" value="true" <?php echo $filter_options->enable_grammar_filters->value == 'true' ? ' checked' : ''; ?>>
                    </td>
                </tr>
                <tr class="form-field form-required filter-sub-fields" id="filters-grammar">
                    <th scope="row"></th>
                    <td>
                        <fieldset>
                            <table class="form-table">
                                <tbody>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Grammar Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="grammar_title" value="<?php echo $filter_options->enable_grammar_filters->grammar_title ? $filter_options->enable_grammar_filters->grammar_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Plural Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="plural_title" value="<?php echo $filter_options->enable_grammar_filters->plural_title ? $filter_options->enable_grammar_filters->plural_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Masculine Singular Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="masculine_singular_title" value="<?php echo $filter_options->enable_grammar_filters->masculine_singular_title ? $filter_options->enable_grammar_filters->masculine_singular_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Feminine Singular Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="feminine_singular_title" value="<?php echo $filter_options->enable_grammar_filters->feminine_singular_title ? $filter_options->enable_grammar_filters->feminine_singular_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Masculine Plural Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="masculine_plural_title" value="<?php echo $filter_options->enable_grammar_filters->masculine_plural_title ? $filter_options->enable_grammar_filters->masculine_plural_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Feminine Plural Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="feminine_plural_title" value="<?php echo $filter_options->enable_grammar_filters->feminine_plural_title ? $filter_options->enable_grammar_filters->feminine_plural_title : ''; ?>">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Enable Extensions Filter</label>
                    </th>
                    <td>
                        <input type="checkbox" class="filter-has-sub-fields" data-fields="filters-extensions" name="enable_extensions_filter" value="true" <?php echo $filter_options->enable_extensions_filter->value == 'true' ? ' checked' : ''; ?>>
                    </td>
                </tr>
                <tr class="form-field form-required filter-sub-fields" id="filters-extensions">
                    <th scope="row"></th>
                    <td>
                        <fieldset>
                            <table class="form-table">
                                <tbody>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Extensions Filter Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="extensions_title" value="<?php echo $filter_options->enable_extensions_filter->extensions_title ? $filter_options->enable_extensions_filter->extensions_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Extension Search Bar placeholder</label>
                                        </th>
                                        <td>
                                            <input type="text" name="extension_search_placeholder" value="<?php echo $filter_options->enable_extensions_filter->extension_search_placeholder ? $filter_options->enable_extensions_filter->extension_search_placeholder : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Apply Button text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="extension_apply_button" value="<?php echo $filter_options->enable_extensions_filter->extension_apply_button ? $filter_options->enable_extensions_filter->extension_apply_button : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Cancel Button text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="extension_cancel_button" value="<?php echo $filter_options->enable_extensions_filter->extension_cancel_button ? $filter_options->enable_extensions_filter->extension_cancel_button : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Mobile Close Button text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="extension_mobile_close_button" value="<?php echo $filter_options->enable_extensions_filter->extension_mobile_close_button ? $filter_options->enable_extensions_filter->extension_mobile_close_button : ''; ?>">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row">
                        <label>Enable Domains Availability Widget</label>
                    </th>
                    <td>
                        <input type="checkbox" class="filter-has-sub-fields" data-fields="filters-domainsAvailability" name="enable_domains_availability_widget" value="true" <?php echo $filter_options->enable_domains_availability_widget->value == 'true' ? ' checked' : ''; ?>>
                    </td>
                </tr>
                <tr class="form-field form-required filter-sub-fields" id="filters-domainsAvailability">
                    <th scope="row"></th>
                    <td>
                        <fieldset>
                            <table class="form-table">
                                <tbody>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Widget Title</label>
                                        </th>
                                        <td>
                                            <input type="text" name="domains_title" value="<?php echo $filter_options->enable_domains_availability_widget->domains_title ? $filter_options->enable_domains_availability_widget->domains_title : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Widget Input Placeholder</label>
                                        </th>
                                        <td>
                                            <input type="text" name="domains_name_placeholder" value="<?php echo $filter_options->enable_domains_availability_widget->domains_name_placeholder ? $filter_options->enable_domains_availability_widget->domains_name_placeholder : ''; ?>">
                                        </td>
                                    </tr>
                                    <tr class="form-field form-required">
                                        <th scope="row">
                                            <label>Widget Button Text</label>
                                        </th>
                                        <td>
                                            <input type="text" name="domains_button_text" value="<?php echo $filter_options->enable_domains_availability_widget->domains_button_text ? $filter_options->enable_domains_availability_widget->domains_button_text : ''; ?>">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
    <?php if (isset($_GET['edit_generator_filter'])) : ?>
        <input type="hidden" name="edit_generator_filter" value="<?php echo $filter_id; ?>">
    <?php endif; ?>
    <p class="submit" style="margin-top: 0;"><input type="submit" name="createrequest" id="createrequest-btn" class="button button-primary" value="<?php echo (isset($_GET['edit_generator_filter'])) ? 'Update Filter Options' : 'Add New Filter Options'; ?>"></p>

</form>