<div class="wrap word-manager-dashboard-container">
    <h1 class="wp-heading-inline">Word Manager Dashboard</h1>
</div>
<div id="dashboard-widgets-wrap">
    <div id="dashboard-widgets" class="metabox-holder">
        <div id="postbox-container-1" class="postbox-container">
            <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                <div id="dashboard_site_health" class="postbox ">
                    <div class="postbox-header">
                        <h2 class="hndle ui-sortable-handle">Word Requests</h2>
                    </div>
                    <div class="inside">
                        <h2 class="hndle ui-sortable-handle">Requests added in the past month</h2>
                        <table>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>No. of Words added</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach( $viewData['requests'] as $req ): ?>
                                <tr>
                                    <td><?php echo $req->name; ?></td>
                                    <td><?php echo $req->total_items; ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <a href="<?php echo home_url(); ?>/wp-admin/wp-admin/wp-admin/admin.php?page=bnwm_word_requests">View all Requests</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="postbox-container-2" class="postbox-container">
            <div id="side-sortables" class="meta-box-sortables ui-sortable">
                <div class="postbox ">
                    <div class="postbox-header">
                        <h2 class="hndle ui-sortable-handle">Word Categories</h2>
                    </div>
                    <div class="inside">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>