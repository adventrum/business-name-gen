<div class="wrap word-manager-settings-container">
    <a href="/wp-admin/admin.php?page=bnwm_generator_advertisements&add_advertisement=true" class="page-title-action">Add New</a>
    <form>
        <h1 class="wp-heading-inline">Advertisements</h1>
        <table class="wp-list-table widefat fixed striped table-view-list pages">
            <thead>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>ID</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Name</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Advertisement Type</span></th>
                </tr>
            </thead>

            <tbody id="advertisements-filters-list">
                <?php foreach( $advertisements as $advertisement ): ?>
                <tr id="cat" class="iedit author-self level-0 request type-page status-publish">
                    <td><?php echo $advertisement->id; ?></td>
                    <td>
                    <a href="/wp-admin/admin.php?page=bnwm_generator_advertisements&edit_advertisement=<?php echo $advertisement->id; ?>">
                        <strong><?php echo $advertisement->name; ?></strong>
                    </a>
                    <div class="row-actions"><span class="edit"><a href="/wp-admin/admin.php?page=bnwm_generator_advertisements&edit_advertisement=<?php echo $advertisement->id; ?>" aria-label="Edit words">Edit Advertisement</a></span></div>
                    </td>
                    <td><?php echo $advertisement->advertisement_type; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>

            <tfoot>
                <tr>
                    <th scope="col" class="manage-column column-title column-primary"><span>ID</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Name</span></th>
                    <th scope="col" class="manage-column column-primary"><span>Advertisement Type</span></th>
                </tr>
            </tfoot>

        </table>
    </form>
</div>