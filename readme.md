# Documentation
## _Minimum Requirements_
```
PHP >= 7.4
MySQL >= 5.7.38
```
## &lt;global&gt;Global Variables
Global Variable use to access the wordmanager and config objects
#### ```$bnwm```
Get Access to WordManager and Config
```
global $bnwm;
$bnwm->wordmanager
```

## &lt;functions&gt;Core Functions
Core functions are plugins functions that can be call at any point of time and place in wordpress.

#### ```bnwm_search_results()```
Get Search Results
```
@return: array

@response: [
   
]
```

## &lt;class&gt;WordManager
WordManager class is responsible for Managing Categories, Words, Word Languages, Word Filters, Search etc.

#### ```getCategories()```
Fetch Categories
```
@return: array

@response: [
    1 => "Agriculture",
    2 => "Art"
    ...
]
```
#### ```getLanguages()```
Fetch Languages
```
@return: array

@response: [
    'en' => "English",
    'de' => "German"
    ...
]
```
#### ```word(string)```
Set Word for Further Search
```
@params: string (Word Name)

@return: WordManager

@response: WordManager
```
#### ```position(string)```
Set Word for Further Search
```
@params: string (Position Name): before|after

@return: WordManager

@response: WordManager
```

#### ```isRhym(bool)```
Set Rhyming for Further Filter
```
@params: boolean (is rhyming status): true|false

@return: WordManager

@response: WordManager
```

#### ```genderFilter(array)```
Set Gender for Further Filter
```
@params: array (array of kind of genders): ["masculine", "feminine", "plural", "masculine_plural", "feminine_plural" ]

@return: WordManager

@response: WordManager
```
#### ```limit(int)```
Limit Results
```
@params: int value

@return: WordManager

@response: WordManager
```

#### ```page(int)```
Pagination Page Number
```
@params: int value

@return: WordManager

@response: WordManager
```
#### ```wordTypes(array)```
Set Word Types
```
@params: array ["one_word", "two_word"]

@return: WordManager

@response: WordManager
```

#### ```getPopulatedCategories()```
Get array of populated category Ids From the search result set.
```
@return: array

@response: [
    1,
    2,
    3,
    ...
]
```

#### ```getSearchResults()```
Get array of searched results.
```
@return: array

@response: [
    "word1",
    "word2",
    "word3",
    ...
]
```

#### ```getTotalCount()```
Get Total Count.
```
@return: int

@response: 1
```

########
__####
Final Changes
__final_one
__--^^^_____
