jQuery(document).ready(function(){

    var file_frame;
    var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
    var set_to_post_id = 0; // Set this

    jQuery('.upload-media').on('click', function( event ){
        let attachment;
        event.preventDefault();

        let mediaPreview = $(this).attr('data-preview');
        let mediaAttachment = $(this).attr('data-attachment');

        let fileFormats = [ 'video', 'image' ];
        let selectedType = $(this).attr("media-type");
        
        if(selectedType == "Video"){
            fileFormats = ['video'];
        }else if(selectedType == "Image"){
            fileFormats = ['image'];
        }

        // Create the media frame.
        file_frame = wp.media.frames.file_frame = wp.media({
            title: 'Select a media to upload',
            button: {
                text: 'Use this media',
            },
            library: {
                type: fileFormats
            },
            multiple: false	// Set to true to allow multiple files to be selected
        });

        // When an image is selected, run a callback.
        file_frame.on( 'select', function() {
            // We set multiple to false so only get one image from the uploader
            attachment = file_frame.state().get('selection').first().toJSON();
            console.log(attachment);
            let mediaSrc = "/wp-includes/images/media/default.png";
            switch(attachment.type){
                case 'image':
                    mediaSrc = attachment.url;
                break;
                case 'video':
                    mediaSrc = "/wp-includes/images/media/video.png";
                break;
            }
            // Do something with attachment.id and/or attachment.url here
            
            $( mediaPreview ).attr( 'src', mediaSrc ).css( 'width', 'auto' );
            $( mediaAttachment ).val( attachment.id );

            // Restore the main post ID
            wp.media.model.settings.post.id = wp_media_post_id;
        });

            // Finally, open the modal
            file_frame.open();
    });

    // Restore the main ID when the add media button is pressed
    jQuery( 'a.add_media' ).on( 'click', function() {
        wp.media.model.settings.post.id = wp_media_post_id;
    });



})