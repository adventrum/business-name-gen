<?php

/**
 * @package BusinessNameWordManager
 *
 */
/*
Plugin Name: Business Name Word Manager
Description: Word Manager for Name Ideas Algo
Version: 1.0.0
Author: Cybertron
Text Domain: business-name-word-manager
 */

// If this file is called directly, Abort!!
defined('ABSPATH') or die('Hey, what are you doing here? You silly human!');

// Require Once the Composer Autoload
if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}

// Define Constants

define ( 'WORD_MANAGER_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define ( 'WORD_MANAGER_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define ( 'WORD_MANAGER_PLUGIN_NAME', plugin_basename( __FILE__ ) );
define ( 'WORD_MANAGER_PLUGIN_FILE', plugin_basename( dirname( __FILE__, 3 ) ).'/business-name-word-manager.php');

use BNWM\Base\Activate;
use BNWM\Base\BusinessNameWordManager;
use BNWM\Base\Deactivate;

// Method that runs on Plugin Activation
function activate_word_manager()
{
    Activate::activate();
}

// Method that runs on Plugin Deactivation
function deactivate_word_manager()
{
    Deactivate::deactivate();
}

register_activation_hook( __FILE__ , 'activate_word_manager' );
register_deactivation_hook( __FILE__ , 'deactivate_word_manager' );


if( class_exists( "BNWM\\Init" ) ){
    BNWM\Init::register_services();
}

/**
 * Returns the main instance of BusinessNameWordManager.
 *
 * @return BusinessNameWordManager
 */
function BNWM() { 
	return BusinessNameWordManager::instance();
}
$GLOBALS['bnwm'] = BNWM();